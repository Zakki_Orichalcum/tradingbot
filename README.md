# TradingBot

A bot that can trade crypto or whatever else that has several different trading algorithms and trying to add more.

I have been working on this for awhile in a private sense but now I think I have something in a relatively stable working state that I know is in no way optimized but I'll keep working and maybe some people can jump in to help and we can all make a better trading bot down here.

# Auth.json

This is required but not be in the repository. The general structure will work like this. Do not add this to requests or they will be instantly denied.

{
    "api": {
        "<apitype>": {"auth":"", "secret": ""}
    },
    email {}
}

# Services.json

This is very similar to Auth.json but this is the bots that will be used by your bot.
{
    "services":[
        {
            "id": <a number>,
            "botType": "<BotName>",
            <additional settings>
        }
    ]
}

# Bots I have implemented so far

* PlaceAndWatchBot
* ADXMomentumBot
* BBAndRSIBot
* BollingerBandsBot
* KSTBot
* MACDBot
* MACDBot_old
* MachineLearningBot
* RSIBot
* ShortBot
* SimpleMeanAveragesBot
* TriangularArbitrageBot
* WatcherBot

