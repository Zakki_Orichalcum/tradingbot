const BaseBotClass = require('./baseBotClass');

const N = require("../utilities").Numbers;

class ADXMomentumBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.timeFrame = args.timeFrame;
    }

    async run()
    {
        super.run();
        
        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        var last = (await this.GetLatestKline(this.timeFrame, this.symbol.combinedSymbol, 1))[0];
        var price = await this.GetAveragePrice(this.symbol.combinedSymbol);

        if(!order && last != null && last.adx != null)
        {
            if(last.adx > 25 && price > last.ma200)
            {
                var o = await this.Buy();
                order = await this.ProcessOrder(o.orderId, o.symbol, o.clientOrderId);
                var or = await this.GetLastOrderFromDatabase(this.symbol.combinedSymbol);
                await this.timer(1000);
                order = await this.api.PlaceStopLossLimitOrder(this.symbol.combinedSymbol, "SELL", or.amount, or.pricePaying * 1.5, price)
            }
            // else if((await this.GetBalance(this.symbol.primarySymbol) * price) > await this.GetBalance(this.symbol.secondarySymbol))
            // {
            //     // var o = await this.GetLastOrderFromDatabase(this.symbol.combinedSymbol);
            //     // var p = N.toFixedRoundDown(Number(o.pricePaying * 1.5), this.priceSize);
            //     // //var sp = N.toFixedRoundDown(Number(0.01 + Number(o.pricePaying + 0.01 > price - 0.01 ? price - 0.01 : o.pricePaying + 0.01)), this.priceSize);
            //     // var sp = N.toFixedRoundDown(Number(price), this.priceSize);
                
            //     // order = await this.api.PlaceStopLossLimitOrder(this.symbol.combinedSymbol, "SELL", await this.GetBalance(this.symbol.primarySymbol), p, sp);
            //     // console.log(order);
            //     order = await this.Sell();
            // }
        }

        if(order){
            await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                order.symbol, 
                order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
        }

        await this.PostLooking(last, price);
    }

    async PostLooking(kline, price){
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (price * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - ADXMomentum ADX ${kline && kline.adx ? kline.adx : 0} | MA200 ${kline && kline.ma200 ? kline.ma200 : 0} | Current ${price} || `+
            `${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }
}
module.exports = ADXMomentumBot;