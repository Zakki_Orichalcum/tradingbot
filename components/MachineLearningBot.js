const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const { Network } = require('../neuralnetwork');
const Arrays = new A();
const day = 24 * 60 * 60 * 1000;
const logic = require('../ml-bot.json');

class MachineLearningBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.checkLines = true;
        this.Network = null;
    }

    async Initialize(){
        super.Initialize();

        this.Network = new Network(logic.networkStructure, logic);
        this.lastKline = null;
    }

    async run()
    {
        super.run();
        
        var last = await this.GetLatestKline("1m", this.symbol.combinedSymbol, 1);
        if(last && (!this.lastKline || last.id != this.lastKline.id))
        {
            this.lastKline = last;
            if(this.lastKline.kSignal)
            {
                var conn = await this.GetConnection();
                var max = await this.query(conn.get, `Select * from [1mKlines] where symbol = '${this.symbol.combinedSymbol}' order by high DESC LIMIT 1`);
                var smmaUpMax = await this.query(conn.get, `Select * from [1mKlines] where symbol = '${this.symbol.combinedSymbol}' order by smmaUp14 DESC LIMIT 1`);
                var smmaDownMax = await this.query(conn.get, `Select * from [1mKlines] where symbol = '${this.symbol.combinedSymbol}' order by smmaDown14 DESC LIMIT 1`);
        
        
                var normalized = this.klineToArray(this.lastKline, max.high, smmaUpMax.smmaUp14 > smmaDownMax.smmaDown14 ? smmaUpMax.smmaUp14 : smmaDownMax.smmaDown14);
        
                var answer = this.Network.run(normalized);
                var max = answer[0];
                var signal = 0;
                this.Post(answer);
                this.Post(JSON.stringify(this.lastKline));
                for(var k = 1; k < answer.length; k++)
                {
                    //console.log(answer[k], max, answer[k] > max);
                    if(answer[k] > max)
                    {
                        max = answer[k];
                        signal = k;
                    }
                }
        
                var currentAction = await this.CurrentAction();
            
                if(currentAction == 'BUY' && signal == 1){
                    await this.Buy();
                }
                else if(currentAction == 'SELL' && signal == 2){
                    await this.Sell();
                }
            }
        }
        
        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        if(order){
            console.log("Trying to process order ", order);
            await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                order.symbol, 
                order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
        }

        await this.PostLooking(signal);
    }

    async PostLooking(signal){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - MLBot ${side} | ${signal ? signal : 0} | `+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }

    klineToArray(kline, maxPrice, smmaMax)
    {
        var output = [];
        output.push(kline.open/maxPrice);
        output.push(kline.close/maxPrice);
        output.push(kline.high/maxPrice);
        output.push(kline.low/maxPrice);
        output.push(kline.ma20 ? kline.ma20/maxPrice : 0);
        output.push(kline.ma50 ? kline.ma50/maxPrice : 0);
        output.push(kline.ma200 ? kline.ma200/maxPrice : 0);
        output.push(kline.diplus ? kline.diplus/100 : 0);
        output.push(kline.diminus ? kline.diminus/100 : 0);
        output.push(kline.adx ? kline.adx/100 : 0);
        output.push(kline.smmaUp14 ? kline.smmaUp14/smmaMax : 0);
        output.push(kline.smmaDown14 ? kline.smmaDown14/smmaMax : 0);
        output.push(kline.rsi ? kline.rsi/100 : 0);
        output.push(kline.kP ? kline.kP/100 : 0);
        output.push(kline.smoothkP ? kline.smoothkP/100 : 0);
        output.push(kline.dP ? kline.dP/100 : 0);
        output.push(kline.kst && kline.kSignal ? kline.kst/kline.kSignal : 0);

        return output;
    }
}
module.exports = MachineLearningBot;