const { Timing, Numbers } = require('../utilities');
const BaseBotClass = require('./baseBotClass');
const day = 24 * 60 * 60 * 1000;

class ExponentialMeanAveragesBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.firstMean = args.firstMean;
        this.secondMean = args.secondMean;
        this.checkLines = true;
        this.lastfth = null;
        this.timingInfo = args.timingInfo ? Timing.TimingObjectToString(args.timingInfo) : "1d";
    }

    async Initialize(){
        super.Initialize();

        //await this.SetupKlines(this.secondMean);
    }

    async run()
    {
        super.run();

        this.lastfth = await this.GetFirstVsSecond();
        var currentAction = await this.CurrentAction();
    
        if(currentAction == 'BUY' && this.lastfth.result > 0)
        {
            await this.Buy();
        }
        else if(currentAction == 'SELL' && this.lastfth.result < 0)
        {
            await this.Sell();
        }

        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        if(order){
            await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                order.symbol, 
                order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
        }

        await this.PostLooking(this.lastfth);
    }

    async GetFirstVsSecond(){
        var k = await this.GetAverageForNumberOfDays(this.firstMean);
        var k2 = await this.GetAverageForNumberOfDays(this.secondMean);

        return { result: k > k2 ? 1 : k < k2 ? -1 : 0, first: k, second: k2};
    }

    async GetAverageForNumberOfDays(days)
    {
        switch(days)
        {
            case 2:
            case 5:
            case 10:
            case 20:
            case 30:
            case 50:
            case 200:
            case 400:
                var last = await this.GetLatestKline(this.timingInfo, this.symbol.combinedSymbol, 1);

                return last.length > 0 && last[0][`ema${days}`]? last[0][`ema${days}`] : 0;
            default:
                var conn = await this.GetConnection();
                var last = await this.query(conn.all, `Select * from [${this.timingInfo}Klines] where symbol = '${this.symbol.combinedSymbol}' ORDER BY date DESC LIMIT ${days}`);
                var sum = 0;
                for (let i = 0; i < last.length; i++) {
                    const element = last[i];
                    sum += element.close;
                }

                return sum/last.length;
        }
    }

    async PostLooking(obj){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
        var total2 = (primary + second/currentPrice);
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - ${String(this.id).padStart(2, ' ')} EMABot ${String(side).padStart(4, ' ')} ${this.timingInfo} `+
            `EMA${String(this.firstMean).padEnd(4, ' ')} ${String(Numbers.toFixedRoundDown(obj.first, 4)).padStart(10, ' ')} | `+
            `EMA${String(this.secondMean).padEnd(4, ' ')} ${String(Numbers.toFixedRoundDown(obj.second, 4)).padStart(10, ' ')} | ${String(obj.result).padStart(2, " ")} | `+
            `Current: ${String(this.symbol.primarySymbol).padStart(4, ' ')} ${String(Numbers.toFixedRoundDown(primary, 4)).padStart(10, ' ')} | `+
            `${String(this.symbol.secondarySymbol).padStart(4, ' ')} ${String(Numbers.toFixedRoundDown(second, 4)).padStart(10, ' ')} | `+
            `Total proposed: $${String(Numbers.toFixedRoundDown(total, 4)).padStart(10, ' ')} `+
            `AKA ${String(this.symbol.primarySymbol).padStart(4, ' ')} ${String(Numbers.toFixedRoundDown(total2, 4)).padStart(10, ' ')}`);
    }
}
module.exports = ExponentialMeanAveragesBot;