const sqlite3 = require('sqlite3').verbose();
const SQLUtility = require("../utilities").SQLUtility;
const SQL = new SQLUtility();
const N = require("../utilities").Numbers;
const fetch = require('node-fetch');
const auth = require('../auth').Binance;
const crypto = require('crypto-js');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const Arrays = new A();
const SendingEmails = require("../utilities/sendEmail");

const DEFAULT_DB = 'data.db';
const binanceFee = 0.001;
const reductionConstant = 0.0001;
const averageFell = 0.025;
const standardWaitBeforeReducing = 2 * 60 * 1000;
const pollingTime = 1500;
const minNotional_adjustmentFactor = 1.75;
const endpoint = 'https://api.binance.us';
const day = 24 * 60 * 60 * 1000;

class BaseBotClass{
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        this.id = args.botId;
        this.symbol = symbols[args.symbol];
        this.limit = args.limit;
        this.databasePath = databasePath;

        this.api = apis[0];

        this.priceAction = 0;

        this.reductionAmount = 1;
        this.nextTimeToStartReducing = 0;

        this.topMessages = [];
        this.postingMessages = [];
        this.serverTimeAdjust = Date.now() - serverTime;

        this.allowances = args.allowance;
        this.HasAllowances = this.allowances != null;
        this.startingPrice = 0;
        this.startingSymbol = 0;
        this.totalExchangeInfo = exchangeInfo;
        this.exchangeInfo = exchangeInfo[args.symbol];
        this.priceSize = N.countDecimalsToAOne(this.exchangeInfo["PRICE_FILTER"].tickSize);
        this.quantitySize = N.countDecimalsToAOne(this.exchangeInfo["LOT_SIZE"].stepSize);
        this.minNotional = this.exchangeInfo["MIN_NOTIONAL"].minNotional;
        this.sendEmail = false;

        ///console.log(this.symbol.combinedSymbol, this.priceSize, this.quantitySize, this.minNotional);
    }

    async Initialize(){
        await this.GetBalances();
        var lastDoge = await this.GetLastPrice(this.symbol.primarySymbol);

        if(!lastDoge){
            var balance = await this.GetBalance(this.symbol.primarySymbol);
            var p = await this.GetLatestPriceInUSDForSymbol(this.symbol.primarySymbol);
            await this.SetTransaction(this.symbol.primarySymbol, 'BUY', balance, p);
            lastDoge = await this.GetLastPrice(this.symbol.primarySymbol);
        }

        if(this.HasAllowances)
        {
            await this.SetupAllowances();
        }

        var order = await this.GetLastOrderFromDatabase(this.symbol.combinedSymbol);
        if(order)
        {
            this.priceAction = order.pricePaying;
        }
        else{
            this.priceAction = await this.GetAveragePrice(this.symbol.combinedSymbol);
        }
    }

    timer(ms){ return new Promise( res => setTimeout(res, ms)) };

    GetConnection(){
        this.db = new sqlite3.Database(this.databasePath);
    
        return this.db;
      }
    
    async query(fn, query, values) {
        if (values) {
            fn = fn.bind(this.db, query, values);
        } else {
            fn = fn.bind(this.db, query);
        }
        return new Promise((resolve, reject) => {
            try {
                fn((err, rows) => {
                    if (err) {
                        reject(
                            new Error(
                                `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                            ),
                        );
                    } else {
                        resolve(rows);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    async run(){
        this.ClearMessages();

        var botActions = await this.GetBotActions();

        for(var i = 0; i < botActions.length; i++)
        {
            var b = botActions[i];
            switch(b.action){
                case "BUY":
                    await this.Buy();
                    break;
                case "SELL":
                    await this.Sell();
                    break;
            }

            await this.DeleteBotAction(b.id);
        }
    }
    
    async ProcessOrder(orderId, symbol, origClientOrderId){
        var o = await this.GetOrder(symbol, orderId, origClientOrderId);
        //console.log(o);
        if(o)
        {
            var od = await this.GetOrderFromDatabase(o.orderId);
    
            if(o.status == "FILLED" && od.condition == 'PENDING'){
                if(o.fills)
                {
                    for(var i = 0; i < o.fills.length; i++){
                        var fill = o.fills[i];
        
                        var price = fill.price ? fill.price : await this.GetAveragePrice(symbol);
                        //this.Post(price);
        
                        await this.SetTransaction(this.symbol.primarySymbol, o.side, 
                            fill.qty, await this.GetLatestPriceInUSDForSymbol(this.symbol.primarySymbol));
            
                        await this.AddCommissionFee(fill.commissionAsset, fill.commission);
    
                        this.priceAction = price;
                    }
                }
                else{
                    var price = o.price ? o.price : await this.GetAveragePrice(symbol);
                    if(o.side == "BUY"){
                        var usd = Number.parseFloat(o.executedQty)*binanceFee;
                        await this.AddCommissionFee(this.symbol.primarySymbol, usd);

                        await this.SetTransaction(this.symbol.primarySymbol, "BUY", 
                            o.executedQty-usd, await this.GetLatestPriceInUSDForSymbol(this.symbol.primarySymbol));

                        var usdt = Number.parseFloat(o.cummulativeQuoteQty);
            
                        //this.Post(`${usdt} - ${o.executedQty-usd}`);
                    }
                    else{
                        var usdt = Number.parseFloat(o.cummulativeQuoteQty) - (Number.parseFloat(o.cummulativeQuoteQty)*binanceFee);

                        await this.SetTransaction(this.symbol.primarySymbol, "SELL", 
                            o.executedQty, await this.GetLatestPriceInUSDForSymbol(this.symbol.primarySymbol));
                    }

                    this.priceAction = price;
                }
    
                
                if(o.side == "BUY"){
                    var usdt = Number.parseFloat(o.cummulativeQuoteQty);
                    await this.SetTransaction(this.symbol.secondarySymbol, "SELL", 
                        usdt, await this.GetLatestPriceInUSDForSymbol(this.symbol.secondarySymbol));
                }
                else{
                    var com = (Number.parseFloat(o.cummulativeQuoteQty)*binanceFee);
                    var usdt = Number.parseFloat(o.cummulativeQuoteQty) - com;
                    //this.Post(`${usdt} - ${o.executedQty}`);
                    await this.SetTransaction(this.symbol.secondarySymbol, "BUY", 
                        usdt, await this.GetLatestPriceInUSDForSymbol(this.symbol.secondarySymbol));

                    await this.AddCommissionFee(this.symbol.secondarySymbol, com);
                }

                this.Post(`FILLED a ${o.side} order for ${o.executedQty} ${this.symbol.primarySymbol} for ${this.symbol.secondarySymbol == "USDT" ? "T$" : this.symbol.secondarySymbol}${o.cummulativeQuoteQty}`)
        
                await this.SetFilledOrder(orderId);
                //await this.timer(3 * pollingTime);
                
                if(!await this.GetBalances())
                    return false;
        
                var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
                var total = (currentPrice * await this.GetBalance(this.symbol.primarySymbol) + await this.GetBalance(this.symbol.secondarySymbol)
                            - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
                this.reductionAmount = 1;
                this.nextTimeToStartReducing = 0;
                this.Post(`Current Records: ${this.symbol.primarySymbol}: ${await this.GetBalance(this.symbol.primarySymbol)}, ${this.symbol.secondarySymbol}: ${await this.GetBalance(this.symbol.secondarySymbol)}, Commission Paided: ${this.balances["commissions"]} | Running Total: $${total}`);
                this.sendEmail = true;
        
                return true;
            }
            else if(o.status == "CANCELED" && od.condition == 'PENDING')
            {
                await this.SetCancelledOrder(od.id);
            }
        }
        
        return false;
    }

    async GetServerTime(){
        var ts = await fetch(`${endpoint}/api/v3/time`);
        var timestamp = await ts.json();

        return timestamp.serverTime;
    }

    async SetupAllowances()
    {
        var conn = await this.GetConnection();

        var keys = Object.keys(this.allowances);
        for(var i = 0; i < keys.length; i++){
            var d = await this.query(conn.get, `Select * from botAllowance where symbol = '${keys[i]}' AND bot = ${this.id}`);
            if(!d){
                 await this.AddUpdateBotAllowance(keys[i], this.allowances[keys[i]]);
            }
        }
    }

    async AddUpdateBotAllowance(symbol, add, ){
        var conn = await this.GetConnection();

        var ba = await this.GetBotAllowance(symbol);
        var q = '';
        if(ba){
            var d = ba.allowance + add > 0 ? ba.allowance + add : 0;
            q = `Update botAllowance SET allowance = ${d} where bot = ${this.id} AND symbol = '${symbol}'`;
        }
        else{
            q =`Insert Into botAllowance(bot, symbol, allowance) Values (${this.id}, '${symbol}', ${add})`;
        }

        //this.Post(q);
        await this.query(conn.run, q);
    }

    async GetBotAllowance(symbol){
        var conn = await this.GetConnection();
    
        return await this.query(conn.get, `SELECT * FROM botAllowance WHERE bot = ${this.id} AND symbol = '${symbol}'`);
    }

    async GetBotAllowances(){
        var conn = await this.GetConnection();
    
        var r = await this.query(conn.all, `SELECT * FROM botAllowance WHERE bot = ${this.id}`);
        var output = {};
        r.forEach(v => {
            output[v.symbol] = v.allowance;
        });

        return output;
    }

    async DeleteBotAction(id)
    {
        var conn = await this.GetConnection();
    
        await this.query(conn.run, `Delete FROM botActions WHERE bot = ${this.id} AND id = ${id}`);
    }
    

    async GetBotActions()
    {
        var conn = await this.GetConnection();
    
        return await this.query(conn.all, `SELECT * FROM botActions WHERE bot = ${this.id}`);
    }
    
    async GetLastPrice(symbol){
        var conn = await this.GetConnection();
    
        return await this.query(conn.get, `SELECT * FROM transactions WHERE symbol = '${symbol}' AND bot = ${this.id} ORDER BY timestamp DESC LIMIT 1`);
    }

    async GetNewPrice(){
        var order = await this.GetLastFilledOrderFromDatabase(this.symbol.combinedSymbol);
        if(order)
        {
            return order.pricePaying;
        }

        var transaction = await this.GetLastPrice(this.symbol.primarySymbol);
        if(transaction)
        {
            var usdtoUsdt = await this.GetLatestPriceInUSDForSymbol(this.symbol.secondarySymbol);
            var action = transaction/usdtoUsdt;
            return action;
        }
        
        var a = await this.GetAveragePrice(this.symbol.combinedSymbol);
        return a;
    }
    
    async SetTransaction(symbol, transactionType, amount, price){
        var conn = await this.GetConnection();
        if(!price || price <= 0)
            price = await this.GetLatestPriceInUSDForSymbol(symbol);

        if(this.allowances){
            await this.AddUpdateBotAllowance(symbol, (transactionType == "BUY" ? 1 : -1) * amount);
        }
    
        return await this.query(conn.run, `INSERT INTO transactions (symbol, transactionType, priceInUsd, amount, bot) ` +
            `Values ('${symbol}', '${transactionType}', ${price}, ${amount}, ${this.id})`);
    }
    
    async AddCommissionFee(symbol, amount){
        var conn = await this.GetConnection();
    
        return await this.query(conn.run, `INSERT INTO commissionsPaid (symbol, amount, bot) Values ('${symbol}', ${amount}, ${this.id})`);
    }

//#region Klines

async GetLatestKline(timeFrame, symbol, num){
    var conn = await this.GetConnection();
    if(!num)
        num = 1;

    var q = `Select * from [${timeFrame}Klines] where symbol = '${symbol}' Order By date DESC LIMIT ${num}`;
    return await this.query(conn.all, q);
}

//#endregion

//#region Orders 
    async AddOrderToDatabase(id, amount, price, symbol, origId, side, bound)
    {
        var conn = await this.GetConnection();
    
        return await this.query(conn.run, `INSERT INTO orders (id, amount, pricePaying, condition, symbol, origClientOrderId, side, bound, bot) `+
         `Values (${id}, ${amount}, ${price}, 'PENDING', '${symbol}', '${origId}', '${side}', ${(bound ? bound : null)}, ${this.id})`);
    }
    
    async GetOrderFromDatabase(orderId)
    {
        var conn = await this.GetConnection();
        //console.log(orderId);
        return await this.query(conn.get, `SELECT * FROM orders Where id = ${orderId} AND bot = ${this.id} ORDER BY timestamp DESC LIMIT 1`);
    }

    async GetTotalOfOrdersInDatabase()
    {
        var conn = await this.GetConnection();

        var os = await this.query(conn.all, `Select * from orders where bot = ${this.id} AND condition = 'FILLED' ORDER BY timestamp ASC`);

        var sum = 0;
        os.forEach(v => {
            if(v.side == "BUY")
                sum -= v.pricePaying * v.amount; 
            else
                sum += v.pricePaying * v.amount;
        });

        return sum;
    }

    async GetLastOrderFromDatabase(symbol)
    {
        var conn = await this.GetConnection();
    
        return await this.query(conn.get, `SELECT * FROM orders Where symbol = '${symbol}' AND bot = ${this.id} ORDER BY timestamp DESC LIMIT 1`);
    }

    async GetLastFilledOrderFromDatabase(symbol)
    {
        var conn = await this.GetConnection();
    
        return await this.query(conn.get, `SELECT * FROM orders Where condition = 'FILLED' AND symbol = '${symbol}' AND bot = ${this.id} ORDER BY timestamp DESC LIMIT 1`);
    }

    async GetLastOpenOrderFromDatabase(symbol)
    {
        var conn = await this.GetConnection();
    
        return await this.query(conn.get, `SELECT * FROM orders Where condition = 'PENDING' AND symbol = '${symbol}' AND bot = ${this.id} ORDER BY timestamp DESC LIMIT 1`);
    }
    
    async SetFilledOrder(id)
    {
        var conn = await this.GetConnection();
    
        return await this.query(conn.run, `UPDATE orders SET condition = 'FILLED' WHERE id = ${id}`);
    }
    
    async SetCancelledOrder(id)
    {
        var conn = await this.GetConnection();
    
        return await this.query(conn.run, `UPDATE orders SET condition = 'CANCELED' WHERE id = ${id}`);
    }

    async PlaceANewOrder(q, stopPrice){
        //var querystr = this.QueryString(q);
        var order = await this.api.PlaceANewOrder(q);

        // var api = '/api/v3/order';
        // var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        // var d = await fetch(`${endpoint}${api}?${querystr}&signature=${hmac}`, { 
        //     method: 'POST',
        //     headers: {
        //         "X-MBX-APIKEY": auth.apiKey,
        //         "Content-Type": "application/json",
        //     }
        // });
        //var order = await this.api.PlaceANewOrder(querystr);
        //console.log(order);
        //this.Post(JSON.stringify(order));

        if(order.code)
        {
            this.Post(`We had an error! Will try to correct in another iteration!`);
            await this.LogError('OrderForSymbolAtPrice error: ' + JSON.stringify(order));
            await this.LogError('OrderForSymbolAtPrice error: ' + JSON.stringify(q));
            await this.timer(pollingTime);
            return;
        }

        var p = order.fills && order.fills.length > 0 ? order.fills[0].price : 
            order.price && Number(order.price) > 0? order.price : await this.GetAveragePrice(q.symbol);
    
        await this.AddOrderToDatabase(order.orderId, q.quantity, p, q.symbol, order.clientOrderId, q.side, stopPrice);

        return order;
    }
    
    async OrderForSymbolAtPrice(symbol, side, quantity, price, stopPrice, type){
        var eInfo = this.totalExchangeInfo[symbol];
        var pSize = N.countDecimalsToAOne(eInfo["PRICE_FILTER"].tickSize);
        var qSize = N.countDecimalsToAOne(eInfo["LOT_SIZE"].stepSize);
        price = N.toFixedRoundDown(Number(price), pSize);
        stopPrice = stopPrice ? N.toFixedRoundDown(Number(stopPrice), pSize) : stopPrice;
        quantity = N.toFixedRoundDown(Number(quantity),qSize);
        //console.log(`${quantity} - ${qSize}`);
        
        this.Post(`Making a new order for ${symbol} to ${side} ${quantity} at $${price} ` + 
            (stopPrice ? `and will not go ${(side == "BUY" ? 'higher' : 'lower')} than ${stopPrice}!` : ''));
    
        var q = {symbol:symbol, side:side, type:type, timeInForce: 'GTC', price:price,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime()};
        var order = await this.PlaceANewOrder(q, stopPrice);
    
        if(order)
            await this.ProcessOrder(order.orderId, order.symbol, order.clientOrderId);

        return order;
    }
    
    async OrderForSymbol(symbol, side, quantity){
        quantity = N.toFixedRoundDown(Number(quantity),this.quantitySize);

        var q = {symbol:symbol, side:side, type:'MARKET', quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime()};
        //this.Post(`Making a new order for ${symbol} to ${side} ${quantity} at MARKET PRICE!`);
    
        var p = await this.GetAveragePrice(this.symbol.combinedSymbol);
        var order = await this.PlaceANewOrder(q, p);
        //this.Post("Attempting to process a Market Order!");

        if(order){
            await this.ProcessOrder(order.orderId, order.symbol, order.clientOrderId);
        }

        return order;
    }
    
    async CancellingOrder(symbol){
        var od = await this.GetLastOpenOrderFromDatabase(symbol);
        //this.Post(JSON.stringify(od));
        if(!od)
            return;
        
        await this.ProcessOrder(od.id, od.symbol, od.origClientOrderId);
        var o = await this.GetOrderFromDatabase(od.id);
        //this.Post(JSON.stringify(o));
        if(!o || o.condition == "FILLED")
        {
            this.Post('can not cancel order');
            return;
        }

        var order = await this.api.CancellingOrder(symbol, od.id, od.origClientOrderId);

        if(order.code)
        {
            this.Post(`We had an error! Will try to correct in another iteration!`);
            await this.LogError('CancellingOrder error: ' + JSON.stringify(order));
            await this.LogError('CancellingOrder error: ' + JSON.stringify(q));
            await this.timer(pollingTime);
            return;
        }
        //this.Post(`Cancelling order for ${symbol} to ${order.side} ${order.origQty} at $${order.price}!`);
    
        await this.SetCancelledOrder(o.id);
        await this.timer(pollingTime);
    }
    async Buy(){
        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        if(order)
        {
            await this.CancellingOrder(this.symbol.combinedSymbol);
        }

        var price = await this.GetAveragePrice(this.symbol.combinedSymbol);
        var usdt = await this.GetBalance(this.symbol.secondarySymbol);

        var quantity = usdt/price;

        return await this.BuyForSymbol(this.symbol.combinedSymbol, quantity);
    }

    async Sell(){
        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        if(order)
        {
            await this.CancellingOrder(this.symbol.combinedSymbol);
        }

        var usdt = await this.GetBalance(this.symbol.primarySymbol);

        return await this.SellForSymbol(this.symbol.combinedSymbol, usdt);
    }
    
    async BuyForSymbol(symbol, quantity){
        return await this.OrderForSymbol(symbol, "BUY", quantity);
    }
    
    async SellForSymbol(symbol, quantity){
        return await this.OrderForSymbol(symbol, "SELL", quantity);
    }
        
    async GetOrder(symbol, orderId, origId){
        // var q = {symbol: symbol, origClientOrderId:origId, orderId:orderId, timestamp: Date.now() - this.serverTimeAdjust};
    
        // var querystr = this.QueryString(q);
        // var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        // var d = await fetch(`${endpoint}/api/v3/order?${querystr}&signature=${hmac}`, { 
        //     method: 'GET',
        //     headers: {
        //         "X-MBX-APIKEY": auth.apiKey
        //     }
        // });
        // return await d.json();

        return await this.api.GetOrder(symbol, orderId, origId);
    }

    //#endregion

//#region Pricing and Balances
    async GetBalances(){
        // var q = { timestamp: Date.now() - this.serverTimeAdjust };
    
        // var querystr = this.QueryString(q);
        // var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        // var d = await fetch(`${endpoint}/sapi/v1/capital/config/getall?${querystr}&signature=${hmac}`, { 
        //     method: 'GET',
        //     headers: {
        //         "Content-Type":"application/json",
        //         "X-MBX-APIKEY": auth.apiKey
        //     }
        // });
        // var j = await d.json();
        // if(j.code)
        // {
        //     this.Post(`We had an error! Will try to correct in another iteration!`);
        //     await this.LogError('GetBalances error: ' + JSON.stringify(j));
        //     await this.timer(pollingTime);
        //     return null;
        // }
        // var js = {};
        // for(var i = 0; i < j.length; i++){
        //     var s = j[i];
        //     js[s.coin] = s;
        // }
    
        // var commission = 0;
        // var con = await this.GetConnection();
        // var c = await this.query(con.all, `Select * from commissionsPaid where bot = ${this.id}`);
        // for(var w = 0; w < c.length; w++){
        //     commission += c[w].amount;
        // }
        // js["commissions"] = commission;
    
        // this.balances = js;
    
        //Post(this.balances);

        var js = await this.api.GetBalances();
        //console.log(this.balances);
        // var q = { timestamp: Date.now() - this.serverTimeAdjust };
    
        // var querystr = this.QueryString(q);
        // var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        // var d = await fetch(`${endpoint}/sapi/v1/capital/config/getall?${querystr}&signature=${hmac}`, { 
        //     method: 'GET',
        //     headers: {
        //         "Content-Type":"application/json",
        //         "X-MBX-APIKEY": auth.apiKey
        //     }
        // });
        // var j = await d.json();
        // if(j.code)
        // {
        //     this.Post(`We had an error! Will try to correct in another iteration!`);
        //     await this.LogError('GetBalances error: ' + JSON.stringify(j));
        //     await this.timer(pollingTime);
        //     return null;
        // }
        // var js = {};
        // for(var i = 0; i < j.length; i++){
        //     var s = j[i];
        //     js[s.coin] = s;
        // }
    
        var commission = 0;
        var con = await this.GetConnection();
        var c = await this.query(con.all, `Select * from commissionsPaid where bot = ${this.id}`);
        for(var w = 0; w < c.length; w++){
            commission += c[w].amount;
        }
        js["commissions"] = commission;
    
        this.balances = js;
    
        //Post(this.balances);
        return this.balances;
    }
    
    async GetBalance(symbol, fromBalances){
        if(!this.balances)
            await this.GetBalances();

        if(!fromBalances && this.HasAllowances){
            var ba = await this.GetBotAllowance(symbol);
            var b = await this.GetBalance(symbol, true);
            if(ba && ba.allowance <= b)
            {
                return ba.allowance;
            }
            else
                return 0;
        }
        var balance = this.balances[symbol];
        if(balance){
            if(balance.free)
                return Number.parseFloat(balance.free) + Number.parseFloat(balance.locked) + Number.parseFloat(balance.freeze) + Number.parseFloat(balance.storage);
            else
                return Number(balance);
        }
        else 
            return 0;
    }
    
    async GetLatestPrice(symbol){
        return this.api.GetLatestPrice(symbol);
        // var d = await fetch(`${endpoint}/api/v3/ticker/price?symbol=${symbol}`);
        // var j = await d.json();
    
        // //await this.AddPriceToTrends(symbol, j.price);
    
        // return j.price;
    }

    async AddLatestPriceToTrends(symbol){
        var price = await this.GetLatestPrice(symbol);
        var conn = await this.GetConnection();

        var g = await this.query(conn.get, `Select * from trends where symbol = '${symbol}' ORDER BY timestamp DESC LIMIT 1`);

        if(g.price == price)
            return;
    
        await this.query(conn.run, `INSERT INTO trends (symbol, price) ` +
            `Values ('${symbol}', ${price})`);
    }
    
    async GetLatestPriceInUSDForSymbol(symbol){
        return this.api.GetLatestPriceInUSDForSymbol(symbol);
        // var d = await fetch(`${endpoint}/api/v3/ticker/price?symbol=${symbol}USD`);
        // var j = await d.json();
    
        // //await this.AddPriceToTrends(symbol, j.price);
    
        // return j.price;
    }
    
    async GetAveragePrice(symbol){
        return this.api.GetAveragePrice(symbol);
        // var d = await fetch(`${endpoint}/api/v3/avgPrice?symbol=${symbol}`);
        // var j = await d.json();
    
        // var o = j.price;
    
        // //await this.AddPriceToTrends(symbol, o);
    
        // return o;
    }
    //#endregion
    
    //#region Messages

    async CurrentAction(){
        var last = await this.GetLastPrice(this.symbol.primarySymbol);
        return last.transactionType == 'BUY' ? 'SELL' : 'BUY';
    }
    
    QueryString(obj){
        return Object.keys(obj).map((key) => {
            return `${key}=${obj[key]}`;
        }).join('&');
    }

    ClearMessages(){
        this.topMessages = [];
        this.postingMessages = [];
        this.sendEmail = false;
    }
    
    Post(str){
        this.topMessages.push(`${this.id} ${this.symbol.combinedSymbol} - ${str}`);
    }
    
    async LogError(error)
    {
        this.Post(error);
        var conn = await this.GetConnection();
    
        return await this.query(conn.run, `INSERT INTO errors (errorMsg) ` +
            `Values ('${error.replace(/;/g, "|").replace(/'/g, "\"")}')`);
    }

    //#endregion

}

module.exports = BaseBotClass;