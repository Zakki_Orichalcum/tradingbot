const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const Arrays = new A();
const Timing = require("../utilities").Timing;

class RSIBot extends BaseBotClass {
    constructor(id, symbol, watchingSymbol, percentchange, watchTime, limit, exchangeInfo, serverTime)
    {
        super(id, symbol, limit, exchangeInfo, serverTime, true);
        this.watchedSymbol = watchingSymbol;
        this.timeWatching = watchTime;
        this.amountChangePercent = percentchange;
        this.watchUntil = -1;
        this.watchedPricing = [];
        this.startingPrice = -1;
    }

    async Initialize(){
        super.Initialize();

        this.watchUntil = Date.now + Timing.GetMilliseconds(this.timeWatching);
        this.startingPrice = await this.GetLatestPrice(this.watchedSymbol.combinedSymbol);
    }

    async run()
    {
        super.run();
        
        if(Date.now >= this.watchUntil){
            var min = math.min(this.watchedPricing);
            var max = math.max(this.watchedPricing);

            if(this.startingPrice < max * (1.0 - this.amountChangePercent)){
                await this.Buy();
            }
            else if(this.startingPrice > min * (1.0 + this.amountChangePercent)){
                await this.Sell();
            }
            
            this.watchUntil = Date.now + Timing.GetMilliseconds(this.timeWatching);
            this.startingPrice = await this.GetLatestPrice(this.watchedSymbol.combinedSymbol);
        }
        else{
            var p = await this.GetLatestPrice(this.watchedSymbol.combinedSymbol);
            this.watchedPricing.push(p);
        }

        await this.ProcessOrder(this.symbol.combinedSymbol);

        await this.PostLooking();
    }

    getRSISignal()
    {
        var data = this.GetData(this.amount+1);
        var prev = Arrays.range(data, 0, data.length-2);
        var current = Arrays.range(data, 1, data.length-1);

        var pRSI = this.RSI(prev);
        var cRSI = this.RSI(current);

        if(pRSI > 30 && cRSI < 30)
        {
            return 1;
        }
        else if(pRSI < 70 && cRSI > 70){
            return -1;
        }
        else
            return 0;
    }

    async Buy(){
        var price = await this.GetAveragePrice(this.symbol.combinedSymbol);
        var usdt = await this.GetBalance(this.symbol.secondarySymbol);

        var quantity = usdt/price;

        await this.BuyForSymbol(this.symbol.combinedSymbol, quantity);
    }

    async Sell(){
        var usdt = await this.GetBalance(this.symbol.primarySymbol);

        await this.SellForSymbol(this.symbol.combinedSymbol, usdt);
    }
        

    async PostLooking(obj){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - MACDBot ${side} Current ${obj.current} | Prev ${obj.previous} | ${obj.signal} |`+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }
}
module.exports = RSIBot;