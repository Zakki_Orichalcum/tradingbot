const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const Arrays = new A();
const Timing = require("../utilities").Timing;

class WatcherBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.watchedSymbol = symbols[args.watchedSymbol];
        this.timeWatching = args.watchTime;
        this.amountChangePercent = args.percentchange;
        this.watchUntil = -1;
        this.watchedPricing = [];
        this.beginningPrice = -1;
    }

    async Initialize(){
        await super.Initialize();

        this.watchUntil = Date.now + Timing.GetMilliseconds(this.timeWatching);
        this.beginningPrice = await this.GetLatestPrice(this.watchedSymbol.combinedSymbol);
    }

    async run()
    {
        super.run();
        
        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);

        if(!order){
            if(Date.now >= this.watchUntil){
                var min = math.min(this.watchedPricing);
                var max = math.max(this.watchedPricing);
    
                if(this.beginningPrice < max * (1.0 - this.amountChangePercent)){
                    await this.Buy();
                }
                else if(this.beginningPrice > min * (1.0 + this.amountChangePercent)){
                    await this.Sell();
                }
                
                this.watchUntil = Date.now + Timing.GetMilliseconds(this.timeWatching);
                this.beginningPrice = await this.GetLatestPrice(this.watchedSymbol.combinedSymbol);
            }
            else{
                var p = await this.GetLatestPrice(this.watchedSymbol.combinedSymbol);
                this.watchedPricing.push(p);
            }
        }
        else{
            await this.ProcessOrder(order.id, order.symbol, order.origClientOrderId);
        }

        await this.PostLooking();
    }

    async Buy(){
        if(await this.CurrentAction() != "BUY")
            return;

        var price = await this.GetAveragePrice(this.symbol.combinedSymbol);
        var usdt = await this.GetBalance(this.symbol.secondarySymbol);

        var quantity = usdt/price;

        await this.BuyForSymbol(this.symbol.combinedSymbol, quantity);
    }

    async Sell(){
        if(await this.CurrentAction() != "SELL")
            return;

        var usdt = await this.GetBalance(this.symbol.primarySymbol);

        await this.SellForSymbol(this.symbol.combinedSymbol, usdt);
    }

    async PostLooking(){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
        var min = this.watchedPricing.length > 0 ? math.min(this.watchedPricing) : 0;
        var max = this.watchedPricing.length > 0 ? math.max(this.watchedPricing) : 0;
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - WatcherBot ${side} Current ${this.watchedSymbol.combinedSymbol} $${Number(this.beginningPrice).toFixed(4)} `
            +`| Min $${Number(min).toFixed(4)} Max $${Number(max).toFixed(4)} | `+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }
}
module.exports = WatcherBot;