const N = require("../utilities").Numbers;
const dateformat = require('dateformat');
const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const Arrays = new A();
const Timing = require("../utilities").Timing;
const Algorithms = require("../utilities/Algorithms");

class ShortBot extends BaseBotClass{
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime);
        this.timeIncrement = args.timeIncrement;
        this.numOfPeriods = args.numOfPeriods;
        this.lastTrend = 0;
    }

    async run(){
        super.run();

        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);

        //await this.AddLatestPriceToTrends(this.symbol.combinedSymbol);
    
        if(!order)
        {
            var points = [];
            var datetime = new Date().getTime();
            var conn = await this.GetConnection();
            var q = `Select * from [5mKlines] where symbol = '${this.symbol.combinedSymbol}' Order by date DESC LIMIT 2`;
            var arr = await this.query(conn.all, q);
            // var increments = Timing.GetMilliseconds(this.timeIncrement);
            // var n = this.numOfPeriods*2+3;
            // for(var i = 0; i < n; i++){
            //     var v = await this.GetValues(datetime-(increments*(i+1)), datetime-(increments*i));
            //     if(!v)
            //         break;
            //     points.push(v);
            // }

            // points = points.reverse();

            // if(points.length < n)
            //     return;
            

            // var 
            // //var prev = this.ADX(Arrays.Range(points, 0, this.numOfPeriods*2+2), this.numOfPeriods);
            // var adx = Algorithms.ADX(points, this.numOfPeriods);

            if(arr.length < 2)
                return;
            
            var prev = arr[1];
            var current = arr[0];

            if(!current.adx && !prev.adx)
                return;
    
            var currentAction = await this.CurrentAction();

            if(prev.adx < 25 && current.adx > 25)
            {
                if(currentAction == 'BUY' && current.diplus > current.diminus){
                    this.Post("Trying to BUY ");
                    await this.Buy();
                }
                else if(currentAction == 'SELL' && current.diplus < current.diminus){
                    this.Post("Trying to sell ");
                    await this.Sell();
                }
            }
            
            var currentPrice = N.toFixedRoundDown(await this.GetLatestPrice(this.symbol.combinedSymbol), this.priceSize);
            var primary = await this.GetBalance(this.symbol.primarySymbol);
            var second = await this.GetBalance(this.symbol.secondarySymbol);
            var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
        
            var date = new Date();
            this.postingMessages.push(`${date.toLocaleString()} - ShortBot ${currentAction} | pADX: ${prev.adx} cADX: ${current.adx} +DI: ${current.diplus} -DI: ${current.diminus} | `+
                `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
                `Total proposed: $${total}`);
        }
        else if(order){
            await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                    order.symbol, 
                    order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
            
            var currentPrice = N.toFixedRoundDown(await this.GetLatestPrice(this.symbol.combinedSymbol), this.priceSize);
            var primary = await this.GetBalance(this.symbol.primarySymbol);
            var second = await this.GetBalance(this.symbol.secondarySymbol);
            var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
        
            var date = new Date();
            var p = order.pricePaying ? N.toFixedRoundDown(Number(order.pricePaying), this.priceSize) : N.toFixedRoundDown(Number(order.price), this.priceSize);
            this.postingMessages.push(`${date.toLocaleString()} - ShortBot ${order.side} | Processing Order: ${p} `+
                `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
                `Total proposed: $${total}`);
        }
    }

    async Buy(){
        var avg = await this.GetAveragePrice(this.symbol.combinedSymbol);
        var usdt = await this.GetBalance(this.symbol.secondarySymbol);

        var quantity = usdt/avg;

        //this.Post("Trying to buy " + quantity);
        //await this.OrderForSymbolAtPrice(this.symbol.combinedSymbol, "BUY", quantity, price, price, "LIMIT");
        await this.BuyForSymbol(this.symbol.combinedSymbol, quantity);
    }

    async Sell(){
        var avg = await this.GetAveragePrice(this.symbol.combinedSymbol);
        var usdt = await this.GetBalance(this.symbol.primarySymbol);

        //this.Post("Trying to sell " + usdt);
        //await this.OrderForSymbolAtPrice(this.symbol.combinedSymbol, "SELL", usdt, price, price, "LIMIT");
        await this.SellForSymbol(this.symbol.combinedSymbol, usdt);
    }
    
    //#region TRENDS
    
    async GetAverageFromPrices(symbol)
    {
        var conn = await this.GetConnection();
    
        var points = await this.query(conn.all, `Select price from trends where symbol = '${symbol}' ORDER BY timestamp DESC LIMIT 200`);
        var sum50 = 0; var sum200 = 0;
        points.forEach((element, i) => {
            if(i < 50)
                sum50 += element.price;
            
            sum200 += element.price;
        });
    
        var avg50 = sum50/50;
        var avg200 = sum200/200;

        this.Post(`avg50 ${avg50} avg200 ${avg200} | ${avg50 > avg200 ? 1 : avg50 < avg200 ? -1 : 0}`);
    
        return avg50 > avg200 ? 1 : avg50 < avg200 ? -1 : 0;
    }
    
    //#endregion

    async GetCurrentTrend()
    {   
        var points = [];
        var datetime = new Date().getTime();
        var increments = Timing.GetMilliseconds(this.timeIncrement);
        var n = this.numOfPeriods*2+1;
        for(var i = 0; i < n; i++){
            points.push(await this.GetValues(datetime-(increments*i)), datetime-(increments*(i+1)));
        }
        
        // var prev = this.ADX(Arrays.Range(points, 1, points.length), this.numOfPeriods);
        // var current = this.ADX(Arrays.Range(points, 0, points.length-1), this.numOfPeriods);

        var currentAction = await this.CurrentAction();
        if(output >= 0 && this.lastTrend < 0 && currentAction == "BUY"){
            price = math.min(arr);
        }
        else if(output <= 0 && this.lastTrend > 0 && currentAction == "SELL"){
            price = math.max(arr);
        }
        this.lastTrend = action != 0 ? action : this.lastTrend;
    
        return { result: output, price: price };
    }

    async GetValues(startTime, endTime)
    {
        var conn = await this.GetConnection();

        var start = new Date(startTime);
        var end = new Date(endTime);
        var q = `SELECT price FROM trends where symbol = '${this.symbol.combinedSymbol}' AND timestamp >= '${dateformat(start, 'yyyy-mm-dd HH:MM:ss', true)}' `+
            `AND timestamp <= '${dateformat(end, 'yyyy-mm-dd HH:MM:ss', true)}' ORDER BY timestamp ASC`;
        var points = await this.query(conn.all, q);
        //console.log(q, JSON.stringify( points));
        if(points.length == 0)
            return null;
        var arr = Arrays.MakeListFromObjects(points, "price");
        

        var output = {
            open: arr[0],
            high: math.max(arr),
            low: math.min(arr),
            close: arr[arr.length-1]
        };

        return output;
    }
    
    //#region Messages

    async PostLooking(obj){
        var side = await this.CurrentAction();
        var currentPrice = N.toFixedRoundDown(await this.GetLatestPrice(this.symbol.combinedSymbol), this.priceSize);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        var p = N.toFixedRoundDown(Number(obj.price), this.priceSize);
        this.postingMessages.push(`${date.toLocaleString()} - ${side} | price: ${p} trend: ${this.lastTrend} result: ${obj.result} | Current Price: $${currentPrice} `+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }

    //#endregion

}

module.exports = ShortBot;