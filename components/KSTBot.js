const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const { Timing } = require('../utilities');
const Arrays = new A();
const day = 24 * 60 * 60 * 1000;

class KSTBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.timingInformation = args.timingInfo;
    }

    async run()
    {
        super.run();

        var lasts = await this.GetLatestKline(Timing.TimingObjectToString(this.timingInformation), this.symbol.combinedSymbol, 21);
        //console.log(lasts);
        if(lasts.length >= 2)
        {
            var prev = lasts[1];
            var cur = lasts[0];
    
            if(prev.kSignal && cur.kSignal)
            {
                var currentAction = await this.CurrentAction();

                // if(currentAction == 'SELL' && prev.kst < prev.kSignal && cur.kst > cur.kSignal){
                //     await this.Sell();
                // }
                // else if(currentAction == 'BUY' && prev.kst > prev.kSignal && cur.kst < cur.kSignal){
                //     await this.Buy();
                // }
        
                if(currentAction == 'BUY' && prev.kst < prev.kSignal && cur.kst > cur.kSignal){
                    await this.Buy();
                }
                else if(currentAction == 'SELL' && prev.kst > prev.kSignal && cur.kst < cur.kSignal){
                    await this.Sell();
                }
        
                var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
                if(order){
                    await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                        order.symbol, 
                        order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
                }
            }

            await this.PostLooking(prev, cur);
            return;
        }
        

        var date = new Date();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
        var sum = await this.GetTotalOfOrdersInDatabase();
        
        this.postingMessages.push(
            `${date.toLocaleString()} - ${this.id} KSTBot  (${Number(sum).toFixed(4)}) Waiting on more information | `+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }

    async PostLooking(prev, cur){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
        var sum = await this.GetTotalOfOrdersInDatabase();
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - KSTBot ${side} (${Number(sum).toFixed(4)}) ROC10 ${Number(cur.roc10).toFixed(4)} ROC15 ${Number(cur.roc15).toFixed(4)} ROC20 ${Number(cur.roc20).toFixed(4)} ROC30 ${Number(cur.roc30).toFixed(4)} KST ${Number(cur.kst).toFixed(4)} Signal ${Number(cur.kSignal).toFixed(4)} | `+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }
}
module.exports = KSTBot;