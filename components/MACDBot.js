const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const Algorithms = require('../utilities/Algorithms');
const Arrays = new A();
const day = 24 * 60 * 60 * 1000;

class MACDBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.short = args.short;
        this.long = args.long;
        this.signalLongLength = args.signalLength;
        this.column = args.column;
        this.checkLines = true;
        this.previousMACD = null;
        this.macdAmount = args.macdAmount;
    }

    async Initialize(){
        super.Initialize();

        //await this.SetupKlines(this.long * 2);
    }

    async run()
    {
        super.run();     

        var last = await this.GetKlines(this.symbol.combinedSymbol, 1);
        if(last[last.length-1].date + (day) < await this.GetServerTime())
        {
            this.checkLines = true;
            //await this.SetupKlines(this.long+this.signalLongLength);//The 1 is to make sure to capture the 20th day based on how we are doing timestamps
        }

        if(this.checkLines)
        {
            var d = await this.GetData();
            this.previousMACD = await this.RunEMAs(d.data, d.emaData, d.long, d.short);

            var currentAction = await this.CurrentAction();
        
            if(currentAction == 'BUY' && this.previousMACD.signal > 0){
                await this.Buy();
            }
            else if(currentAction == 'SELL' && this.previousMACD.signal < 0){
                await this.Sell();
            }

            this.checkLines = false;
        }
        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        if(order){
            await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                order.symbol, 
                order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
        }

        await this.PostLooking(this.previousMACD);
    }

    async GetData(){
        var quotes = await this.GetKlines(this.symbol.combinedSymbol, this.long + this.signalLongLength);
    
        quotes = this.getMvgAvg(quotes,this.short,'MvgAvgShort');       //12 point moving average
        quotes = this.getMvgAvg(quotes,this.short,'MvgAvgLong');        //26 point moving average
        quotes = this.getMACDSignal(quotes);
        quotes = this.getMACDMvgAvg(quotes,this.signalLongLength);
        quotes = this.getMACDTrigger(quotes);

        return {data: data, emaData: emaData, short:shortSMAData, long:longSMAData};
    }
    
    async RunEMAs(data, emaData, longData, shortData){
        //console.log(data, longData, shortData);
        // use first <long/short> # of points to start the EMA since it depends on previous EMA
        var longSMA = Algorithms.SMA(longData);
        var shortSMA = Algorithms.SMA(shortData);
        var longEMA = [longSMA];
        var shortEMA = [shortSMA];

        // need to remove these values at the end
        for( var i = 1; i < this.long; i++){
            var v = emaData[i];
            longEMA.push(Algorithms.EMA(this.long, v[this.column], longEMA[longEMA.length-1]));
        }

        for( var i = 1; i < this.short; i++){
            var v = emaData[i];
            shortEMA.push(Algorithms.EMA(this.short, v[this.column], shortEMA[shortEMA.length-1]));
        }

        for( var i = 1; i < data.length; i++){
            var v = data[i];
            longEMA.push(Algorithms.EMA(this.long, v[this.column], longEMA[longEMA.length-1]))
            shortEMA.push(Algorithms.EMA(this.short, v[this.column], shortEMA[shortEMA.length-1]))
        }

        shortEMA.splice(0, this.long - this.short);

        var macd = [];
        for( var i = 0; i < shortEMA.length; i++){
            macd.push(shortEMA[i] - longEMA[i]);
        }
        //console.log(data, shortEMA, longEMA, macd);

        // use the first N values to start signal line EMA calc
        var macSign = [];
        for( var i = 0; i < this.signalLongLength; i++){
            macSign.push(macd[i]);
        }

        var signalLineSma = Algorithms.SMA(macSign);
        var longSignal = [signalLineSma];
        for( var i = this.signalLongLength+1; i < macd.length; i++){
            longSignal.push(Algorithms.EMA(this.signalLongLength, macd[i], longSignal[longSignal.length-1]));
        }
        // remove first entry in signal since it was only used to start calc
        longSignal.splice(0, 1);
        // remove the first few values of macd/short/long emas to catch up with signal/data
        macd.splice(0, macd.length - this.signalLongLength);
        shortEMA.splice(0, shortEMA.length - this.signalLongLength);
        longEMA.splice(0, longEMA.length - this.signalLongLength);

        var diffs = [];
        for( var i = 0; i < macd.length; i++){
            diffs.push(macd[i] - longSignal[i]);
        }

        var buyLines = [];
        for( var i = `0`; i < diffs.length; i++){

            if(diffs[i-1] < 0 && diffs[i] > 0)
                buyLines.push(1);
            else if(diffs[i-1] > 0 && diffs[i] < 0)
                buyLines.push(-1);
            else
                buyLines.push(0);
        }

        this.PostTable(data, macd, longEMA, shortEMA, buyLines);

        var signal = 0;
        var current = diffs[diffs.length-1];
        var prev = diffs[diffs.length-2];
        if(diffs[i-1] < 0 && diffs[i] > 0)
            signal = 1;
        else if(diffs[i-1] > 0 && diffs[i] < 0)
            signal = -1;

        return {macd: macd, shortEMA: shortEMA, longEMA: longEMA, signal: signal, current: current, previous: prev};
    }

    async Buy(){
        var price = await this.GetAveragePrice(this.symbol.combinedSymbol);
        var usdt = await this.GetBalance(this.symbol.secondarySymbol);

        var quantity = usdt/price;

        await this.BuyForSymbol(this.symbol.combinedSymbol, quantity);
    }

    async Sell(){
        var usdt = await this.GetBalance(this.symbol.primarySymbol);

        await this.SellForSymbol(this.symbol.combinedSymbol, usdt);
    }



    getMvgAvg(quotes, points, title){
        //Function takes the average of points number of ticks.
    
        //quotes[title] = [];
        var sum = 0;
    
        for(var n = 0; n < points; n++){
            sum = sum + quotes[n].close;
            var mvgAvg = sum / points;
            quotes[n][title] = mvgAvg.toFixed(4);
        }
    
        for(var n = points; n < quotes.length; n++){
            sum = sum - quotes[n-points].close + quotes[n].close;
            var mvgAvg = sum / points;
            quotes[n][title] = mvgAvg.toFixed(4);
        }
    
        return quotes;
    }
    

    getMACDMvgAvg(quotes, points){
        //Function takes the average of points number of ticks.
        var title = 'MACDMvgAvg' + str(points);
        var sum = 0;

        for(var n = this.macdAmount; n < points+this.macdAmount; n++){
            sum = sum + quotes[n].MACD;
            var mvgAvg = sum / points;
            quotes[n][title] = mvgAvg.toFixed(4);
        }
    
        for(var n = points+this.macdAmount; n < quotes.length; n++){
            sum = sum - quotes[n-points].MACD + quotes[n].MACD;
            var mvgAvg = sum / points;
            quotes[n][title] = mvgAvg.toFixed(4);
        }

        return quotes;
    }
    
    getMACDSignal(quotes)
    {
        for(var n = this.macdAmount; n < quotes.length; n++){
            var macd = quotes[n].MvgAvgShort - quotes[n].MvgAvgLong;
            quotes[n][title] = macd.toFixed(4);
        }

        return quotes;
    }

    getMACDTrigger(quotes)
    {
        for(var n = this.macdAmount; n < quotes.length; n++){
            if(quotes[n].MACD > 0){
                quotes[n].MACDTrigger = 1;
            }
            else{
                quotes[n].MACDTrigger = 0;
            }
        }

        return quotes;
    }

    

    Volatility(data, column){
        var arr = Arrays.MakeListFromObjects(data, column);
        return math.std(arr);
    }
        

    async PostLooking(obj){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - MACDBot ${side} Current ${obj.current} | Prev ${obj.previous} | ${obj.signal} |`+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }

    PostTable(data, macd, longEMA, shortEMA, buyLine){
        this.Post(`${"MACD".padStart(20)} | ${"LongEMA".padStart(20)} | ${"ShortEMA".padStart(20)} | ${"Buy/Sell Line".padStart(20)}`);
        this.Post(`${"".padStart(89, "_")}`);
        for(var i = 0; i < buyLine.length; i++){
            this.Post(`${`${macd[i]}`.padStart(20)} | ${`${longEMA[i]}`.padStart(20)} | ${`${shortEMA[i]}`.padStart(20)} | ${`${buyLine[i]}`.padStart(20)}`);
        }
    }
}
module.exports = MACDBot;