const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const Arrays = new A();
const Timing = require("../utilities").Timing;
const N = require("../utilities").Numbers;
const binanceFee = 0.001;


class TriangularArbitrageBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        var ws = {};
        this.arbitrageList = args.arbitrageSymbols;
        var cryptosCaredAbout = {};
        this.arbitrageList.forEach( v => {
            ws[v] = symbols[v];

            if(!cryptosCaredAbout[symbols[v].primarySymbol])
                cryptosCaredAbout[symbols[v].primarySymbol] = 1;
            if(!cryptosCaredAbout[symbols[v].secondarySymbol])
                cryptosCaredAbout[symbols[v].secondarySymbol] = 1;
        });
        this.cryptos = Object.keys(cryptosCaredAbout);
        this.watchedSymbols = ws;
        this.useFee = args.useFee;
        this.tradeLevel = args.tradeLevel ? args.tradeLevel : 0;
    }

    
    timer(ms){ return new Promise( res => setTimeout(res, ms)) };

    async run()
    {
        super.run();
        //starting with USDT
        //var usdt = await this.GetBalance("USDT"); //Temp
        var usdt = 100;

        var d = {};
        var keys = Object.keys(this.watchedSymbols);
        for(var i = 0; i < keys.length; i++)
        {
            d[keys[i]] = await this.GetAveragePrice(keys[i]);
        }

        var forwardDirection = await this.ArbitragePath(true);
        var backwardDirection = await this.ArbitragePath(false);

        if(forwardDirection.diff > backwardDirection.diff && forwardDirection.diff > this.tradeLevel)
        {
            await this.Arbitrage(forwardDirection, true);
        }
        else if(backwardDirection.diff > this.tradeLevel)
        {
            await this.Arbitrage(backwardDirection, false);
        }

        await this.PostLooking(d, forwardDirection, backwardDirection);
    }

    async ProcessOrder(orderId, symbol, origClientOrderId){
        var o = await this.GetOrder(symbol, orderId, origClientOrderId);
        //console.log(o);
        if(o)
        {
            var od = await this.GetOrderFromDatabase(o.orderId);
    
            if(o.status == "FILLED" && od.condition == 'PENDING'){
                var ws = this.watchedSymbols[symbol];
                if(o.fills)
                {
                    for(var i = 0; i < o.fills.length; i++){
                        var fill = o.fills[i];
        
                        var price = fill.price ? fill.price : await this.GetAveragePrice(symbol);
                        //this.Post(price);
        
                        await this.SetTransaction(ws.primarySymbol, o.side, 
                            fill.qty, await this.GetLatestPriceInUSDForSymbol(ws.primarySymbol));
            
                        await this.AddCommissionFee(fill.commissionAsset, fill.commission);
    
                        this.priceAction = price;
                    }
                }
                else{
                    var price = o.price ? o.price : await this.GetAveragePrice(symbol);
                    if(o.side == "BUY"){
                        var usd = Number.parseFloat(o.executedQty)*binanceFee;
                        await this.AddCommissionFee(ws.primarySymbol, usd);

                        await this.SetTransaction(ws.primarySymbol, "BUY", 
                            o.executedQty-usd, await this.GetLatestPriceInUSDForSymbol(ws.primarySymbol));

                        var usdt = Number.parseFloat(o.cummulativeQuoteQty);
            
                        this.Post(`${usdt} - ${o.executedQty-usd}`);
                    }
                    else{
                        var usdt = Number.parseFloat(o.cummulativeQuoteQty) - (Number.parseFloat(o.cummulativeQuoteQty)*binanceFee);

                        await this.SetTransaction(ws.primarySymbol, "SELL", 
                            o.executedQty, await this.GetLatestPriceInUSDForSymbol(ws.primarySymbol));
                    }

                    this.priceAction = price;
                }
    
                
                if(o.side == "BUY"){
                    var usdt = Number.parseFloat(o.cummulativeQuoteQty);
                    await this.SetTransaction(ws.secondarySymbol, "SELL", 
                        usdt, await this.GetLatestPriceInUSDForSymbol(ws.secondarySymbol));
                }
                else{
                    var com = (Number.parseFloat(o.cummulativeQuoteQty)*binanceFee);
                    var usdt = Number.parseFloat(o.cummulativeQuoteQty) - com;
                    this.Post(`${usdt} - ${o.executedQty}`);
                    await this.SetTransaction(ws.secondarySymbol, "BUY", 
                        usdt, await this.GetLatestPriceInUSDForSymbol(ws.secondarySymbol));

                    await this.AddCommissionFee(ws.secondarySymbol, com);

                    this.Post(`${usdt} - ${o.executedQty}`);
                }
        
                await this.SetFilledOrder(orderId);
                //await this.timer(3 * pollingTime);
                
                if(!await this.GetBalances())
                    return false;
        
                var currentPrice = await this.GetLatestPrice(ws.combinedSymbol);
                var total = (currentPrice * await this.GetBalance(ws.primarySymbol) + await this.GetBalance(ws.secondarySymbol)
                            - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
                this.reductionAmount = 1;
                this.nextTimeToStartReducing = 0;
                //this.Post(`Current Records: ${ws.primarySymbol}: ${await this.GetBalance(ws.primarySymbol)}, ${ws.secondarySymbol}: ${await this.GetBalance(ws.secondarySymbol)}, Commission Paided: ${this.balances["commissions"]} | Running Total: $${total}`);
        
                return true;
            }
            else if(o.status == "CANCELED" && od.condition == 'PENDING')
            {
                await this.SetCancelledOrder(od.id);
            }
        }
        
        return false;
    }

    async Arbitrage(direction, isGoingForward)
    {
        for(var i = 0; i < this.arbitrageList.length; i++)
        {
            var p = isGoingForward ? i : this.arbitrageList.length-1 - i;
            var tryTime = 0;
            var symbol = this.arbitrageList[p];
            var price = direction.pricesPath[i];
            var quan = direction.quantities[i];
            var am = direction.amounts[i];
            //this.Post(price);
            var buy = direction.buySellPath[i];
            var e = this.watchedSymbols[symbol];
            var order = null; var quantity = 0;
            if(buy)
            {
                //console.log("Is Buying");
                var usdt = await this.GetBalance(e.secondarySymbol);

                quantity = usdt/price;

                order = await this.OrderForSymbolAtPrice(symbol, "BUY", quantity, price, null, "LIMIT");
            }
            else
            {
                quantity = await this.GetBalance(e.primarySymbol);

                order = await this.OrderForSymbolAtPrice(symbol, "SELL", quantity, price, null, "LIMIT");
            }
            this.Post(am + ' - ' + quan + " T");

            this.Post(JSON.stringify(order));
            while(order && order.status != "FILLED" && tryTime++ < 50)
            {
                await this.timer(500);
                order = await this.GetOrder(symbol, order.orderId, order.clientOrderId ? order.clientOrderId : order.origClientOrderId);
            }

            if(tryTime >= 50)
                break;

            if(order)
                await this.ProcessOrder(order.orderId, symbol, order.clientOrderId ? order.clientOrderId : order.origClientOrderId);
        }
    }

    async ArbitragePath(isGoingForward)
    {
        var allowances = await this.GetBotAllowances();
        var startingSum = await this.GetTotal(allowances);
        //console.log(`start pathing`);
        var currentCrypto = "USDT";
        var path = []; var prices = []; var sum = 0; var quantities = []; var amounts = [];
        for(var i = 0; i < this.arbitrageList.length; i++)
        {
            var symbol = isGoingForward ? this.arbitrageList[i] : this.arbitrageList[this.arbitrageList.length-1-i];
            var price = await this.GetAveragePrice(symbol);
            //console.log(eInfo);
            //console.log(price, symbol);
            var amount = allowances[currentCrypto];
            var e = this.watchedSymbols[symbol];
            price = N.toFixedRoundDown(Number(price), e.priceSize);
            var q = 0; var asum = 0;
            if(e.secondarySymbol == currentCrypto)
            {
                currentCrypto = e.primarySymbol;
                q = (amount * (1/price));
                asum = N.toFixedRoundDown(Number(q), e.quantitySize);
                var s = asum * price;
                quantities.push(asum);
                amounts.push(s);
                allowances[e.secondarySymbol] -= s;
                allowances[e.primarySymbol] += asum - (this.useFee ? (binanceFee * asum) : 0);
                path.push(true); //BUYing
            }
            else
            {
                currentCrypto = e.secondarySymbol;
                q = amount * (price);
                asum = N.toFixedRoundDown(Number(q), e.quantitySize);
                var s = asum * (1/price);
                amounts.push(asum - (this.useFee ? (binanceFee * asum) : 0));
                quantities.push(s);
                allowances[e.secondarySymbol] += asum - (this.useFee ? (binanceFee * asum) : 0);
                allowances[e.primarySymbol] -= s
                path.push(false); //SELLing
            }
            //this.Post(`${symbol} - ${currentCrypto} ${(path[path.length-1] ? "BUY" : "SELL")} - ${price} - ${q} - ${asum} - ${e.priceSize} - ${e.quantitySize}`);
            prices.push(price);
            //console.log(sum);
        }

        var sum = await this.GetTotal(allowances);
        this.Post(`Starting Point ${startingSum} | Ending Point ${sum}`);

        return { diff: sum - startingSum, buySellPath: path, pricesPath: prices, quantities: quantities, amounts:amounts };
    }

    async GetTotal(allowances)
    {
        var sum = 0;
        var keys = Object.keys(allowances);
        for(var i = 0; i < keys.length; i++)
        {
            var c = allowances[keys[i]];
            var price = await this.GetLatestPriceInUSDForSymbol(keys[i]);
            //currents.push(`${this.cryptos[i]} ${Number(c).toFixed(4)}`);
            sum += c * price;
        }

        return sum;
    }
        

    async PostLooking(obj, f, b){
        var currents = [];
        var sum = 0;
        var allowances = await this.GetBotAllowances();
        var keys = Object.keys(allowances);
        for(var i = 0; i < keys.length; i++)
        {
            var c = allowances[keys[i]];
            var price = await this.GetLatestPriceInUSDForSymbol(keys[i]);
            currents.push(`${keys[i]} ${Number(c).toFixed(4)}`);
            sum += c * price;
        }

        var message = [];
        var keys = Object.keys(this.watchedSymbols);
        for(var i = 0; i < keys.length; i++)
        {
            var p = obj[keys[i]];

            message.push(`(${keys[i]} ${Number(p).toFixed(4)})`);
        }
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - ${this.id} TriArbBot - Current ${currents.join(" ")} $${Number(sum).toFixed(4)} | f ${Number(f.diff).toFixed(4)} b ${Number(b.diff).toFixed(4)} | ${message.join(' | ')}`);
    }
}
module.exports = TriangularArbitrageBot;