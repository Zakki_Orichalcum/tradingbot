const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const { Timing } = require('../utilities');
const Arrays = new A();
const day = 24 * 60 * 60 * 1000;

class BBAndRSIBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.timingInformation = args.timingInfo ? args.timingInfo : {days:1};
        this.checkLines = true;
    }

    async Initialize(){
        super.Initialize();

        //await this.SetupKlines(20 + 1);//The 1 is to make sure to capture the 20th day based on how we are doing timestamps
    }

    async run()
    {
        super.run();

        var bbs = await this.GetActionBasedOnBBs();

        var currentAction = await this.CurrentAction();
    
        if(currentAction == 'BUY' && bbs.signal > 0){
            await this.Buy();
        }
        else if(currentAction == 'SELL' && bbs.signal < 0){
            await this.Sell();
        }

        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        if(order){
            await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                order.symbol, 
                order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
        }

        await this.PostLooking(bbs);
    }

    async GetActionBasedOnBBs(){
        var lasts = await this.GetLatestKline(Timing.TimingObjectToString(this.timingInformation), this.symbol.combinedSymbol, 21);
        if(lasts.length < 2)
            return {current: {}, previous: {}, signal: 0, rsi: 0};

        var closes = Arrays.MakeListFromObjects(lasts, "close");
        var current = this.GetBollingerBands(Arrays.Range(closes, 0, closes.length-1), lasts[0].ma20);
        var prev = this.GetBollingerBands(Arrays.Range(closes, 1, closes.length), lasts[1].ma20);
        var signal = 0;
        var rsi = lasts[0].rsi;
        if(rsi)
        {
            var aRsi = math.mean(Arrays.RemoveAllNulls(Arrays.MakeListFromObjects(lasts, 'rsi')));

            this.Post(`C Close vs. C Upper ${current.close > current.upper} | P Close vs. C Lower ${prev.close > current.lower} | C Close vs. C Lower ${current.close < current.lower} | RSI ${rsi} (${Number(rsi - aRsi).toFixed(4)})`)
    
            if (prev.close > current.lower && current.close < current.lower && rsi < 50)
            {
                signal = 1; //BUY
            }
            else if (current.close > current.upper && rsi > 60)
            {
                signal = -1; //SELL
            }
        }

        return {current: current, previous: prev, signal: signal, rsi: rsi};
    }

    GetBollingerBands(data, sma){
        var std = math.std(data);
        var upper_bb = sma + std * 2;
        var lower_bb = sma - std * 2;

        return {upper:upper_bb, lower:lower_bb, sma:sma, close: data[0]};
    }

    async PostLooking(obj){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - BBBot ${side} RSI: ${obj.rsi} C^^^ ${obj.current.upper ? obj.current.upper.toFixed(4) : 0} | ${obj.current.close} | ${obj.current.lower ? obj.current.lower.toFixed(4) : 0} vvv `+
            ` | P^^^ ${obj.previous.upper ? obj.previous.upper.toFixed(4): 0} | ${obj.previous.close} | ${obj.previous.lower ? obj.previous.lower.toFixed(4) : 0} vvv  | ${obj.signal} |`+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }
}
module.exports = BBAndRSIBot;