const sqlite3 = require('sqlite3').verbose();
const SQLUtility = require("../utilities").SQLUtility;
const SQL = new SQLUtility();
const N = require("../utilities").Numbers;
const fetch = require('node-fetch');
const auth = require('../auth');
const crypto = require('crypto-js');
const BaseBotClass = require('./baseBotClass');
const Algorithms = require("../utilities/Algorithms");
const c = require("../utilities").Arrays;
const Arrays = new c();

const DEFAULT_DB = 'data.db';
const binanceFee = 0.001;
const reductionConstant = 0.0001;
const averageFell = 0.025;
const standardWaitBeforeReducing = 2 * 60 * 1000;
const pollingTime = 1500;
const minNotional_adjustmentFactor = 1.75;
const endpoint = 'https://api.binance.us';

class PlaceAndWatchBot extends BaseBotClass{
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath){
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.buyConstant = args.buyConstant;
        this.sellConstant = args.sellConstant;
        this.averageCutOff = args.averageCutOff;
        this.trendAdjust = args.trendAdjust;        

        this.stateAction = "TRADING";
        this.IsTrend = "NONE";
        this.isWaitingForBounce = false;
        this.miniTrend = false;
        this.oppositeTrend = 0;
    }

    async run(){
        super.run();

        var rsiTop = 60;
        var trendBreakingAmount = 0; //was not working before, switch back to always being 0
        var klineAmount = 5;
        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        var last = await this.GetLastFilledOrderFromDatabase(this.symbol.combinedSymbol);
        var price = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var k = await this.GetLatestKline("5m", this.symbol.combinedSymbol, klineAmount);
        var lastKline = k[0];
        if(k.length >= klineAmount)
        {
            //var trend = this.GetTrendInformation(Arrays.Reverse(k), this.IsTrend == "NONE" ? 0 : trendBreakingAmount);
            var trend = this.GetTrendInformation(Arrays.Reverse(k), trendBreakingAmount);

            //var trend = this.GetTrendInformation(k, 0);

            //var bands = Algorithms.GetBollingerBands(Arrays.MakeListFromObjects(k, 'close'), lastKline.ma20);
            //this.Post(`Current ${price} +${bands.upper} -${bands.lower} ${price > bands.upper || price < bands.lower}`);
    
            if(this.IsTrend == "NONE") //still trying to work this out properly
            {
                // if(price > bands.upper && lastKline.rsi > 50){
                //     this.IsTrend = "UP";
                // }
                // else if(price < bands.lower && lastKline.rsi > 50)
                // {
                //     this.IsTrend = "DOWN";
                // }
                // else{
                //     this.IsTrend = "NONE";
                // }

                if(trend == 1 && lastKline.rsi > rsiTop){
                    this.IsTrend = "UP";
                    this.oppositeTrend = -1;
                }
                else if(trend == -1 && lastKline.rsi > rsiTop)
                {
                    this.IsTrend = "DOWN";
                    this.oppositeTrend = 1;
                }
                else{
                    this.IsTrend = "NONE";
                }
            }

            //if(trend == 0 && lastKline.rsi < rsiTop-10 && this.stateAction == "FREEZE")
            if(((trend == 0 && lastKline.rsi < rsiTop-10) || (trend == this.oppositeTrend)) && this.stateAction == "FREEZE")
            {
                var avg = await this.GetAveragePrice(this.symbol.combinedSymbol);
                if(this.IsTrend == "DOWN" && last && avg < last.pricePaying)
                {
                    order = await this.Buy();
                }
                else if (this.IsTrend == "UP" && last && avg > last.pricePaying)
                {
                    order = await this.Sell();
                }

                this.stateAction = "TRADING";
                this.IsTrend = "NONE";
                this.isWaitingForBounce = false;
                this.miniTrend = false;
                this.oppositeTrend = 0;
            }
            else if(this.stateAction == "FREEZE" && this.isWaitingForBounce)
            {
                var b = await this.GetLatestKline("1m", this.symbol.combinedSymbol, klineAmount);
                if(b.length >= klineAmount)
                {
                    var ty = this.GetTrendInformation(Arrays.Reverse(b), 1);
                    if(ty == this.oppositeTrend)
                        this.miniTrend = true;

                    var hasBounced = true; //this.miniTrend && (this.IsTrend == "UP" ? ty == 1 : ty == -1); //need to improve the detection
                    var action = await this.CurrentAction();
                    if(hasBounced)
                    {
                        if(action == "BUY")
                        {
                            order = await this.Buy();
                        }
                        else
                        {
                            order = await this.Sell();
                        }
                        this.isWaitingForBounce = false;
                    }
                    else if(action == "BUY" && price <= this.MinimumBuyPrice(last.pricePaying))
                    {
                        order = await this.Buy();
                        this.isWaitingForBounce = false;
                    }
                    else if(action == "SELL" && price >= this.MinimumSellPrice(last.pricePaying))
                    {
                        order = await this.Sell();
                        this.isWaitingForBounce = false;
                    }
                }
            }
            else if(this.IsTrend == "UP" && this.stateAction != "FREEZE")
            {
                if(order)
                {
                    await this.CancellingOrder(this.symbol.combinedSymbol);
                }
        
                if(await this.CurrentAction() == "BUY")
                {
                    this.isWaitingForBounce = true;
                }
        
                this.stateAction = "FREEZE";
            }
            else if (this.IsTrend == "DOWN" && this.stateAction != "FREEZE")
            {
                if(order)
                {
                    await this.CancellingOrder(this.symbol.combinedSymbol);
                }
        
                if(await this.CurrentAction() == "SELL")
                {
                    this.isWaitingForBounce = true;
                }
        
                this.stateAction = "FREEZE";
            }
        }

        if(!order && this.stateAction != "FREEZE")
        {
            order = await this.PlaceNewOrder(await this.GetNewPrice());
        }

        if(!order || !order.id){
            await this.PostLooking(0, 0);
        }
        else{
            if(await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                order.symbol, 
                order.origClientOrderId ? order.origClientOrderId : order.clientOrderId))
            return;
    
            if(this.stateAction != "FREEZE")
            {    
                await this.runPriceEvaluation(order);
            }
            else{
                this.Post(`Frozen until trend has ended.`);
            }
        }
    }
    
    async checkAverageAndSwitchIfNecessary(order){
        if(this.nextTimeToStartReducing == 0){
            this.nextTimeToStartReducing = Date.now() + standardWaitBeforeReducing;
        }
    
        // var average = await this.GetAveragePrice(this.symbol.combinedSymbol);
        // if(!await this.GetBalances())
        //     return;
    
        // var averagePercent = average/order.bound - 1;
        // if(await this.CurrentAction() == 'SELL' && averagePercent < -this.averageCutOff)
        // {
        //     var previousPrice = this.priceAction;
        //     //this.priceAction = average;
        //     this.priceAdjust = -1;
        //     this.reductionAmount = 1;
        //     this.nextTimeToStartReducing = 0;
        //     var num = await this.GetBalance(this.symbol.primarySymbol) ;
        //     var half = num/2;
        //     var mn = Math.ceil(this.minNotional/this.PreferredSellPrice(average)) + this.minNotional_adjustmentFactor;
        //     //TODO Locking mechanism not working at all.
        //     //await this.lockHalfUntilUnderLimit(this.symbol.combinedSymbol, mn > half ? num-mn : half, previousPrice);
    
        //     await this.CancellingOrder(this.symbol.combinedSymbol);
        //     this.Post(`Average too low. Switching from $${previousPrice} to $${average}`);
        //     await this.SellForSymbol(this.symbol.combinedSymbol, num);
        // }
        // else if(await this.CurrentAction() == 'BUY' && averagePercent > this.averageCutOff)
        // {
        //     var previousPrice = this.priceAction;
        //     //this.priceAction = average;
        //     this.priceAdjust = -1;
        //     this.reductionAmount = 1;
        //     this.nextTimeToStartReducing = 0;
        //     //TODO need to correct this
        //     // var num = await this.GetBalance(this.symbol.primarySymbol) ;
        //     // var half = num/2;
        //     // var mn = Math.ceil(this.minNotional/this.PreferredBuyPrice(average)) + this.minNotional_adjustmentFactor;
        //     var usdInAccount = await this.GetBalance(this.symbol.secondarySymbol) - await this.GetLockedAmount("BUY", average);
        //     var quantity = usdInAccount/average;
        //     //await this.lockHalfUntilUnderLimit(this.symbol.combinedSymbol, Math.max(0, mn > half ? half : num - mn), previousPrice);
    
        //     await this.CancellingOrder(this.symbol.combinedSymbol);
        //     this.Post(`Average too high. Switching from $${previousPrice} to $${average}`);
        //     await this.BuyForSymbol(this.symbol.combinedSymbol, quantity);
        // }
    }
    
    async lockHalfUntilUnderLimit(symbol, amount, price){
        var conn = await this.GetConnection();
    
        return await this.query(conn.run, `INSERT INTO lockedAmount (symbolLocked, lockedDuringAction, unlockAt, amount) ` +
            `Values ('${symbol}', '${await this.CurrentAction()}', ${price}, ${amount})`);
    }
    
    async GetLockedAmount(action, amount){
        var conn = await this.GetConnection();
        var output = 0;
        var all = await this.query(conn.all, `Select * from lockedAmount Where lockedDuringAction = '${action}'`);
        for(var i = 0; i < all.length; i++){
            var a = all[i];
            if((action == 'BUY' && a.unlockAt > amount) || 
                (action == 'SELL' && a.unlockAt < amount)){
                    this.Post(`Deleteing lock on ${a.amount} because price is now $${amount}!`);
                await this.DeleteLock(a.id);
            }
            else
            {
                output += all[i].amount;
            }
        }
    
        return output;
    }
    
    async DeleteLock(id){
        var conn = await this.GetConnection();
    
        return await this.query(conn.run, `DELETE FROM lockedAmount where id = ${id}`);
    }

    async runPriceEvaluation(order){
        var newPrice = 0;
        var price = await this.GetLatestPrice(this.symbol.combinedSymbol);
        if(this.nextTimeToStartReducing != 0 && Date.now() > this.nextTimeToStartReducing)
        {
            this.reductionAmount += reductionConstant;
        }
        if(await this.CurrentAction() == "SELL")
        {
            newPrice = this.priceAction / this.reductionAmount;
    
            if(newPrice < order.bound)
            {
                newPrice = order.bound;
                if(this.priceAdjust == -1)
                    this.priceAdjust = 0;
                this.priceAdjust++;
            }
    
            if(price >= newPrice)
            {
                await this.CancellingOrder(this.symbol.combinedSymbol);
                await this.timer(2 * pollingTime);
                if(!await this.GetBalances())
                    return;
                var num = await this.GetBalance(this.symbol.primarySymbol) ;
                this.Post(`Getting balances for ${this.symbol.primarySymbol} of ${num}`);
                //TODO why it is still getting balance of Zero
                // while(num < 1){
                //     await this.timer(1000);
                //     await this.GetBalances();
                //     num = await this.GetBalance(this.symbol.primarySymbol) ;
                // }
                var getAmountToSell = num - await this.GetLockedAmount("SELL", newPrice);
                getAmountToSell = this.limit > 0 && getAmountToSell > this.limit ? 100 : getAmountToSell;
    
                if(newPrice == order.bound)
                {
                    await this.SellForSymbol(this.symbol.combinedSymbol, getAmountToSell);
                }
                else{
                    await this.OrderForSymbolAtPrice(this.symbol.combinedSymbol, "SELL", getAmountToSell, newPrice, null, "LIMIT");
                }
            }
        }
        else{
            newPrice = this.priceAction * this.reductionAmount;
    
            if(newPrice > order.bound)
            {
                newPrice = order.bound;
                if(this.priceAdjust == -1)
                    this.priceAdjust = 0;
                this.priceAdjust++;
            }
    
            if(price <= newPrice)
            {
                await this.CancellingOrder(this.symbol.combinedSymbol);
                await this.timer(2 * pollingTime);
                if(!await this.GetBalances())
                    return;
                var num = await this.GetBalance(this.symbol.secondarySymbol) ;
                this.Post(`Getting balances for ${this.symbol.secondarySymbol} of ${num}`);
                //TODO why it is still getting balance of Zero
                // while(num < 1){
                //     await this.timer(1000);
                //     await this.GetBalances();
                //     num = await GetBalance(this.symbol.primarySymbol) ;
                // }
                var usdInAccount = num - await this.GetLockedAmount("BUY", newPrice);
                var getAmountToBuy = usdInAccount/price;
                getAmountToBuy = this.limit > 0 && getAmountToBuy > this.limit ? 100 : getAmountToBuy;
    
                if(newPrice == order.bound)
                {
                    await this.BuyForSymbol(this.symbol.combinedSymbol, getAmountToBuy);
                }
                else{
                    await this.OrderForSymbolAtPrice(this.symbol.combinedSymbol, "BUY", getAmountToBuy, newPrice, null, "LIMIT");
                }
            }
        }
    
        await this.PostLooking(order.pricePaying, newPrice);
    
        await this.checkAverageAndSwitchIfNecessary(order);
    }

//#region Orders 

    async PlaceNewOrder(lastPrice){
        var newPrice = 0; var stopPrice = 0; var quantity = 0; var type = "";
        if(!await this.GetBalances())
            return;
        var avg = await this.GetAveragePrice(this.symbol.combinedSymbol);
        if(await this.CurrentAction() == "SELL")
        {
            //We need to SELL
            newPrice = this.PreferredSellPrice(lastPrice);
            stopPrice = this.MinimumSellPrice(lastPrice);
            quantity = await this.GetBalance(this.symbol.primarySymbol) - await this.GetLockedAmount("SELL", newPrice);
            if(avg > newPrice)
            {
                newPrice = avg;
                //this.priceAction = avg;
            }
            type = "LIMIT";
        }
        else{
            //We need to buy
            newPrice = this.PreferredBuyPrice(lastPrice);
            stopPrice = this.MinimumBuyPrice(lastPrice);
            if(avg < newPrice)
            {
                newPrice = avg;
                //this.priceAction = avg;
            }
            var usdInAccount = await this.GetBalance(this.symbol.secondarySymbol) - await this.GetLockedAmount("BUY", newPrice);
            quantity = usdInAccount/newPrice;
            console.log(lastPrice, avg, newPrice, quantity, stopPrice);
            type = "LIMIT";
        }
    
        return await this.OrderForSymbolAtPrice(this.symbol.combinedSymbol, await this.CurrentAction(), quantity, newPrice, stopPrice, type);
    }

    //#endregion

    PreferredBuyPrice(price){
        return N.toFixedRoundDown((price * (1 - this.buyConstant - binanceFee)),this.priceSize);
    }
    MinimumBuyPrice(price){
        return N.toFixedRoundDown(((1-binanceFee) * price - reductionConstant),this.priceSize);
    }
    PreferredSellPrice(price){
        return N.toFixedRoundDown((price * (1 + this.sellConstant + binanceFee)),this.priceSize);
    }
    MinimumSellPrice(price){
        return N.toFixedRoundDown((price/(1-binanceFee) + reductionConstant),this.priceSize);
    }
    
    //#region TRENDS
    GetTrendInformation(list, trendbreak)
    {
        var obj = {higher:0, lower:0};
        for(var i = 1; i < list.length; i++)
        {
            if(list[i].ema20 > list[i-1].ema20)
            {
                obj.higher += 1;   
            }
            else
            {
                obj.lower += 1;
            }
        }

        if(obj.higher >= list.length-1-trendbreak)
        {
            return 1;
        }
        else if(obj.lower >= list.length-1-trendbreak)
        {
            return -1;
        }
        else{
            return 0;
        }
    }

    async AddPriceToTrends(symbol, price){
        var conn = await this.GetConnection();
    
        return await this.query(conn.run, `INSERT INTO trends (symbol, price) ` +
            `Values ('${symbol}', ${price})`);
    }
    
    async GetCurrentTrend(symbol, amount)
    {
        var conn = await this.GetConnection();
    
        var points = await this.query(conn.all, `SELECT price FROM (Select * from trends where symbol = '${symbol}' ORDER BY timestamp DESC LIMIT ${amount} ) ORDER BY timestamp ASC`);
        //console.log(points);
        var x = []; var y = [];
        points.forEach((element, i) => {
            x.push(i);
            y.push(element.price);
        });
    
        var lr = Algorithms.findLineByLeastSquares(x, y);
    
        return lr.slope * this.trendAdjust;
    }
    
    //#endregion
    
    //#region Messages
    async PostLooking(price, creepingPrice){
        var last = await this.GetLastPrice(this.symbol.primarySymbol);
        var side = last.transactionType == 'BUY' ? 'SELL' : 'BUY';
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var total = (currentPrice * await this.GetBalance(this.symbol.primarySymbol) + await this.GetBalance(this.symbol.secondarySymbol)
                 - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        var m = date%4;
        var spinningDisplay = m == 0 ? "|" : m == 1 ? "/" : m == 2 ? "-" : "\\";
        //process.stdout.write("\r\x1b[K");
        this.postingMessages.push(`${date.toLocaleString()} - ${side} ${this.symbol.primarySymbol} at $${price} `+
                `(${(side == 'BUY' ? 'Sold' : 'Bought')} $${last.priceInUsd}); Willing to pay $${creepingPrice}. Current: $${currentPrice} (${currentPrice-price}) `+
                `Total proposed: $${total} | ${this.stateAction} ${this.IsTrend} | ${(this.priceAdjust > 0 ? "| Adjusting price in " + (200 - this.priceAdjust) : "")} (${spinningDisplay})`);
    }
    
    //#endregion

}

module.exports = PlaceAndWatchBot;