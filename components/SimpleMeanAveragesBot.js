const { Timing } = require('../utilities');
const BaseBotClass = require('./baseBotClass');
const day = 24 * 60 * 60 * 1000;

class SimpleMeanAveragesBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.firstMean = args.firstMean;
        this.secondMean = args.secondMean;
        this.checkLines = true;
        this.lastfth = null;
        this.timingInfo = args.timingInfo ? Timing.TimingObjectToString(args.timingInfo) : "1d";
    }

    async Initialize(){
        super.Initialize();

        //await this.SetupKlines(this.secondMean);
    }

    async run()
    {
        super.run();

        this.lastfth = await this.GetFirstVsSecond();
        var currentAction = await this.CurrentAction();
    
        if(currentAction == 'BUY' && this.lastfth.result > 0)
        {
            await this.Buy();
        }
        else if(currentAction == 'SELL' && this.lastfth.result < 0)
        {
            await this.Sell();
        }

        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        if(order){
            await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                order.symbol, 
                order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
        }

        await this.PostLooking(this.lastfth);
    }

    async GetFirstVsSecond(){
        var k = await this.GetAverageForNumberOfDays(this.firstMean);
        var k2 = await this.GetAverageForNumberOfDays(this.secondMean);

        return { result: k > k2 ? 1 : k < k2 ? -1 : 0, first: k, second: k2};
    }

    async PostLooking(obj){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - SMABot ${side} SMA${this.firstMean} ${obj.first} | SMA${this.secondMean} ${obj.second} | ${obj.result} | `+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }

    async GetAverageForNumberOfDays(days)
    {
        var conn = await this.GetConnection();
        if(days == 20 || days == 50 || days == 200)
        {
            var last = await this.query(conn.get, `Select * from [${this.timingInfo}Klines] where symbol = '${this.symbol.combinedSymbol}' ORDER BY date DESC LIMIT 1`);
            return last && last.ma20 && days == 20 ? last.ma20 : last && last.ma50 && days == 50 ? last.ma50 : last && last.ma200 && days == 200 ? last.ma200 : 0;
        }

        var last = await this.query(conn.all, `Select * from [${this.timingInfo}Klines] where symbol = '${this.symbol.combinedSymbol}' ORDER BY date DESC LIMIT ${days}`);
        var sum = 0;
        for (let i = 0; i < last.length; i++) {
            const element = last[i];
            sum += element.close;
        }

        return sum/last.length;
    }
}
module.exports = SimpleMeanAveragesBot;