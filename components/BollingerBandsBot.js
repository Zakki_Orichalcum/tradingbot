const BaseBotClass = require('./baseBotClass');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const { Timing } = require('../utilities');
const Arrays = new A();
const day = 24 * 60 * 60 * 1000;

class BollingerBandsBot extends BaseBotClass {
    constructor(args, apis, symbols, exchangeInfo, serverTime, databasePath)
    {
        super(args, apis, symbols, exchangeInfo, serverTime, databasePath);
        this.timingInformation = args.timingInfo ? args.timingInfo : {days:1};
        this.checkLines = true;
    }

    async Initialize(){
        super.Initialize();

        //await this.SetupKlines(20 + 1);//The 1 is to make sure to capture the 20th day based on how we are doing timestamps
    }

    async run()
    {
        super.run();

        var bbs = await this.GetActionBasedOnBBs();

        var currentAction = await this.CurrentAction();
    
        if(currentAction == 'BUY' && bbs.signal > 0){
            await this.Buy();
        }
        else if(currentAction == 'SELL' && bbs.signal < 0){
            await this.Sell();
        }

        var order = await this.GetLastOpenOrderFromDatabase(this.symbol.combinedSymbol);
        if(order){
            await this.ProcessOrder(order.orderId ? order.orderId : order.id, 
                order.symbol, 
                order.origClientOrderId ? order.origClientOrderId : order.clientOrderId);
        }

        await this.PostLooking(bbs);
    }

    async GetActionBasedOnBBs(){
        var lasts = await this.GetLatestKline(Timing.TimingObjectToString(this.timingInformation), this.symbol.combinedSymbol, 21);
        if(lasts.length < 2)
            return {current: {}, previous: {}, signal: 0};

        var closes = Arrays.MakeListFromObjects(lasts, "close");
        var current = this.GetBollingerBands(Arrays.Range(closes, 0, closes.length-1), lasts[0].ma20);
        var prev = this.GetBollingerBands(Arrays.Range(closes, 1, closes.length), lasts[1].ma20);
        var signal = 0;

        if (prev.close > prev.lower && current.close < current.lower)
        {
            signal = 1;
        }
        else if (prev.close < prev.upper && current.close > current.upper){
            signal = -1;
        }

        return {current: current, previous: prev, signal: signal};
    }

    GetBollingerBands(data, sma){
        var std = math.std(data);
        var upper_bb = sma + std * 2;
        var lower_bb = sma - std * 2;

        return {upper:upper_bb, lower:lower_bb, sma:sma, std:std, close: data[0]};
    }

    async PostLooking(obj){
        var side = await this.CurrentAction();
        var currentPrice = await this.GetLatestPrice(this.symbol.combinedSymbol);
        var primary = await this.GetBalance(this.symbol.primarySymbol);
        var second = await this.GetBalance(this.symbol.secondarySymbol);
        var total = (currentPrice * primary + second - this.balances["commissions"]) - (this.startingPrice * this.startingSymbol);
    
        var date = new Date();
        this.postingMessages.push(
            `${date.toLocaleString()} - BBBot ${side} Current ^^^ ${obj.current.upper ? obj.current.upper.toFixed(4) : 0} | ${obj.current.close} | ${obj.current.lower ? obj.current.lower.toFixed(4) : 0} vvv `+
            ` | Previous ^^^ ${obj.previous.upper ? obj.previous.upper.toFixed(4): 0} | ${obj.previous.close} | ${obj.previous.lower ? obj.previous.lower.toFixed(4) : 0} vvv  | ${obj.signal} |`+
            `Current: ${this.symbol.primarySymbol} ${primary} | ${this.symbol.secondarySymbol} ${second} | `+
            `Total proposed: $${total}`);
    }
}
module.exports = BollingerBandsBot;