const emailAuth = require('../auth.json').email;
const nodemailer = require('nodemailer');

const port = 556;

class SendingEmails
{
    constructor()
    {
        this.transporter = nodemailer.createTransport({
            service: emailAuth.service,
            // port: port,
            // secure: false,
            auth: 
            {
                user: emailAuth.user,
                pass: emailAuth.pass
            }
        });
        // this.transporter = nodemailer.createTransport({
        //     host: emailAuth.host,
        //     port: port,
        //     secure: false,
        //     auth: 
        //     {
        //         user: emailAuth.user,
        //         pass: emailAuth.pass
        //     }
        // });
        this.from = emailAuth.from;
        this.to = emailAuth.to;
    }

    async SendMail(subject, text, html)
    {
        let info = await this.transporter.sendMail({
            from: this.from,
            to: this.to,
            subject: subject,
            text: text,
            html: html
        });

        console.log("Message sent: " + info.messageId);
    }
}

module.exports = SendingEmails;