const sqlite3 = require('sqlite3').verbose();
const dateformat = require('dateformat');
const dbbuild = require('../dbbuildjs');
const SQLUtility = require("../utilities").SQLUtility;
const SQL = new SQLUtility();
const Arrs = require("../utilities").Arrays;
const Arrays = new Arrs();
const Timing = require("../utilities").Timing;
const math = require("mathjs");
const Algorithms = require("../utilities/Algorithms");

const fetch = require('node-fetch');

const BinanceAPI = require("../apis/BinanceAPI");
const day = 24 * 60 * 60 * 1000;

const timer = ms => new Promise( res => setTimeout(res, ms));

const DEFAULT_DB = 'data.db';
 const endpoint = 'https://api.binance.us';
// //const endpoint = 'http://localhost:4425';


class DataCollection {
    constructor(){
        this.nextTime = {}; 
        this.klines = {};
    }

//#region Utilities
    GetConnection(){
        this.db = new sqlite3.Database(DEFAULT_DB);

        return this.db;
    }

    async query(fn, query, values) {
        if (values) {
            fn = fn.bind(this.db, query, values);
        } else {
            fn = fn.bind(this.db, query);
        }
        return new Promise((resolve, reject) => {
            try {
                fn((err, rows) => {
                    if (err) {
                        reject(
                            new Error(
                                `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                            ),
                        );
                    } else {
                        resolve(rows);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    QueryString(obj){
        return Object.keys(obj).map((key) => {
            return `${key}=${obj[key]}`;
        }).join('&');
    }

    //#region Pricing and Setup Information

    async CleanTrends()
    {
        var conn = await this.GetConnection();
        var t = (new Date()).getTime() - Timing.GetMilliseconds({days:5});
        var date = new Date(t);

        await this.query(conn.all, `DELETE from trends where timestamp <= '${dateformat(date, 'yyyy-mm-dd HH:MM:ss', true)}'`);
    }

    async GetTrendsFrom(symbol, datetime)
    {
        var conn = await this.GetConnection();
        var date = new Date(datetime);

        return await this.query(conn.all, `Select * from trends where symbol = '${symbol}' AND timestamp > '${dateformat(date, 'yyyy-mm-dd HH:MM:ss', true)}'`);
    }

    async GetLatestPrice(symbol){
        var d = await fetch(`${endpoint}/api/v3/ticker/price?symbol=${symbol}`);
        var j = await d.json();

        //await AddPriceToTrends(symbol, j.price);

        return j.price;
    }

    async AddLatestPriceToTrends(symbol){
        var price = await this.GetLatestPrice(symbol);
        var conn = await this.GetConnection();

        var g = await this.query(conn.get, `Select * from trends where symbol = '${symbol}' ORDER BY timestamp DESC LIMIT 1`);

        if(g && g.price == price)
            return;

        await this.query(conn.run, `INSERT INTO trends (symbol, price) ` +
            `Values ('${symbol}', ${price})`);
    }

    async SetupKlines(symbol, num){
        var klines = await this.GetKlinesForLastNumberOfDays(symbol, num);
        await this.AddKlines(symbol, klines);
    }

    async AddKlines(symbol, klines){
        var conn = await this.GetConnection();
        for(var i = 0; i < klines.length; i++){
            var k = klines[i];
            var e = await this.query(conn.get, `Select * from [1dKlines] where symbol = '${symbol}' AND date = ${k[0]}`);
            if(!e){
                var kline = {
                    date: k[0],
                    open: k[1],
                    close: k[4],
                    high: k[2],
                    low: k[3],
                };
                await this.AddKline(kline, "1dKlines", symbol);
            }
        }
    }

    async GetKlinesForLastNumberOfDays(symbol, days){
        var timestamp = await this.GetServerTime();

        var last = timestamp - (days * day);

        var q = { symbol:symbol, interval: "1d", startTime: last };
        var queryStr = this.QueryString(q);

        var d = await fetch(`${endpoint}/api/v3/klines?${queryStr}`);
        var j = await d.json();

        return j;
    }

    async GetServerTime(){
        var ts = await fetch(`${endpoint}/api/v3/time`);
        var timestamp = await ts.json();

        return timestamp.serverTime;
    }

    async GetLatestKline(symbol, timeString){
        var conn = await this.GetConnection();

        var q = `Select * from [${timeString}Klines] where symbol = '${symbol}' Order By date DESC LIMIT 1`;
        return await this.query(conn.get, q);
    }

    async SetTrendKlines(symbol, timingObj)
    {
        var timeString = Timing.TimingObjectToString(timingObj);
        var last = await this.GetLatestKline(symbol, timeString);

        if(last && last.date < (new Date()).getTime() - (2 * Timing.GetMilliseconds(timingObj)))
        {
            var conn = await this.GetConnection();

            var q = `Delete from [${timeString}Klines]`;
            return await this.query(conn.run, q);
        }

        var symbolTime = symbol+timeString;
        if(!this.klines[symbolTime])
        {
            if(!last)
                this.klines[symbolTime] = [];
            else{
                var k = await this.GetTrendsFrom(symbol, last.date);
                if(k && k.length > 0)
                    this.klines[symbolTime] = Arrays.MakeListFromObjects(k, 'price');
            }
        }

        if(!this.nextTime[symbolTime]){
            var t = (new Date()).getTime() + Timing.GetMilliseconds(timingObj);
            if(last)
                t = last.date;
            this.nextTime[symbolTime] = t;
        }

        if(this.nextTime[symbolTime] > (new Date()).getTime())
        {
            var price = await this.GetLatestPrice(symbol);
            this.klines[symbolTime].push(price);
        }
        else{
            var arr = this.klines[symbolTime];
            if(arr && arr.length > 0)
            {
                var kline = {
                    date: await this.GetServerTime(),
                    open: arr[0],
                    close: arr[arr.length-1],
                    high: math.max(arr),
                    low: math.max(arr)
                };

                await this.AddKline(kline, timeString+"Klines", symbol);
            }

            var t = (new Date()).getTime() + Timing.GetMilliseconds(timingObj);
            this.nextTime[symbolTime] = t;

            this.klines[symbolTime] = [];
        }
    }

    async AddKline(kline, table, symbol){
        var conn = await this.GetConnection();
        var e = await this.query(conn.all, `Select * from [${table}] where symbol = '${symbol}' ORDER BY date ASC`);
        if(e.length > 0){
            e.push(kline);

            var ma20Arr = (e.length < 20) ? e :  Arrays.Range(e, e.length-20, e.length);
            var ma50Arr = (e.length < 50) ?  e :  Arrays.Range(e, e.length-50, e.length);
            var ma200Arr = (e.length < 200) ?  e :  Arrays.Range(e, e.length-200, e.length);

            ma20Arr = Arrays.MakeListFromObjects(ma20Arr, 'close');
            ma50Arr = Arrays.MakeListFromObjects(ma50Arr, 'close');
            ma200Arr = Arrays.MakeListFromObjects(ma200Arr, 'close');

            var ma20 = math.mean(ma20Arr);
            var ma50 = math.mean(ma50Arr);
            var ma200 = math.mean(ma200Arr);

            var prev = e[e.length-2];
            var cur = e[e.length-1];

            var ms = Algorithms.getMs(prev, cur);

            var mplus = ms.Mplus;
            var mminus = ms.Mminus;
            var tr = math.max([cur.high-prev.high, cur.high-cur.close, cur.low-cur.close]);
            var dms = Algorithms.getDMs(prev, cur);
            var dmplus = dms.DMplus;
            var dmminus = dms.DMminus;
            var tr14 = null;
            var dmplus14 = null;
            var dmminus14 = null;
            var diplus = null;
            var diminus = null;
            var dx = null;
            var adx = null;
            var smmaUp14 = null;
            var smmaDown14 = null;
            var rsi = null;
            var rs = null;
            var kP = null;
            var smoothkP = null;
            var dP = null;
            var roc10 =null;
            var roc15 = null;
            var roc20 = null;
            var roc30 = null;
            var kst = null;
            var kSignal = null;

            var ema2 = e.length >= 3 ? Algorithms.EMA(2, cur.close, prev.ema2) : Algorithms.EMA(2, cur.close, prev.close);
            var ema5 = e.length >= 6 ? Algorithms.EMA(5, cur.close, prev.ema5) : Algorithms.EMA(5, cur.close, prev.close);
            var ema10 = e.length >= 11 ? Algorithms.EMA(10, cur.close, prev.ema2) : Algorithms.EMA(10, cur.close, prev.close);
            var ema20 = e.length >= 21 ? Algorithms.EMA(20, cur.close, prev.ema2) : Algorithms.EMA(20, cur.close, prev.close);
            var ema30 = e.length >= 31 ? Algorithms.EMA(30, cur.close, prev.ema2) : Algorithms.EMA(30, cur.close, prev.close);
            var ema50 = e.length >= 51 ? Algorithms.EMA(50, cur.close, prev.ema2) : Algorithms.EMA(50, cur.close, prev.close);
            var ema200 = e.length >= 201 ? Algorithms.EMA(200, cur.close, prev.ema2) : Algorithms.EMA(200, cur.close, prev.close);
            var ema400 = e.length >= 401 ? Algorithms.EMA(400, cur.close, prev.ema2) : Algorithms.EMA(400, cur.close, prev.close);

            if(e.length >= 14)
            {
                var r = Arrays.Range(e, e.length-14, e.length);
                var high = math.max(Arrays.MakeListFromObjects(r, "high"));
                var low = math.min(Arrays.MakeListFromObjects(r, "low"));
                kP = ((cur.close-low)/(high-low)) * 100;
                e[e.length-1].kP = kP;
                if(e.length >= 17)
                {
                    var er = Arrays.Range(e, e.length-3, e.length);
                    smoothkP = math.mean(Arrays.MakeListFromObjects(er, "kP"));
                }
                if(e.length >= 28)
                {
                    var dr = Arrays.Range(e, e.length-14, e.length);
                    dP = math.mean(Arrays.MakeListFromObjects(dr, "kP"));
                }
            }

            if(e.length == 14)
            {
                var r = Arrays.Range(e, e.length-14, e.length);
                var ups = []; var downs = [];
                for(var i = 0; i< r.length; i++){
                    var item = r[i];
                    var d = item.open-item.close;
                    ups.push(d > 0 ? d : 0);
                    downs.push(d < 0 ? -1 * d : 0);
                }

                smmaUp14 = math.mean(ups);
                smmaDown14 = math.mean(downs);
                rs = smmaUp14 / smmaDown14;
                rsi = 100 - (100/(1+rs));
            }
            else if(e.length > 14)
            {
                var d = cur.open-cur.close;
                var up = d > 0 ? d : 0;
                var down = d < 0 ? -1 * d : 0;
                smmaUp14 = (up + (prev.smmaUp14 * 13))/14;
                smmaDown14 = (down + (prev.smmaDown14 * 13))/14;
                rs = smmaUp14 / smmaDown14;
                rsi = 100 - (100/(1+rs));
            }

            if(e.length == 15)
            {
                e[e.length-1].dmplus = dmplus;
                e[e.length-1].dmminus = dmminus;
                e[e.length-1].tr = tr;
                var e2 = Arrays.Range(e, 1, 15);
                dmplus14 = math.sum(Arrays.MakeListFromObjects(e2, 'dmplus')); 
                dmminus14 = math.sum(Arrays.MakeListFromObjects(e2, 'dmminus')); 
                tr14 = math.sum(Arrays.MakeListFromObjects(e2, 'tr'));
            }
            else if(e.length > 15){
                tr14 = (prev.tr14 - (prev.tr14/14)) + tr;
                dmplus14 = (prev.dmplus14 - (prev.dmplus14/14)) + dmplus;
                dmminus14 = (prev.dmminus14 - (prev.dmminus14/14)) + dmminus;

                diplus = (dmplus14/tr14) * 100;
                diminus = (dmminus14/tr14) * 100;

                var abs = math.abs(diplus-diminus);
                var sum = diplus+diminus;
                dx = (abs/sum) * 100;

                if(e.length == 28)
                {
                    e[e.length-1].dx = dx;
                    var e3 = Arrays.Range(e, 16, 28);

                    adx = math.mean(Arrays.MakeListFromObjects(e3, 'dx'));
                }
                else if(e.length > 28)
                {
                    adx = ((prev.adx * (13)) + dx)/14;
                }
            }

            if(e.length > 10)
            {
                var closes = Arrays.MakeListFromObjects(e, 'close');
                roc10 = Algorithms.ROC(Arrays.Range(closes, closes.length-10, closes.length));
                if(e.length > 15)
                {
                    roc15 = Algorithms.ROC(Arrays.Range(closes, closes.length-15, closes.length));
                    if(e.length > 20)
                    {
                        roc20 = Algorithms.ROC(Arrays.Range(closes, closes.length-20, closes.length));
                        if(e.length > 30)
                        {
                            roc30 = Algorithms.ROC(Arrays.Range(closes, closes.length-30, closes.length));
                            if(e.length > 45)
                            {
                                e[e.length-1].roc10 = roc10;
                                e[e.length-1].roc15 = roc15;
                                e[e.length-1].roc20 = roc20;
                                e[e.length-1].roc30 = roc30;
    
                                var rocsma1 = math.mean(Arrays.Range(Arrays.MakeListFromObjects(e, 'roc10'), closes.length-10, closes.length));
                                var rocsma2 = math.mean(Arrays.Range(Arrays.MakeListFromObjects(e, 'roc15'), closes.length-10, closes.length));
                                var rocsma3 = math.mean(Arrays.Range(Arrays.MakeListFromObjects(e, 'roc20'), closes.length-10, closes.length));
                                var rocsma4 = math.mean(Arrays.Range(Arrays.MakeListFromObjects(e, 'roc30'), closes.length-15, closes.length));
    
                                kst = (1 * rocsma1) + (2 * rocsma2) + (3 * rocsma3) + (4 * rocsma4);
                                
                                e[e.length-1].kst = kst;
    
                                if(e.length > 54)
                                {
                                    kSignal = math.mean(Arrays.Range(Arrays.MakeListFromObjects(e, 'kst'), closes.length-9, closes.length));
                                }
                            }
                        }
                    }
                }
            }

            var q = `Insert Into [${table}] (symbol, date, open, high, low, close, ma20, ma50, ma200, mplus, mminus, tr, dmplus, dmminus, tr14, dmplus14, dmminus14, diplus, diminus, dx, adx, ` +
                `smmaUp14, smmaDown14, rs, rsi, kP, smoothkP, dP, roc10, roc15, roc20, roc30, kst, kSignal, ema2, ema5,ema10,ema20,ema30,ema50,ema200,ema400 ) ` + 
            `Values ('${symbol}', ${kline.date}, ${kline.open}, ${kline.high}, ${kline.low}, ${kline.close}, ${ma20}, ${ma50}, `+
            `${ma200}, ${mplus}, ${mminus}, ${tr}, ${dmplus}, ${dmminus}, ${tr14}, ${dmplus14}, ${dmminus14}, ${diplus}, ${diminus}, ${dx}, ${adx}, ${smmaUp14}, ${smmaDown14}, ${rs && Number.isFinite(rs) ? rs : 0}, ${rsi && Number.isFinite(rsi) ? rsi : 0}, `+ 
            `${kP && Number.isFinite(kP)? kP : 0}, ${smoothkP && Number.isFinite(smoothkP) ? smoothkP : 0}, ${dP && Number.isFinite(dP) ? dP : 0}, ${roc10}, ${roc15}, ${roc20}, ${roc30}, ${kst}, ${kSignal},` + 
            `${ema2}, ${ema5},${ema10},${ema20},${ema30},${ema50},${ema200},${ema400} )`;
            await this.query(conn.run, q);
        }
        else{
            var q = `Insert Into [${table}] (symbol, date, open, high, low, close, ma20, ma50, ma200)` + 
            `Values ('${symbol}', ${kline.date}, ${kline.open}, ${kline.high}, ${kline.low}, ${kline.close}, ${kline.close}, ${kline.close}, ${kline.close})`;
            await this.query(conn.run, q);
        }
    }
}


module.exports = DataCollection;
