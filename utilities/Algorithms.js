const c = require("../utilities").Arrays;
const Arrays = new c();
const math = require("mathjs");

class Algorithms {
    SMA(data){
        return math.mean(data);
    }

    EMA(num, currentPrice, pastEMA){
        //Smoothing Factor
        var k = 2 / (num + 1);
        // ema = (currentPrice - pastEMA) * k + pastEMA
        var ema = currentPrice * k + pastEMA * (1-k);
        return ema;
    }

    EMARange(arr, window)
    {
        // first item is just the same as the first item in the input
        var emaArray = [arr[0]];
        // for the rest of the items, they are computed with the previous one
        for (var i = 1; i < arr.length; i++) {
            emaArray.push(this.EMA(window, arr[i], emaArray[i - 1]));
        }
        return emaArray;
    }

    GetBollingerBands(data, sma){
        var std = math.std(data);
        var upper_bb = sma + std * 2;
        var lower_bb = sma - std * 2;

        return {upper:upper_bb, lower:lower_bb, sma:sma, std:std, close: data[0]};
    }

    getMs(prev, current)
    {
        return {Mplus: current.high - prev.high, Mminus: prev.low - current.low};
    }

    getDMs(prev, current)
    {
        var M = this.getMs(prev, current);
        var DMplus = M.Mplus > M.Mminus && M.Mplus > 0 ? M.Mplus : 0;
        var DMminus = M.Mminus > M.Mplus && M.Mminus > 0 ? M.Mminus : 0;

        return {DMplus:DMplus, DMminus:DMminus};
    }

    getDI(arr, period)
    {
        var DMpos = []; var DMneg = []; var tr = [];
        for(var i = 1; i < arr.length; i++)
        {
            var prev = arr[i-1]; var current = arr[i];
            var dms = this.getDMs(prev, current);

            DMpos.push(dms.DMplus);
            DMneg.push(dms.DMminus);

            var a1 = current.high - prev.high;
            var a2 = current.high - prev.close;
            var a3 = current.low - prev.close;
            tr.push(math.max([a1,a2,a3]));
        }

        var DMposP = [math.sum(Arrays.Range(DMpos, 1, period+1))]; 
        var DMnegP = [math.sum(Arrays.Range(DMneg, 1, period+1))]; 
        var trP = [math.sum(Arrays.Range(tr, 1, period+1))];
        DMpos = Arrays.Range(DMpos, period-1, DMpos.length);
        DMneg = Arrays.Range(DMneg, period-1, DMneg.length);
        tr = Arrays.Range(tr, period-1, tr.length);

        for(var i = 1; i < DMpos.length; i++)
        {
            DMposP.push((DMposP[i-1] - (DMposP[i-1]/period)) + DMpos[i]);
            DMnegP.push((DMnegP[i-1] - (DMnegP[i-1]/period)) + DMneg[i]);
            trP.push((trP[i-1] - (trP[i-1]/period)) + tr[i]);
        }

        var DIpos = []; var DIneg = [];
        for(var i = 0; i < DMposP.length; i++)
        {
            DIpos.push((DMposP[i]/trP[i]) * 100);
            DIneg.push((DMnegP[i]/trP[i]) * 100);
        }

        return {DIpos:DIpos, DIneg:DIneg}; 
    }

    ADX(df, amount)
    {
        var di = this.getDI(df, amount);

        var dx = [];
        for(var i = 0; i < di.DIpos.length; i++)
        {
            var abs = math.abs(di.DIpos[i]-di.DIneg[i]);
            var sum = di.DIpos[i]+di.DIneg[i];
            dx.push(abs/sum * 100);
        }

        var adx = [math.mean(Arrays.Range(dx, 0, dx.length-(dx.length-amount)))];
        for(var i = 1; i < dx.length-amount; i++)
        {
            adx.push(((adx[i-1] * (amount-1)) + dx[dx.length-amount+i])/amount);
        }

        var prev = {
            ADX: adx[adx.length-2],
            DI: {
                DIpos: di.DIpos[di.DIpos.length-2],
                DIneg: di.DIneg[di.DIneg.length-2]
            }
        };
        var current = {
            ADX: adx[adx.length-1],
            DI: {
                DIpos: di.DIpos[di.DIpos.length-1],
                DIneg: di.DIneg[di.DIneg.length-1]
            }
        };

        
        this.Post(`prev ${JSON.stringify(prev)}`);
        this.Post(`current ${JSON.stringify(current)}`);

        return { AllADX: adx, AllDI: di, previous: prev, current: current };
    }

    RSI(prices, Tolerance)
    {
        var sumGain = 0;
        var sumLoss = 0;
        for (var i = 1; i < prices.Length; i++)
        {
            var difference = prices[i] - prices[i - 1];
            if (difference >= 0)
            {
                sumGain += difference;
            }
            else
            {
                sumLoss -= difference;
            }
        }

        if (sumGain == 0) return 0;
        if (math.Abs(sumLoss) < Tolerance) return 100;

        var relativeStrength = sumGain / sumLoss;

        return 100.0 - (100.0 / (1 + relativeStrength));
    }

    RSI2(close, lookback){
        var ret = Arrays.Diff(close);
        var up = [];
        var down = [];
        for(var i = 0 ; i< ret.length; i++){
            if(ret[i] < 0){
                up.push(0);
                down.push(math.abs(ret[i]));
            }
            else{
                down.push(0);
                up.push(ret[i]);
            }
        }

        var up_ewm = this.EMARange(up, lookback - 1);
        var down_ewm = this.EMARange(down, lookback - 1);
        var rs = up_ewm[up_ewm.length-1]/down_ewm[up_ewm.length-1];

        return 100 - (100 / (1 + rs));
    }

    findLineByLeastSquares(values_x, values_y) 
    {
        var sum_x = 0;
        var sum_y = 0;
        var sum_xy = 0;
        var sum_xx = 0;
        var count = 0;
    
        /*
         * We'll use those variables for faster read/write access.
         */
        var x = 0;
        var y = 0;
        var values_length = values_x.length;
    
        if (values_length != values_y.length) {
            throw new Error('The parameters values_x and values_y need to have same size!');
        }
    
        /*
         * Nothing to do.
         */
        if (values_length === 0) {
            return [ [], [] ];
        }
    
        /*
         * Calculate the sum for each of the parts necessary.
         */
        for (var v = 0; v < values_length; v++) {
            x = values_x[v];
            y = values_y[v];
            sum_x += x;
            sum_y += y;
            sum_xx += x*x;
            sum_xy += x*y;
            count++;
        }
    
        /*
         * Calculate m and b for the formular:
         * y = x * m + b
         */
        var m = (count*sum_xy - sum_x*sum_y) / (count*sum_xx - sum_x*sum_x);
        var b = (sum_y/count) - (m*sum_x)/count;
    
        return { slope: m, coefficient: b};
    }

    ROC(arr)
    {
        var first = arr[0];
        var last = arr[arr.length-1];

        return ((last - first)/first ) * 100;
    }
}
module.exports = new Algorithms()