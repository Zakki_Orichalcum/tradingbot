const sqlite3 = require('sqlite3').verbose();
const dbbuild = require('../dbbuildjs');
const SQLUtility = require("./index").SQLUtility;
const SQL = new SQLUtility();

const fetch = require('node-fetch');
const servicesJson = require('../services');
const auth = require('../auth.json').Binance;
const crypto = require('crypto-js');

const DEFAULT_DB = 'data.db';

class Database 
{
    constructor(dataname)
    {
        this.databaseName = dataname ? dataname : DEFAULT_DB;
    }
    GetConnection(){
        global.db = new sqlite3.Database(this.databaseName);

        return global.db;
    }

    async query(fn, query, values) {
        if (values) {
            fn = fn.bind(global.db, query, values);
        } else {
            fn = fn.bind(global.db, query);
        }
        return new Promise((resolve, reject) => {
            try {
                fn((err, rows) => {
                    if (err) {
                        reject(
                            new Error(
                                `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                            ),
                        );
                    } else {
                        resolve(rows);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    async Get(query)
    {
        var conn = this.GetConnection();
        return await this.query(conn.get, query);
    }

    async GetAll(query)
    {
        var conn = this.GetConnection();
        return await this.query(conn.all, query);
    }

    async Run(query)
    {
        var conn = this.GetConnection();
        return await this.query(conn.run, query);
    }
}

module.exports = Database;