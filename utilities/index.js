'use strict'
const math = require('mathjs');

class Guid {
  newGuid(type) {
    if (!type || type.indexOf('v4') > -1) {
      return this.uuidv4()
    }

    return this.uuidv4()
  }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c == 'x' ? r : (r & 0x3) | 0x8
      return v.toString(16)
    })
  }
}

class SQLUtility {
  constructor() {
    this.s = new Strings()
  }

  insert(tableName, values, entries) {
    if (entries.length == 0) return null

    return (
      `INSERT INTO ${tableName} (${this.s.joinExtended(
        values,
        ',',
        '[',
        ']'
      )})` + `Values ${this.objectKeysToValueString(values, entries)}`
    )
  }

  update(tableName, updateArguments, where) {
    var arr = []

    Object.keys(updateArguments).forEach(key => {
      switch (typeof updateArguments[key]) {
        case 'number':
          arr.push(`${key} = ${updateArguments[key]}`)
          break
        case 'undefined':
          break
        default:
          arr.push(`${key} = '${updateArguments[key]}'`)
          break
      }
    })

    return `UPDATE ${tableName} SET ${arr.join(', ')} WHERE ${where}`
  }

  delete(tableName, where) {
    return `DELETE FROM ${tableName} WHERE ${where};`
  }

  objectKeysToValueString(values, entries) {
    var output = []

    entries.forEach(entry => {
      var sb = []

      values.forEach(v => {
        switch (typeof entry[v]) {
          case 'number':
            sb.push(entry[v])
            break
          case 'undefined':
            sb.push(null)
          default:
            sb.push("'" + entry[v] + "'")
            break
        }
      })

      output.push('(' + sb.join(',') + ')')
    })

    return output.join(',')
  }
}

class Strings {
  constructor(){
    this.Alphabet = "abcdefghijklmnopqrstuvwxyz";
    this.Vowels = "aeiouy";
  }

  IsVowel(char) { return this.Vowels.indexOf(char) > -1; }

  GetFirstVowel(str){
      for(var i = 0; i < str.length; i++){
          if(this.IsVowel(str[i])){
              return str[i];
          }
      }

      return null;
  }

  joinExtended(arr, delimiter, wrapperLeft, wrapperRight){
      var output = [];
      delimiter = delimiter ? delimiter : ',';
      wrapperLeft = wrapperLeft ? wrapperLeft : '';
      wrapperRight = wrapperRight ? wrapperRight : '';

      arr.forEach((v, i) => { output.push(wrapperLeft + v + wrapperRight); });

      return output.join(delimiter);
  }

  Capitalize(str)
  {
      return str[0].toUpperCase() + str.substring(1);
  }
}

class Numbers {
  ratioConvertion(ratioN, ratioD, converted) {
    c = 1.0 * converted
    b = 1.0 * ratioD
    a = 1.0 * ratioN
    return (c * b) / a
  }

  countDecimals(value) {
    let text = value.toString()
    // verify if number 0.000005 is represented as "5e-6"
    if (text.indexOf('e-') > -1) {
      let [base, trail] = text.split('e-');
      let deg = parseInt(trail, 10);
      return deg;
    }
    // count decimals for number in representation like "0.123456"
    if (Math.floor(value) !== value) {
      return value.toString().split(".")[1].length || 0;
    }
    return 0;
  }

  countDecimalsToAOne(value) {
    if (Math.floor(value) !== value) {
      var s = value.toString().split(".")[1]
      if(s.indexOf("1") > -1)
        return s.split("1")[0].length + 1 || 0;
    }
    return 0;
  }

  toFixedRoundDown(number, places) {
    var multiplier = Math.pow(10, places+2); // get two extra digits
    var fixed = Math.floor(number*multiplier); // convert to integer
    //fixed += 44; // round down on anything less than x.xxx56
    fixed = Math.floor(fixed/100); // chop off last 2 digits
    return fixed/Math.pow(10, places);
  }
}

class Arrays {
  join(firstArr, secondArr){
    for(var i = 0; i < secondArr.length; i++){
      firstArr.push(secondArr[i]);
    }

    return firstArr;
  }

  MakeListFromObjects(objs, key){
    var output = [];
    for(var i = 0; i < objs.length; i++){
      output.push(objs[i][key]);
    }

    return output;
  }

  Reverse(list)
  {
    var output = [];
    for(var i = list.length-1; i > -1; i--)
    {
      output.push(list[i]);
    }

    return output;
  }

  Diff(arr)
  {
      var output = [];
      for(var i = 0; i < arr.length; i++){
          var b = i == 0 ? 0 : arr[i-1];
          var a = arr[i];
          output.push(a-b);
      }

      return output;
  }

  Range(arr, start, end)
  {
    if(start < 0)
        start = 0;
      if(end > arr.length)
          end = arr.length;
      var output = [];
      for(var i = start; i < end; i++){
          output.push(arr[i]);
      }

      return output;
  }

  RollingMean(arr, window){
    var length = arr.length;
    var diff = length - window;
    var output = [];
    for(var i = 0; i < arr.length; i++){
      var s = i < window ? 0 : i;
      var e = i < window ? i+1 : window+i;
      var r = this.Range(arr, s, e);
      output.push(math.mean(r));
    }

    return output;
  }

  RemoveAllNulls(arr)
  {
    var output = [];
    for(var i = 0; i < arr.length; i++)
    {
      if(arr[i])
        output.push(arr[i]);
    }

    return output;
  }
}

class Timing{
  GetMilliseconds(timing){
      var output = 0;
      if(timing.days){
          output += timing.days * 24 * 60 * 60 * 1000;
      }

      if(timing.hours){
          output += timing.hours * 60 * 60 * 1000;
      }

      if(timing.minutes){
          output += timing.minutes * 60 * 1000;
      }

      if(timing.seconds){
          output += timing.seconds * 1000;
      }

      if(timing.milliseconds){
          output += timing.milliseconds;
      }

      return output;
  }

  TimingObjectToString(obj){
    var output = [];
    if(obj.days)
    {
      output.push(obj.days+"d");
    }
    if(obj.hours)
    {
      output.push(obj.hours+"h");
    }
    if(obj.minutes)
    {
      output.push(obj.minutes+"m");
    }
    if(obj.seconds)
    {
      output.push(obj.seconds+"s");
    }
    if(obj.milliseconds)
    {
      output.push(obj.milliseconds+"ms");
    }

    return output.join();
  }
}

module.exports = { Guid, SQLUtility, Strings, Numbers: new Numbers(), Arrays, Timing: new Timing() }
