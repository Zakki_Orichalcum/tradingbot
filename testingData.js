#!/usr/bin/env node

const A = require('./utilities').Arrays;
const Arrays = new A();
const math = require('mathjs');
const Algorithms = require('./utilities/Algorithms');
const sqlite3 = require('sqlite3');

function GetConnection(){
    global.db = new sqlite3.Database("data2.db");

    return global.db;
}

async function query(fn, query, values) {
    if (values) {
        fn = fn.bind(global.db, query, values);
    } else {
        fn = fn.bind(global.db, query);
    }
    return new Promise((resolve, reject) => {
        try {
            fn((err, rows) => {
                if (err) {
                    reject(
                        new Error(
                            `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                        ),
                    );
                } else {
                    resolve(rows);
                }
            });
        } catch (err) {
            reject(err);
        }
    });
}

