#!/usr/bin/env node
const bodyParser = require('body-parser');
const { time } = require('console');
const sqlite3 = require('sqlite3').verbose();
const dateformat = require('dateformat');
const express = require('express');
const BinanceAPI = require('./apis/BinanceAPI');
const app = express();
const http = require('http').Server(app);
const dbbuild = require('./dbbuildjs');
const seeded = require('./seededcurrency.json');
const fetch = require('node-fetch');
const SendingEmails = require('./utilities/sendEmail');

const DEFAULT_DB = 'data.db';
const symbolsCaredAbout = ["DOGEUSDT", "BTCUSDT", "USDTUSD", "ETHUSDT", "ETHBTC"];
const endpoint = 'https://api.binance.us';
const emailTiming = 1000 * 60 * 60 * 4;
// const PORT = process.env.PORT || 4427;

// app.use((req, res, next) => {
//     req.connection.setNoDelay(true);
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Headers', '*');
//     next();
// });

// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
// app.use(bodyParser.raw());

// app.get('/api/posts',
//     wrap(async function (req, res){
//         var conn = GetConnection();
//         var posts = await query(conn.all, `Select * from posts`);
//         var p = [];
//         for(var i = 0; i < posts.length; i++)
//         {
//             p.push(posts[i].id, decodeURIComponent(posts[i]));
//         }
//         var attentions = await query(conn.all, `Select * from attentions`);
//         var a = [];
//         for(var i = 0; i < attentions.length; i++)
//         {
//             a.push(attentions[i].id, decodeURIComponent(attentions[i]));
//         }

//         var result = { posts: p, attentions: a};
//         res.send(result);
//     }));

function timer(ms) { return new Promise( res => setTimeout(res, ms)) };

function GetConnection(){
        global.db = new sqlite3.Database(DEFAULT_DB);

        return global.db;
    }

async function query(fn, query, values) {
    if (values) {
        fn = fn.bind(global.db, query, values);
    } else {
        fn = fn.bind(global.db, query);
    }
    return new Promise((resolve, reject) => {
        try {
            fn((err, rows) => {
                if (err) {
                    reject(
                        new Error(
                            `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                        ),
                    );
                } else {
                    resolve(rows);
                }
            });
        } catch (err) {
            reject(err);
        }
    });
    }

// function wrap(func) {
//     return (req, res, next) => {
//         Promise.resolve(func(req, res, next)).catch(err => {
//             console.error(err);
//             res.status(503).json({ error: err.message });
//         });
//     };
// }

    async function GetLatestPriceInUSDForSymbol(symbol){
        var d = await fetch(`${endpoint}/api/v3/ticker/price?symbol=${symbol}USD`);
        var j = await d.json();

        //await this.AddPriceToTrends(symbol, j.price);

        return j.price;
    }

    async function Post()
    {
        var gt = await GrandTotal();
        var conn = await GetConnection();
        var posts = await query(conn.all, `Select * from posts`);
        var attentions = query(conn.all, `Select * from attentions`);
        console.clear();

        console.log("******************************************************************************************");
        console.log("");

        var remainder = 18 - attentions.length;
        for(var k = 0; k < attentions.length; k++) { console.log(decodeURIComponent(attentions[k].message)); }

        for(var i = 0; i < remainder; i++) { console.log("");}

        console.log("");
        console.log("******************************************************************************************\n");

        var qp = [];
        for(var k = 0; k < posts.length; k++) { console.log(decodeURIComponent(posts[k].message)); }
        var gtmsg = `Grand total | DOGE: ${gt.DOGE}, ETH: ${gt.ETH}, BTC: ${gt.BTC}, USDT: ${gt.USDT}`;
        qp.push(`('${gtmsg}')`);
        console.log(gtmsg);
        
    }

    async function GetExchangedInfo(){
        var d = await fetch(`https://api.binance.us/api/v1/exchangeInfo`);
        var j = await d.json();

        var orderBetweenSeconds = j.rateLimits[1].limit;
        var ordersPerDayLimt = j.rateLimits[2].limt;
        var serverTime = j.serverTime;

        var symbolInfo = {};
        var symbols = j.symbols;
        for(var i = 0; i < symbols.length; i++)
        {
            var sym = symbols[i];
            if(symbolsCaredAbout.indexOf(sym.symbol) > -1)
            {
                for(var k = 0; k < sym.filters.length; k++){
                    var filter = sym.filters[k];
                    sym[filter.filterType] = filter;
                }
                symbolInfo[sym.symbol] = sym;
            }
        }

        global.symbolInfo = symbolInfo;
    }

    async function GrandTotal()
    {
        var conn = await GetConnection();
        //var transactions = await query(conn.all, `Select * from transactions order by timestamp ASC`);
        var balances = await global.api.GetBalances();
        var totals = { DOGE: -seeded.DOGE, ETH: -seeded.ETH, BTC: -seeded.BTC, USDT: -seeded.USDT};

        var c = await query(conn.all, `Select * from commissionsPaid`);
        c.forEach(x => {
            totals.USDT -= x.amount;
        });

        var totalDollars = 0;
        var keys = Object.keys(totals);
        for(var i = 0; i < keys.length; i++){
            var b = balances[keys[i]];
            var balance = Number.parseFloat(b.free) + Number.parseFloat(b.locked) + Number.parseFloat(b.freeze) + Number.parseFloat(b.storage);
            totals[keys[i]] += balance;
            var price = await GetLatestPriceInUSDForSymbol(keys[i]);
            totalDollars += price * totals[keys[i]];
        }

        totals.Dollars = totalDollars;

        return totals;
    }

    async function SendEmail()
    {
        var ms = [];
        var gt = await GrandTotal();
        var conn = await GetConnection();
        var posts = await query(conn.all, `Select * from posts`);
        var attentions = await query(conn.all, `Select * from attentions`);

        ms.push("******************************************************************************************");
        ms.push("");

        var remainder = 18 - attentions.length;
        for(var k = 0; k < attentions.length; k++) { ms.push(decodeURIComponent(attentions[k].message)); }

        for(var i = 0; i < remainder; i++) { ms.push("");}

        ms.push("");
        ms.push("******************************************************************************************\n");

        var qp = [];
        for(var k = 0; k < posts.length; k++) { ms.push(decodeURIComponent(posts[k].message)); }
        var gtmsg = `Grand total | DOGE: ${gt.DOGE}, ETH: ${gt.ETH}, BTC: ${gt.BTC}, USDT: ${gt.USDT}`;
        qp.push(`('${gtmsg}')`);
        ms.push(gtmsg);

        var d = (new Date()).getTime() - emailTiming;
        var da = new Date(d);
        var errors = await query(conn.all, `Select * from errors where timestamp > '${dateformat(da, 'yyyy-mm-dd HH:MM:ss', true)}'`);
        ms.push("");
        for(var k = 0; k < errors.length; k++) { ms.push(`${errors[k].timestamp} - ${decodeURIComponent(errors[k].errorMsg)}`); }

        var str = ms.join('\n');
        var html = [];
        ms.forEach(v => {
            html.push(`<p>${v}</p>`);
        });
        var htmlstr = html.join('');

        var e = new SendingEmails("chaoszakkimon@gmail.com");
        await e.SendMail("zakkiorichalcum@protonmail.com", "Crypto Update", str, htmlstr);
    }

async function main() {
    await GetExchangedInfo();
    global.api = new BinanceAPI(global.symbolInfo);

    while(true)
    {
        await Post();

        await timer(10000);

        if(!global.emailTime || global.emailTime < Date.now)
        {
            global.emailTime = Date.now + emailTiming;

            await SendEmail();
        }
    }
    // console.log('Starting server.');
    // http.listen(PORT, () => {
    //     console.log(`Server started on http://localhost:${PORT}`);
    // });
}

main().catch(err => {
    console.log(err);
    process.exit(1);
});