const sqlite3 = require('sqlite3').verbose();
const dateformat = require('dateformat');
const dbbuild = require('./dbbuildjs');
const SQLUtility = require("./utilities").SQLUtility;
const SQL = new SQLUtility();
const Arrs = require("./utilities").Arrays;
const Arrays = new Arrs();
const Timing = require("./utilities").Timing;
const math = require("mathjs");
const Algorithms = require("./utilities/Algorithms");
const DC = require("./utilities/dataCollection");
const DataCollection = new DC();
const N = require("./utilities").Numbers;

const SendingEmails = require('./utilities/sendEmail');

const fetch = require('node-fetch');
const servicesJson = require('./services');
const auth = require('./auth.json').Binance;
const seeded = require('./seededcurrency.json');
const crypto = require('crypto-js');

const BinanceAPI = require("./apis/BinanceAPI");
const TestAPI = require("./apis/BinanceAPITester");

const PlaceAndWatchBot = require('./components/placeAndWatchBot');
const ShortBot = require('./components/shortBot');
const SimpleMeanAveragesBot = require('./components/SimpleMeanAveragesBot');
const ExponentialMeanAveragesBot = require('./components/ExponentialMeanAveragesBot');
const BollingerBandsBot = require('./components/BollingerBandsBot');
const MACDBot = require('./components/MACDBot');
const WatcherBot = require('./components/WatcherBot');
const ADXMomentumBot = require('./components/ADXMomentumBot');
const MachineLearningBot = require('./components/MachineLearningBot');
const BBAndRSIBot = require('./components/BBAndRSIBot');
const TriangularArbitrageBot = require('./components/TriangularArbitrageBot');
const KSTBot = require('./components/KSTBot');
const day = 24 * 60 * 60 * 1000;

const symbolsCaredAbout = ["DOGEUSDT", "BTCUSDT", "USDTUSD", "ETHUSDT", "ETHBTC"];
// const bodyParser = require('body-parser');
// const express = require('express');
// const app = express();
// const http = require('http').Server(app);

const timer = ms => new Promise( res => setTimeout(res, ms));

const DEFAULT_DB = 'data.db';
const endpoint = 'https://api.binance.us';
// //const endpoint = 'http://localhost:4425';

//const PORT = process.env.PORT || 13522;

// app.use((req, res, next) => {
//     req.connection.setNoDelay(true);
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Headers', '*');
//     next();
// });

// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
// app.use(bodyParser.raw());

// app.get('/api/updates',
//     wrap(async (req, res){
//         var result = {};

//         var conn = await GetConnection();
//         result.posts = await query(conn.all, `Select * from posts`);
//         result.attentions = await query(conn.all, `Select * from attentions`);

//         res.send(result);
//     }));

class MainProgram
{
    //#region Utilities
    GetConnection(){
        this.db = new sqlite3.Database(DEFAULT_DB);

        return this.db;
    }

    async query(fn, query, values) {
    if (values) {
        fn = fn.bind(this.db, query, values);
    } else {
        fn = fn.bind(this.db, query);
    }
    return new Promise((resolve, reject) => {
        try {
            fn((err, rows) => {
                if (err) {
                    reject(
                        new Error(
                            `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                        ),
                    );
                } else {
                    resolve(rows);
                }
            });
        } catch (err) {
            reject(err);
        }
    });
    }

    async OrderForSymbol(symbol, side, quantity){
        var q = {symbol:symbol, side:side, type:'MARKET', quantity: quantity, recvWindow: 5000, timestamp: Date.now()};
        //Post(`Making a new order for ${symbol} to ${side} ${quantity} at MARKET PRICE!`);

        quantity = Number(quantity).toFixed(8);

        var querystr = this.QueryString(q);
        var api = '/api/v3/order';
        var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        var d = await fetch(`${endpoint}${api}?${querystr}&signature=${hmac}`, { 
            method: 'POST',
            headers: {
                "X-MBX-APIKEY": auth.apiKey,
                "Content-Type": "application/json",
            }
        });
        var order = await d.json();
        console.log(order);
    }

    QueryString(obj){
        return Object.keys(obj).map((key) => {
            return `${key}=${obj[key]}`;
        }).join('&');
    }

    //#endregion

    //#region Messages
    async Post(attentions, posts, gt, sendEmail)
    {
        var conn = await this.GetConnection();
        await this.query(conn.run, `Delete from posts`);
        await this.query(conn.run, `Delete from attentions`);
        console.clear();

        console.log("******************************************************************************************");
        console.log("");

        var remainder = 18 - attentions.length;
        var att = [];
        for(var k = 0; k < attentions.length; k++) { console.log(attentions[k]); att.push(`('${encodeURIComponent(attentions[k])}')`);}

        for(var i = 0; i < remainder; i++) { console.log("");}

        console.log("");
        console.log("******************************************************************************************\n");

        if(att.length > 0)
            await this.query(conn.run, `Insert Into attentions (message) Values ${att.join(", ")}`);

        var qp = [];
        for(var k = 0; k < posts.length; k++) { console.log(posts[k]); qp.push(`('${encodeURIComponent(posts[k])}')`); }
        var gtmsg = `Grand total | DOGE: ${gt.DOGE}, ETH: ${gt.ETH}, BTC: ${gt.BTC}, USDT: ${gt.USDT} | $${gt.Dollars}`;
        qp.push(`('${gtmsg}')`);
        console.log(gtmsg);
        if(qp.length > 0)
            await this.query(conn.run, `Insert Into posts (message) Values ${qp.join(", ")}`);

        if(sendEmail)
            await this.SendEmail(attentions, posts, gt);
    }

    async SendEmail(attentions, posts, gt)
    {
        var ms = [];

        ms.push("******************************************************************************************");
        ms.push("");

        var remainder = 18 - attentions.length;
        for(var k = 0; k < attentions.length; k++) { ms.push(decodeURIComponent(attentions[k])); }

        for(var i = 0; i < remainder; i++) { ms.push("");}

        ms.push("");
        ms.push("******************************************************************************************\n");

        var qp = [];
        for(var k = 0; k < posts.length; k++) { ms.push(decodeURIComponent(posts[k])); }
        var gtmsg = `Grand total | DOGE: ${gt.DOGE}, ETH: ${gt.ETH}, BTC: ${gt.BTC}, USDT: ${gt.USDT} | $${gt.Dollars}`;
        qp.push(`('${gtmsg}')`);
        ms.push(gtmsg);

        // var d = (new Date()).getTime() - Timing.GetMilliseconds({hours:4});
        // var da = new Date(d);
        // var errors = await this.query(conn.all, `Select * from errors where timestamp > '${dateformat(da, 'yyyy-mm-dd HH:MM:ss', true)}'`);
        // ms.push("");
        // for(var k = 0; k < errors.length; k++) { ms.push(`${errors[k].timestamp} - ${decodeURIComponent(errors[k].errorMsg)}`); }

        var str = ms.join('\n');
        var html = [];
        ms.forEach(v => {
            html.push(`<p>${v}</p>`);
        });
        var htmlstr = html.join('');

        var e = new SendingEmails();
        await e.SendMail("Crypto Update", str, htmlstr);
    }

    async LogError(error){
        console.log(error);
        var conn = await this.GetConnection();

        return await this.query(conn.run, `INSERT INTO errors (errorMsg) ` +
            `Values ('${error}')`);
    }
    //#endregion

    //#region Pricing and Setup Information

    async GetLatestPriceInUSDForSymbol(symbol){
        var d = await fetch(`${endpoint}/api/v3/ticker/price?symbol=${symbol}USD`);
        var j = await d.json();

        //await this.AddPriceToTrends(symbol, j.price);

        return j.price;
    }

    async GrandTotal()
    {
        var conn = await this.GetConnection();
        //var transactions = await this.query(conn.all, `Select * from transactions order by timestamp ASC`);
        var balances = await this.apiTypes.Binance.GetBalances();
        var totals = { DOGE: -seeded.DOGE, ETH: -seeded.ETH, BTC: -seeded.BTC, USDT: -seeded.USDT};

        var c = await this.query(conn.all, `Select * from commissionsPaid`);
        c.forEach(x => {
            totals.USDT -= x.amount;
        });

        var totalDollars = 0;
        var keys = Object.keys(totals);
        for(var i = 0; i < keys.length; i++){
            var b = balances[keys[i]];
            var balance = Number.parseFloat(b.free) + Number.parseFloat(b.locked) + Number.parseFloat(b.freeze) + Number.parseFloat(b.storage);
            totals[keys[i]] += balance;
            var price = await this.GetLatestPriceInUSDForSymbol(keys[i]);
            totalDollars += price * totals[keys[i]];
        }

        totals.Dollars = totalDollars;

        return totals;
    }

    async GetExchangedInfo(){
        var d = await fetch(`https://api.binance.us/api/v1/exchangeInfo`);
        var j = await d.json();

        this.orderBetweenSeconds = j.rateLimits[1].limit;
        this.ordersPerDayLimt = j.rateLimits[2].limt;
        this.serverTime = j.serverTime;

        var symbolInfo = {};
        var symbols = j.symbols;
        for(var i = 0; i < symbols.length; i++)
        {
            var sym = symbols[i];
            if(symbolsCaredAbout.indexOf(sym.symbol) > -1)
            {
                for(var k = 0; k < sym.filters.length; k++){
                    var filter = sym.filters[k];
                    sym[filter.filterType] = filter;
                }
                symbolInfo[sym.symbol] = sym;
            }
        }

        this.symbolInfo = symbolInfo;
    }

    //#endregion

    async main() {
        //Post('Setting up database.');
        //const buildEngine = new dbbuild.SQLBuildEngine(DEFAULT_DB, true, false, false, false, true);
        //buildEngine.Add(new dbbuild.ExecuteOnce('./SQL/*.sql'));
        //await buildEngine.Run();
        //Post('DB complete');

        console.log(__dirname);
        console.log(process.cwd());

        await DataCollection.CleanTrends();

        await this.GetExchangedInfo();

        this.services = [];

        var symbols = {
            "DOGEUSDT" : { combinedSymbol: 'DOGEUSDT', primarySymbol: 'DOGE', secondarySymbol: 'USDT' },
            "ETHUSDT" : { combinedSymbol: 'ETHUSDT', primarySymbol: 'ETH', secondarySymbol: 'USDT' },
            "BTCUSDT" : { combinedSymbol: 'BTCUSDT', primarySymbol: 'BTC', secondarySymbol: 'USDT' },
            "ETHBTC" : { combinedSymbol: 'ETHBTC', primarySymbol: 'ETH', secondarySymbol: 'BTC' },
        };

        var keys = Object.keys(symbols);
        keys.forEach( v => {
            var s = symbols[v];
            var eInfo = this.symbolInfo[s.combinedSymbol];
            var pSize = N.countDecimalsToAOne(eInfo["PRICE_FILTER"].tickSize);
            var qSize = N.countDecimalsToAOne(eInfo["LOT_SIZE"].stepSize);

            s.priceSize = pSize;
            s.quantitySize = qSize;

            symbols[v] = s;
        });

        this.apiTypes = {
            "Tester": new TestAPI(this.symbolInfo),
            "Binance" : new BinanceAPI(this.symbolInfo)//,
            //"Bittrex" : new BittrexAPI()
        };

        var databasePath = `${process.cwd()}/${DEFAULT_DB}`;

        for(var i = 0; i < servicesJson.services.length; i++){
            var s = servicesJson.services[i];

            var apis = [];
            s.apis.forEach(x => {
                apis.push(this.apiTypes[x]);
            });

            switch(s.BotType)
            {
                case "PlaceAndWatchBot":
                    this.services.push(new PlaceAndWatchBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "MACDBot":
                    this.services.push(new MACDBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "ShortBot":
                    this.services.push(new ShortBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "SimpleMeanAveragesBot":
                    this.services.push(new SimpleMeanAveragesBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "BollingerBandsBot":
                    this.services.push(new BollingerBandsBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "BBAndRSIBot":
                    this.services.push(new BBAndRSIBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "WatcherBot":
                    this.services.push(new WatcherBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "ADXMomentumBot":
                    this.services.push(new ADXMomentumBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "MachineLearningBot":
                    this.services.push(new MachineLearningBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "TriangularArbitrageBot":
                    this.services.push(new TriangularArbitrageBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                    break;
                case "KSTBot":
                    this.services.push(new KSTBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                case "ExponentialMeanAveragesBot":
                    this.services.push(new ExponentialMeanAveragesBot(s, apis, symbols, this.symbolInfo, this.serverTime, databasePath));
                default:
                    continue;
            }
        }

        var symbolsToWatchObj = {};
        for(var j = 0; j < this.services.length; j++){
            var service = this.services[j];
            await service.Initialize();
            if(!symbolsToWatchObj[service.symbol.combinedSymbol])
                symbolsToWatchObj[service.symbol.combinedSymbol] = "";
        }

        var amountForKlines = 200;
        var symbolsToWatch = Object.keys(symbolsToWatchObj);

        // console.log('Starting server.');
        // http.listen(PORT, () => {
        //     console.log(`Server started on http://localhost:${PORT}`);
        // });


        while(true){
            try{
                var posts = [];
                var attentions = [];
                var sendEmail = false;

                for(var s = 0; s < symbolsToWatch.length; s++){
                    await DataCollection.SetupKlines(symbolsToWatch[s], amountForKlines);
                    await DataCollection.AddLatestPriceToTrends(symbolsToWatch[s]);
                    await DataCollection.SetTrendKlines(symbolsToWatch[s], {minutes:1});
                    await DataCollection.SetTrendKlines(symbolsToWatch[s], {minutes:5});
                    await DataCollection.SetTrendKlines(symbolsToWatch[s], {hours:4});
                    await DataCollection.SetTrendKlines(symbolsToWatch[s], {minutes:2});
                    //await DataCollection.SetTrendKlines(symbolsToWatch[s], {days:1});
                }

                for(var j = 0; j < this.services.length; j++){
                    var service = this.services[j];
                    await service.run();

                    var p = service.postingMessages;
                    var a = service.topMessages;
                    if(service.sendEmail)
                        sendEmail = true;

                    posts = Arrays.join(posts, p);
                    attentions = Arrays.join(attentions, a);
                }

                var gt = await this.GrandTotal();

                await this.Post(attentions, posts, gt, sendEmail);

                await timer(1000);
            }
            catch(ex)
            {
                console.log(ex);
                await this.LogError(JSON.stringify(ex));
            }
        }
    }
}

module.exports = MainProgram;