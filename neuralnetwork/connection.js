const r = require('../utilities/RNG');
const RNG = new r();

class Connection {
  constructor(from, to, weight) {
    this.from = from;
    this.to = to;
    this.weight = weight ? weight : RNG.nextFloat();
    this.change = 0;
  }

  toJSON() {
    return {
      change: this.change,
      weight: this.weight,
      from: this.from.id,
      to: this.to.id
    }
  }

  setWeight(w) {
    this.weight = w;
  }

  setChange(val) {
    this.change = val;
  }
}

module.exports = Connection;