
const Connection = require('./connection');
const uid = require('./uid');
const r = require('../utilities/RNG');
const sigmoid = require('./sigmoid');
const RNG = new r();
const math = require('mathjs');

class Neuron {
  constructor(neuronJson) {
    this.biasList = [-3, -2, -1, 0, 1, 2, 3];
    this.inputConnections = []; //neuronJson ? this.mapConnections(neuronJson.inputConnections) : [];
    this.outputConnections = []; //neuronJson ? this.mapConnections(neuronJson.outputConnections): [];
    this.bias = neuronJson ? (neuronJson.bias >0 && neuronJson.bias < 1 ? 
      this.getBiasFromFloat(neuronJson.bias) : math.ceil(neuronJson.bias)) : 
      this.getRandomBias();
    // delta is used to store a percentage of change in the weight
    this.delta = neuronJson ? neuronJson.delta : 0;
    this.output = neuronJson ? neuronJson.output : 0;
    this.error = neuronJson ? neuronJson.error : 0;
    this.id = neuronJson ? neuronJson.id : uid();

  }

  toJSON() {
    return {
      id: this.id,
      delta: this.delta,
      output: this.output,
      error: this.error,
      bias: this.bias,
      inputConnections: this.inputConnections.map(i => i.toJSON()),
      outputConnections: this.outputConnections.map(i => i.toJSON())
    }
  }

  mapConnections(connectionsJson)
  {
    var connections = [];

    for(var i = 0; i < connectionsJson.length; i++){
      var c = connectionsJson[i];
      connections.push(new Connection(null, null, c.weight, c.from, c.to));
    }

    return connections;
  }

  getBiasFromFloat(b)
  {
      return math.floor(b * 7) - 3;
  }
  
  getRandomBias() {
    return RNG.choice(this.biasList); 
  }

  addInputConnection(connection) {
    this.inputConnections.push(connection)
  }

  addOutputConnection(connection) {
    this.outputConnections.push(connection)
  }

  setBias(val) {
    this.bias = val
  }

  setOutput(val) {
    this.output = val
  }

  setDelta(val) {
    this.delta = val
  }

  setError(val) {
    this.error = val
  }

  run(input)
  {
    if(input)
    {
      this.output = input;
    }
    else
    {
      var connectionsValue = 0; var weightAndOutput = [];
      this.inputConnections.forEach((conn)  => {
        var val = conn.weight * conn.from.output;
        weightAndOutput.push(`(${conn.from.id} | W${conn.weight}, O${conn.from.output}, V${val})`);
        connectionsValue += val;
      });

      this.output = sigmoid(this.bias + connectionsValue);

      //console.log(`ID: ${this.id}, bias: ${this.bias}, CV: ${connectionsValue} Out: ${this.output}\nc: \n${weightAndOutput.join('\n')}`);
    }
  }
}

module.exports = Neuron