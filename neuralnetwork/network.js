const sigmoid = require('./sigmoid');
const Connection = require('./connection');
const Layer = require('./layer');

class Network {
  constructor(numberOfLayers, networkJson) {
    this.layers = [];
    this.neurons = {};
    for(var i = 0; i < numberOfLayers.length; i++)
    {
      if(networkJson){
        this.layers.push(new Layer(numberOfLayers[i], networkJson.layers[i]));
      }
      else{
        this.layers.push(new Layer(numberOfLayers[i])); 
      }

      var n = this.layers[this.layers.length-1].neurons;
      for(var ni = 0; ni < n.length; ni++)
      {
        this.neurons[n[ni].id] = { layer: i, neuron: ni};
      }
    }
    this.learningRate = 0.3  // multiply's against the input and the delta then adds to momentum
    this.momentum =  0.1       // multiply's against the specified "change" then adds to learning rate for change
    this.iterations = 0;

    if(networkJson)
    {
      //console.log('new layer connections');
      this.connectFromJson(networkJson);
    }
    else
    {
      //console.log('old layer connections');
      this.connectLayers();
    }
  }

  toJSON() {
    return {
      learningRate: this.learningRate,
      iterations: this.iterations,
      layers: this.layers.map(l => l.toJSON())
    }
  }

  setLearningRate(value) {
    this.learningRate = value
  }

  setIterations(val) {
    this.iterations = val
  }

  connectFromJson(networkJson)
  {
    for (var l = 0; l < networkJson.layers.length-1; l++) {
      var thisLayer = networkJson.layers[l];
      //console.log(thisLayer);
      for (var n = 0; n < thisLayer.length; n++) {
        var thisNeuron = thisLayer[n];
        //console.log(thisNeuron);
        for(var c = 0; c < thisNeuron.outputConnections.length; c++)
        {
          var con = thisNeuron.outputConnections[c];
          var fromT = this.neurons[con.from];
          var toT = this.neurons[con.to];
          var connection = new Connection(this.layers[fromT.layer].neurons[fromT.neuron], this.layers[toT.layer].neurons[toT.neuron], con.weight);
          this.layers[fromT.layer].neurons[fromT.neuron].addOutputConnection(connection);
          this.layers[toT.layer].neurons[toT.neuron].addInputConnection(connection);
        }
      }
    }
  }

  connectLayers() {
    for (var layer = 1; layer < this.layers.length; layer++) {
      const thisLayer = this.layers[layer]
      const prevLayer = this.layers[layer - 1]
      for (var neuron = 0; neuron < prevLayer.neurons.length; neuron++) {
        for(var neuronInThisLayer = 0; neuronInThisLayer < thisLayer.neurons.length; neuronInThisLayer++) {
          const connection = new Connection(prevLayer.neurons[neuron], thisLayer.neurons[neuronInThisLayer])
          prevLayer.neurons[neuron].addOutputConnection(connection)
          thisLayer.neurons[neuronInThisLayer].addInputConnection(connection)
        }
      }
    }
  }

  // When training we will run this set of functions each time
  train(input, output) {
    this.activate(input)

    // Forward propagate
    this.runInputSigmoid()

    // backpropagate
    this.calculateDeltasSigmoid(output)
    this.adjustWeights()
    // console.log(this.layers.map(l => l.toJSON()))
    this.setIterations(this.iterations + 1)
  }

  activate(values) {
    this.layers[0].neurons.forEach((n, i) => {
      n.setOutput(values[i])
    })
  }

  run(inputs) {
    // this.activate(inputs);
    // // For now we only use sigmoid function
    // return this.runInputSigmoid()

    //console.log(this.layers.length);

    for(var i = 0; i < this.layers.length; i++)
    {
      if(i == 0)
      {
        this.layers[i].run(inputs);
      }
      else{
        this.layers[i].run();
      }
    }

    var finalLayerNeurons = this.layers[this.layers.length-1].neurons;
    var output = [];
    for(var i = 0; i < finalLayerNeurons.length; i++)
    {
      output.push(finalLayerNeurons[i].output);
    }

    //console.log("\t \t Output: " + [output.join(", ")]);
    return output;
  }

  runInputSigmoid() {
    for (var layer = 1; layer < this.layers.length; layer++) {
      for (var neuron = 0; neuron < this.layers[layer].neurons.length; neuron++) {
        const bias = this.layers[layer].neurons[neuron].bias;
        // For each neuron in this layer we compute its output value

        const connectionsValue = this.layers[layer].neurons[neuron].inputConnections.reduce((prev, conn)  => {
          const val = conn.weight * conn.from.output;
          return prev + val;
        }, 0);

        var sig = sigmoid(bias + connectionsValue);

        //console.log(`Connection Value: ${connectionsValue} Output: ${sig}`);
        this.layers[layer].neurons[neuron].setOutput(sig);
      }
    }

    return this.layers[this.layers.length - 1].neurons.map(n => n.output)
  }

  calculateDeltasSigmoid(target) {
    for (let layer = this.layers.length - 1; layer >= 0; layer--) {
      const currentLayer = this.layers[layer];

      for (let neuron = 0; neuron < currentLayer.neurons.length; neuron++) {
        const currentNeuron = currentLayer.neurons[neuron];
        let output = currentNeuron.output;

        let error = 0;
        if (layer === this.layers.length -1 ) {
          // Is output layer
          error = target[neuron] - output;
          //console.log('calculate delta, error, last layer', error)
        }
        else {
          // Other than output layer
          for (let k = 0; k < currentNeuron.outputConnections.length; k++) {
            const currentConnection = currentNeuron.outputConnections[k]
            error += currentConnection.to.delta * currentConnection.weight
            //console.log('calculate delta, error, inner layer', error)
          }


        }
        currentNeuron.setError(error)
        currentNeuron.setDelta(error * output * (1 - output))
      }
    }
  }

  adjustWeights() {
    
    for (let layer = 1; layer <= this.layers.length -1; layer++) {
      const prevLayer = this.layers[layer - 1]
      const currentLayer = this.layers[layer]

      for (let neuron = 0; neuron < currentLayer.neurons.length; neuron++) {
         const currentNeuron = currentLayer.neurons[neuron]
         let delta = currentNeuron.delta
         
        for (let i = 0; i < currentNeuron.inputConnections.length; i++) {
          const currentConnection = currentNeuron.inputConnections[i]
          let change = currentConnection.change
         
          change = (this.learningRate * delta * currentConnection.from.output)
              + (this.momentum * change);
          
          currentConnection.setChange(change)
          currentConnection.setWeight(currentConnection.weight + change)
        }

        currentNeuron.setBias(currentNeuron.bias + (this.learningRate * delta))
       
      }
    }
  }

}

module.exports = Network;
