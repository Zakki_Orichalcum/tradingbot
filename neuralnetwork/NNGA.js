const math = require('mathjs');
const Network = require('../neuralnetwork/').Network;
const sqlite3 = require('sqlite3').verbose();
const A = require('../utilities').Arrays;
const Arrays = new A();
const r = require('../utilities/RNG');
const RNG = new r();
const uid = require('./uid');

class NNGAMachineLearning
{
    constructor(tableName, continueFromLast, dataSet, population, network)
    {
        //this.database = new Database("test.db");
        this.agents = [];
        this.tableName = tableName;
        this.continueFromLast = continueFromLast;
        this.dataSet = dataSet;
        this.population = population;
        this.network = network;

        this.mutationChance = 0.5;
        this.commissionLoss = 0.001;
    }

    GetConnection(){
        global.db = new sqlite3.Database("test.db");

        return global.db;
    }

    async query(fn, query, values) {
        if (values) {
            fn = fn.bind(global.db, query, values);
        } else {
            fn = fn.bind(global.db, query);
        }
        return new Promise((resolve, reject) => {
            try {
                fn((err, rows) => {
                    if (err) {
                        reject(
                            new Error(
                                `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                            ),
                        );
                    } else {
                        resolve(rows);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    async Get(query)
    {
        var conn = this.GetConnection();
        return await this.query(conn.get, query);
    }

    async GetAll(query)
    {
        var conn = this.GetConnection();
        return await this.query(conn.all, query);
    }

    async Run(query)
    {
        var conn = this.GetConnection();
        return await this.query(conn.run, query);
    }

    async Execute(generations)
    {
        var agents = []; var startingPoint = 0;
        if(this.continueFromLast)
        {
            var s = await this.Get(`Select * from ${this.tableName} order by generation DESC LIMIT 1`);
            startingPoint = s ? s.generation : 0;
            agents = await this.GetAgentsForGeneration(startingPoint);
        }

        for(var i = startingPoint; i < generations + startingPoint; i++)
        {
            if(i > 30)
            {
                await this.Run(`Delete from ${this.tableName} where generation < ${i-10}`);
            }
            agents = await this.RunEvalutionOfGeneration(agents, i);
            console.log(`Completed generation ${i}. Top fitness is ${agents[0].fitness}`);
        }
            
        return agents[0];
    }

    async RunEvalutionOfGeneration(agents, generation)
    {
        if(generation == 0 || !agents || agents.length == 0)
        {
            agents = this.GenerateAgents(this.population, this.network);
        }
        else{
            agents = await this.Selection(agents.length, generation);
            agents = this.Crossover(agents, this.population);
            agents = this.Mutation(agents);
        }
        //console.log(agents);
        agents = this.Fitness(agents);

        await this.AddCurrentAgentsToDatabase(agents, generation);

        agents.sort((a, b) => { return a.fitness < b.fitness ? 1 : a.fitness > b.fitness ? -1 : 0});
        return agents;
    }

    async GetAgentsForGeneration(generation)
    {
        var s = await this.GetAll( `Select * from ${this.tableName} where generation = ${generation}`);
        var output = [];
        for(var i = 0; i < s.length; i++){
            var a = new Agent(this.network, new Network(this.network, JSON.parse(s[i].weights)));
            output.push(a);
        }

        return output;
    }

    async AddCurrentAgentsToDatabase(agents, generation)
    {
        for(var i = 0; i < agents.length; i++){
            await this.Run( `Insert Into ${this.tableName} (generation, fitness, weights) Values (${generation}, ${agents[i].fitness}, '${JSON.stringify(agents[i].NeuralNetwork.toJSON())}')`);
        }
    }

    GenerateAgents(population, network)
    {
        var output = [];
        for(var i = 0; i < population; i++)
        {
            output.push(new Agent(network));
        }

        return output;
    }

    Fitness(agents)
    {
        const DogeStarting = 40;
        //console.log(agents);
        var output = {}; var str = "";
        for(var y = 0; y < this.dataSet.length; y++)
        {
            var ds = this.dataSet[y];
            for(var j = 0; j < ds.normalized.length; j++)
            {
                var close = Number(ds.actual[j].close);
                //console.log(close);
                for(var i = 0; i < agents.length; i++)
                {
                    var answer = agents[i].NeuralNetwork.run(ds.normalized[j]);

                    if(!output[i])
                    {
                        var s = {USDT: 10, last: 0, history: [], commission: { USDT: 0}};
                        output[i] = s;
                    }
                    
                    if(!output[i][ds.primary+ds.secondary])
                    {
                        var s = {USDT: 10, last: 0, history: [], commission: { USDT: 0}};
                        if(ds.primary == "DOGE")
                        {
                            s.DOGE = DogeStarting;
                            s.commission.DOGE = 0;
                        }
                        if(ds.primary == "ETH")
                        {
                            s.ETH = 0.003;
                            s.commission.ETH = 0;
                        }
                        if(ds.primary == "BTC")
                        {
                            s.commission.BTC = 0;
                            s.BTC = 0.0004;
                        }
                        //output[i] = { DOGE: 150, ETH: 0.02, BTC: 0.001, USDT: 50, last: 0, history: []};
                        output[i][ds.primary+ds.secondary] = s;
                    }
    
                    var max = answer[0];
                    var signal = 0;
                    for(var k = 1; k < answer.length; k++)
                    {
                        //console.log(answer[k], max, answer[k] > max);
                        if(answer[k] > max)
                        {
                            max = answer[k];
                            signal = k;
                        }
                    }
                    //console.log(signal);
                    // console.log(max, signal, close);
                    var o = output[i][ds.primary+ds.secondary];
                    
                    if(j == 0)
                    {
                        o[ds.secondary] += 10;
                    }
                    switch(signal){
                        case 1: //Buy
                            if(o.last != 1)
                            {
                                var b = o[ds.secondary];
                                var quantity = b/close;
                                o.history.push(1);
                                o[ds.primary] += quantity - (this.commissionLoss * quantity);
                                o[ds.secondary] -= b;
                                o.last = 1;
                                o.commission[ds.primary] += (this.commissionLoss * quantity);
                                break;
                            }
                        case 2: //Sell
                            if(o.last != 2)
                            {
                                var a = o[ds.primary];
                                var money = a*close;
                                o.history.push(2);
                                o[ds.primary] -= a;
                                o[ds.secondary] += money - (this.commissionLoss * money);
                                o.commission[ds.secondary] += (this.commissionLoss * money);
                                o.last = 2;
                                break;
                            }
                        case 0:
                        default:
                            o.history.push(0);
                            break;
                    }
                    //console.log(ds, o);
                    output[i][ds.primary+ds.secondary] = o;
    
                    if(y == this.dataSet.length-1 && j == ds.normalized.length-1)
                    {
                        var sum = 0; var t = {"USDT":0}; var commissionSum = 0;
                        for(var h = 0; h < this.dataSet.length; h++)
                        {
                            var dsc = this.dataSet[h];
                            var c = Number(dsc.actual[dsc.actual.length-1].close);
                            //console.log(dsc.primary, output[i]);
                            t[dsc.primary] = output[i][dsc.primary+dsc.secondary][dsc.primary];
                            t[dsc.secondary] += output[i][dsc.primary+dsc.secondary][dsc.secondary];
                            // var d = output[i][dsc.primary+dsc.secondary][dsc.secondary] / c;
                            // sum += output[i][dsc.primary+dsc.secondary][dsc.primary] + d;
                            var d = output[i][dsc.primary+dsc.secondary][dsc.primary] * c;
                            sum += output[i][dsc.primary+dsc.secondary][dsc.secondary] + d;
                            var e = output[i][dsc.primary+dsc.secondary].commission[dsc.primary] * c;
                            commissionSum += e + output[i][dsc.primary+dsc.secondary].commission[dsc.secondary];
                            output[i].history = output[i].history.concat(output[i][dsc.primary+dsc.secondary].history);
                        }

                        agents[i].fitness = sum;
                        str += `${i} fitness: ${agents[i].fitness} commissionLoss: ${commissionSum} final: BTC ${t["BTC"]} DOGE ${t["DOGE"]} ETH ${t["ETH"]} USDT $${t["USDT"]} hist: ${output[i].history.join('')}\n`;
                        //str += `${i} fitness: ${agents[i].fitness}  final:DOGE ${t["DOGE"]} USDT $${t["USDT"]} hist: ${output[i].history.join('')}\n`;
                    
                    }
                }
            }
        }
        
        console.log(str);

        return agents;
    }
            
    // Fitness(agents, x, y)
    // {
    //     for(var i = 0; i < agents.length; i++)
    //     {
    //         var yhat = agents[i].NeuralNetwork.propagate(x);
    //         var cost = [];
    //         for(var j = 0; j < yhat.length; j++)
    //         {
    //             cost.push(math.pow(yhat[j] - y, 2));
    //         }
    //         agents[i].fitness = math.sum(cost);
    //     }
        
    //     return agents;
    // }

    async Selection(length, generation)
    {
        var u = math.ceil(0.15 * length);
        if(u < 3) u = 3;

        var d = await this.GetAll(`Select * from [${this.tableName}] where generation = ${generation-1} Order by fitness DESC LIMIT ${u}`);
        var agents = [];
        d.forEach(v => {
            var a = new Agent(this.network, JSON.parse(v.weights));
            a.fitness = v.fitness;
            agents.push(a);
        });
        //console.log(agents);
        return agents;
        
    }

    Flatten(json)
    {
        var output = [];
        for(var i = 0; i < json.layers.length; i++)
        {
            var layer = json.layers[i];
            for(var j = 0; j < layer.length; j++)
            {
                var neuron = layer[j];
                for(var c = 0; c < neuron.outputConnections.length; c++)
                {
                    output.push(neuron.outputConnections[c].weight);
                }
                output.push(neuron.bias/3);
            }
        }

        return output;
    }

    Unflatten(flattened, json)
    {
        var index = 0;
        for(var i = 0; i < json.layers.length; i++)
        {
            for(var j = 0; j < json.layers[i].length; j++)
            {
                for(var c = 0; c < json.layers[i][j].outputConnections.length; c++)
                {
                    json.layers[i][j].outputConnections[c].weight = flattened[index++];
                }
                json.layers[i][j].bias = flattened[index++]*3;
            }
        }
            
        return json;
    }

    // Unflatten(flattened, shapes)
    // {
    //     var newarray = [];
    //     var index = 0;
    //     shapes.forEach(shape => {
    //         var size = math.prod(shape);
    //         newarray.push(math.reshape(Arrays.Range(flattened, index, index + size), shape));
    //         index += size;
    //     });
            
    //     return newarray
    // }

    ReplaceUIDs(networkJson)
    {
        var outputStr = JSON.stringify(networkJson);
        for (var l = 0; l < networkJson.layers.length-1; l++) {
            var thisLayer = networkJson.layers[l];
            for (var n = 0; n < thisLayer.length; n++) {
                var thisNeuron = thisLayer[n];
                var regex = new RegExp(thisNeuron.id, "g");
                outputStr = outputStr.replace(regex, uid());
                // for(var c = 0; c < thisNeuron.outputConnections.length; c++)
                // {
                //     var con = thisNeuron.outputConnections[c];
                //     var fromT = this.neurons[con.from];
                //     var toT = this.neurons[con.to];
                //     var connection = new Connection(this.layers[fromT.layer].neurons[fromT.neuron], this.layers[toT.layer].neurons[toT.neuron], con.weight);
                //     this.layers[fromT.layer].neurons[fromT.neuron].addOutputConnection(connection);
                //     this.layers[toT.layer].neurons[toT.neuron].addInputConnection(connection);
                // }
            }
        }
        
        return JSON.parse(outputStr);
    }

    Crossover(agents, pop_size)
    {
        var offspring = [];
        var copy = new Agent(this.network, this.ReplaceUIDs(agents[0].NeuralNetwork.toJSON()));

        var parent1 = agents[0];
        var parent2 = agents[1];   

        var parent1Json = parent1.NeuralNetwork.toJSON();
        var parent2Json = parent2.NeuralNetwork.toJSON();

        var genes1 = this.Flatten(parent1Json);
        var genes2 = this.Flatten(parent2Json);
        
        var split = RNG.nextRange(0,genes1.length);
        var child1Genes = Arrays.Range(genes1, 0, split).concat(Arrays.Range(genes2, split, genes2.length));
        var child2Genes = Arrays.Range(genes2, 0, split).concat(Arrays.Range(genes1, split, genes1.length));

        var up1 = this.Unflatten(child1Genes, this.ReplaceUIDs(parent1Json));
        var up2 = this.Unflatten(child2Genes, this.ReplaceUIDs(parent2Json));

        //console.log(JSON.stringify(up1));
        //console.log(JSON.stringify(up2));

        var child1 = new Agent(this.network, up1);
        var child2 = new Agent(this.network, up2);
        
        offspring.push(child1);
        offspring.push(child2);

        parent1 = agents[0];
        parent2 = agents[2];   

        parent1Json = parent1.NeuralNetwork.toJSON();
        parent2Json = parent2.NeuralNetwork.toJSON();

        genes1 = this.Flatten(parent1Json);
        genes2 = this.Flatten(parent2Json);
        
        split = RNG.nextRange(0,genes1.length);
        child1Genes = Arrays.Range(genes1, 0, split).concat(Arrays.Range(genes2, split, genes2.length));
        child2Genes = Arrays.Range(genes2, 0, split).concat(Arrays.Range(genes1, split, genes1.length));
        
        up1 = this.Unflatten(child1Genes, this.ReplaceUIDs(parent1Json));
        up2 = this.Unflatten(child2Genes, this.ReplaceUIDs(parent2Json));

        //console.log(JSON.stringify(up1));
        //console.log(JSON.stringify(up2));

        child1 = new Agent(this.network, up1);
        child2 = new Agent(this.network, up2);
        
        offspring.push(child1);
        offspring.push(child2); 


        for(var i = 0; i < pop_size - agents.length - 4; i++)
        {
            parent1 = agents[RNG.nextRange(0, agents.length)];
            parent2 = agents[RNG.nextRange(0, agents.length)];   

            parent1Json = parent1.NeuralNetwork.toJSON();
            parent2Json = parent2.NeuralNetwork.toJSON();

            genes1 = this.Flatten(parent1Json);
            genes2 = this.Flatten(parent2Json);
            
            split = RNG.nextRange(0,genes1.length);
            child1Genes = Arrays.Range(genes1, 0, split).concat(Arrays.Range(genes2, split, genes2.length));
            child2Genes = Arrays.Range(genes2, 0, split).concat(Arrays.Range(genes1, split, genes1.length));
            
            up1 = this.Unflatten(child1Genes, this.ReplaceUIDs(parent1Json));
            up2 = this.Unflatten(child2Genes, this.ReplaceUIDs(parent2Json));

            //console.log(JSON.stringify(up1));
            //console.log(JSON.stringify(up2));

            child1 = new Agent(this.network, up1);
            child2 = new Agent(this.network, up2);
            
            offspring.push(child1);
            offspring.push(child2);
        }
            
        agents = [copy].concat(agents.concat(offspring));
        agents = Arrays.Range(agents, 0, pop_size);
        //console.log(agents);
        return agents;
    }
            

    Mutation(agents)
    {
        for(var i = 1; i < agents.length; i++)
        {
            var agent = agents[i];
            if(RNG.nextFloat() <= this.mutationChance || (i == 1))
            {
                console.log(`MUTATIONS! ${i}`);
                var shapes = agent.NeuralNetwork.toJSON();

                var flattened = this.Flatten(shapes);
                //console.log(flattened);
                var randint = RNG.nextRange(0,flattened.length-1);
                var randint2 = RNG.nextRange(randint+1, flattened.length);

                for(var j = randint; j < randint2; j++)
                {
                    var changed = RNG.nextFloat();
                    //console.log(flattened[i], changed);
                    flattened[j] = changed;
                }
                //console.log(randint, randint2);

                agent.NeuralNetwork = new Network(this.network, this.Unflatten(flattened, shapes));
            }
        }
        
        return agents 
    }
            
}

class Agent
{
    constructor(network, json)
    {
        this.NeuralNetwork = new Network(network, json);
        //console.log(JSON.stringify(this.NeuralNetwork.toJSON()));
        this.fitness = 0;
    }
}
        
        

// class NeuralNetwork
// {
//     constructor(network, w)
//     {
//         this.weights = [];
//         this.activations = [];

//         for(var i = 0; i < network.layers.length; i++)
//         {
//             var layer = network.layers[i];
//             var inputsize = i == 0 ? layer[0] : network.layers[i-1][1];
//             var outputsize = layer[1];
//             var activation = layer[2];
//             this.weights.push(RNG.nextRange(inputsize,outputsize));
//             this.activations.push(activation);
//         }

//         if(w)
//             this.weights = w;
//     }
    
//     propagate(data)
//     {
//         var input_data = data; var a = null;
//         for(var i = 0; i < this.weights.length; i++)
//         {
//             var z = math.dot(input_data, this.weights[i]);
//             a = this.activations[i](z);
//             input_data = a;
//         }
            
//         var yhat = a;
//         return yhat;
//     }
// }


module.exports = NNGAMachineLearning;