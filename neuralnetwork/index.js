const Network = require('./network');
const Layer = require('./layer');
const Neuron = require('./neuron');
const Connection = require('./connection');

module.exports = {
  Network,
  Layer,
  Neuron,
  Connection
}