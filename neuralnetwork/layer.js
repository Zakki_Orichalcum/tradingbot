const Neuron = require('./neuron');

class Layer {
  constructor(numberOfNeurons, neuronJson) {
    var neurons = []
    for (var j = 0; j < numberOfNeurons; j++) {
      // const value = Math.random()
      var neuron = (neuronJson) ? new Neuron(neuronJson[j]) : new Neuron();
      // Neurons in other than initial layer have a bias value
      
      neurons.push(neuron)
    }
    
    this.neurons = neurons;
  }

  toJSON() {
    return this.neurons.map(n => {
      return n.toJSON()
    })
  }

  run(inputs)
  {
    this.neurons.forEach((n, i) => {
      n.run(inputs ? inputs[i] : null)
    })
  }
}

module.exports = Layer