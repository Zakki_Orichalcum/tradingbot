CREATE TABLE "commissionsPaid" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"timestamp"	datetime DEFAULT current_timestamp,
	"symbol"	TEXT,
	"amount"	INTEGER,
	"bot"	INTEGER
);

CREATE TABLE "orders" (
	"id"	INTEGER,
	"amount"	INTEGER,
	"pricePaying"	INTEGER,
	"timestamp"	datetime DEFAULT current_timestamp,
	"condition"	TEXT,
	"symbol"	TEXT,
	"origClientOrderId"	TEXT,
	"side"	TEXT,
	"bound"	REAL,
	"bot"	INTEGER,
	PRIMARY KEY("id")
);

CREATE TABLE "taxes" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"taxAmount"	REAL
);

CREATE TABLE "transactions" (
	"transactionId"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"timestamp"	datetime DEFAULT current_timestamp,
	"symbol"	TEXT,
	"transactionType"	TEXT,
	"priceInUsd"	REAL,
	"amount"	INTEGER,
	"bot"	INTEGER
);

CREATE TABLE "lockedAmount" (
	"id"	INTEGER,
	"lockedDuringAction"	TEXT,
	"amount"	INTEGER,
	"symbolLocked"	INTEGER,
	"unlockAt"	REAL,
	PRIMARY KEY("id")
);

CREATE TABLE "errors" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"timestamp"	datetime DEFAULT current_timestamp,
	"errorMsg"	TEXT
);

CREATE TABLE "trends" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"symbol"	TEXT,
	"price"	REAL,
	"timestamp"	datetime DEFAULT current_timestamp
);

CREATE TABLE "botAllowance" (
	"id"	INTEGER,
	"symbol"	TEXT,
	"allowance"	REAL,
	"bot"	INTEGER,
	PRIMARY KEY("id")
);

CREATE TABLE "1dKlines" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"symbol"	TEXT,
	"date"	INTEGER,
	"open"	REAL,
	"high"	REAL,
	"low"	REAL,
	"close"	REAL,
	"ma20"	REAL,
	"ma50"	REAL,
	"ma200"	REAL,
	"mplus"	REAL,
	"mminus"	REAL,
	"tr"	REAL,
	"dmplus"	REAL,
	"dmminus"	REAL,
	"tr14"	REAL,
	"dmplus14"	REAL,
	"dmminus14"	REAL,
	"diplus"	REAL,
	"diminus"	REAL,
	"dx"	REAL,
	"adx"	REAL,
	"smmaUp14"	REAL, 
	"smmaDown14"	REAL,
	"rs"	REAL, 
	"rsi"	REAL, 
	"kP"	REAL, 
	"smoothkP"	REAL, 
	"dP"	REAL, 
	"roc10"	REAL, 
	"roc15"	REAL,
	"roc20"	REAL,
	"ros30"	REAL,
	"kst"	REAL, 
	"kSignal"	REAL
);

CREATE TABLE "5mKlines" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"symbol" TEXT
	"date"	INTEGER,
	"open"	REAL,
	"high"	REAL,
	"low"	REAL,
	"close"	REAL,
	"ma20"	REAL,
	"ma50"	REAL,
	"ma200"	REAL,
	"mplus"	REAL,
	"mminus"	REAL,
	"tr"	REAL,
	"dmplus"	REAL,
	"dmminus"	REAL,
	"tr14"	REAL,
	"dmplus14"	REAL,
	"dmminus14"	REAL,
	"diplus"	REAL,
	"diminus"	REAL,
	"dx"	REAL,
	"adx"	REAL,
	"smmaUp14"	REAL, 
	"smmaDown14"	REAL,
	"rs"	REAL, 
	"rsi"	REAL, 
	"kP"	REAL, 
	"smoothkP"	REAL, 
	"dP"	REAL, 
	"roc10"	REAL, 
	"roc15"	REAL,
	"roc20"	REAL,
	"ros30"	REAL,
	"kst"	REAL, 
	"kSignal"	REAL
);

CREATE TABLE "4hKlines" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"symbol" TEXT
	"date"	INTEGER,
	"open"	REAL,
	"high"	REAL,
	"low"	REAL,
	"close"	REAL,
	"ma20"	REAL,
	"ma50"	REAL,
	"ma200"	REAL,
	"mplus"	REAL,
	"mminus"	REAL,
	"tr"	REAL,
	"dmplus"	REAL,
	"dmminus"	REAL,
	"tr14"	REAL,
	"dmplus14"	REAL,
	"dmminus14"	REAL,
	"diplus"	REAL,
	"diminus"	REAL,
	"dx"	REAL,
	"adx"	REAL,
	"smmaUp14"	REAL, 
	"smmaDown14"	REAL,
	"rs"	REAL, 
	"rsi"	REAL, 
	"kP"	REAL, 
	"smoothkP"	REAL, 
	"dP"	REAL, 
	"roc10"	REAL, 
	"roc15"	REAL,
	"roc20"	REAL,
	"ros30"	REAL,
	"kst"	REAL, 
	"kSignal"	REAL
);

CREATE TABLE "1mKlines" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"symbol" TEXT
	"date"	INTEGER,
	"open"	REAL,
	"high"	REAL,
	"low"	REAL,
	"close"	REAL,
	"ma20"	REAL,
	"ma50"	REAL,
	"ma200"	REAL,
	"mplus"	REAL,
	"mminus"	REAL,
	"tr"	REAL,
	"dmplus"	REAL,
	"dmminus"	REAL,
	"tr14"	REAL,
	"dmplus14"	REAL,
	"dmminus14"	REAL,
	"diplus"	REAL,
	"diminus"	REAL,
	"dx"	REAL,
	"adx"	REAL,
	"smmaUp14"	REAL, 
	"smmaDown14"	REAL,
	"rs"	REAL, 
	"rsi"	REAL, 
	"kP"	REAL, 
	"smoothkP"	REAL, 
	"dP"	REAL, 
	"roc10"	REAL, 
	"roc15"	REAL,
	"roc20"	REAL,
	"ros30"	REAL,
	"kst"	REAL, 
	"kSignal"	REAL
);

CREATE table "posts"
(
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"message" TEXT
)

CREATE table "attentions"
(
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"message" TEXT
)