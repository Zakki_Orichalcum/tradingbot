#!/usr/bin/env node

const fetch = require('node-fetch');
const BinanceAPI = require('./apis/BinanceAPI');
const NNGAMachineLearning = require('./neuralnetwork/NNGA');
const A = require('./utilities').Arrays;
const Arrays = new A();
const math = require('mathjs');
const Algorithms = require('./utilities/Algorithms');
const sqlite3 = require('sqlite3');

function GetConnection(){
    global.db = new sqlite3.Database("data2.db");

    return global.db;
}

async function query(fn, query, values) {
    if (values) {
        fn = fn.bind(global.db, query, values);
    } else {
        fn = fn.bind(global.db, query);
    }
    return new Promise((resolve, reject) => {
        try {
            fn((err, rows) => {
                if (err) {
                    reject(
                        new Error(
                            `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                        ),
                    );
                } else {
                    resolve(rows);
                }
            });
        } catch (err) {
            reject(err);
        }
    });
}

async function Get(q)
{
    var conn = GetConnection();
    return await query(conn.get, q);
}

async function GetAll(q)
{
    var conn = GetConnection();
    return await query(conn.all, q);
}

async function Run(q)
{
    var conn = GetConnection();
    return await query(conn.run, q);
}

async function PrepareDataset()
{
    // var d = await PrepareDatasetLiveKline("BTC", "USDT");
    // var e = await PrepareDatasetLiveKline("ETH", "USDT");
    // var f = await PrepareDatasetLiveKline("DOGE", "USDT");

    // var output = [d, e, f];
    var output = [];
    var dt = [{p: "DOGE", s: "USDT"},{p: "ETH", s: "USDT"},{p: "BTC", s: "USDT"}];
    //var dt = [{p: "DOGE", s: "USDT"}];
    //var t = ["1m","5m","4h"];
    var t = ["5m"];
    for(var i = 0; i < dt.length; i++)
    {
        for(var j = 0; j < t.length; j++)
        {
            output.push(await PrepareDatasetFromLiveData(dt[i].p, dt[i].s, t[j]));
        }
    }

    return output;
}

async function PrepareDatasetFromLiveData(primary, secondary, timeFrame)
{
    var all = await GetAll(`Select * from [${timeFrame}Klines] where symbol = '${primary+secondary}' Order by date ASC`);
    var d = [];
    //console.log(all);

    var maxPrice = math.max(Arrays.MakeListFromObjects(all, "high"));
    var smUp = []; var smDown =[]; 
    all.forEach(v => {smUp.push(v.smmaUp14 ? v.smmaUp14 : 0); smDown.push(v.smmaDown14 ? v.smmaDown14 : 0);});
    var smmaMax = math.max(smUp.concat(smDown));

    all.forEach(x => d.push(klineToArray(x, maxPrice, smmaMax)));

    return { actual: all, normalized: d, primary: primary, secondary: secondary };
}

async function PrepareDatasetLiveKline(primary, secondary)
{
    var api = new BinanceAPI();
    var r = await api.GetKlinesForLastNumberOfDays(primary+secondary, 400);

    var klinesRaw = [];
    r.forEach(k => {
        klinesRaw.push({
            date: Number(k[0]),
            open: Number(k[1]),
            close: Number(k[4]),
            high: Number(k[2]),
            low: Number(k[3])
        });
    })

    var e = [];
    for(var ej = 0; ej < klinesRaw.length; ej++)
    {
        var kline = klinesRaw[ej];
        if(e.length > 0){
            e.push(kline);
    
            var ma20Arr = (e.length < 20) ? e :  Arrays.Range(e, e.length-20, e.length);
            var ma50Arr = (e.length < 50) ?  e :  Arrays.Range(e, e.length-50, e.length);
            var ma200Arr = (e.length < 200) ?  e :  Arrays.Range(e, e.length-200, e.length);
    
            ma20Arr = Arrays.MakeListFromObjects(ma20Arr, 'close');
            ma50Arr = Arrays.MakeListFromObjects(ma50Arr, 'close');
            ma200Arr = Arrays.MakeListFromObjects(ma200Arr, 'close');
    
            kline.ma20 = math.mean(ma20Arr);
            kline.ma50 = math.mean(ma50Arr);
            kline.ma200 = math.mean(ma200Arr);
    
            var prev = e[e.length-2];
    
            var ms = Algorithms.getMs(prev, kline);
    
            kline.mplus = ms.Mplus;
            kline.mminus = ms.Mminus;
            kline.tr = math.max([kline.high-prev.high, kline.high-kline.close, kline.low-kline.close]);
            var dms = Algorithms.getDMs(prev, kline);
            kline.dmplus = dms.DMplus;
            kline.dmminus = dms.DMminus;
            kline.tr14 = 0;
            kline.dmplus14 = 0;
            kline.dmminus14 = 0;
            kline.diplus = 0;
            kline.diminus = 0;
            kline.dx = 0;
            kline.adx = 0;
            kline.smmaUp14 = 0;
            kline.smmaDown14 = 0;
            kline.rsi = 0;
            kline.rs = 0;
            kline.kP = 0;
            kline.smoothkP = 0;
            kline.dP = 0;
    
            if(e.length >= 14)
            {
                var r = Arrays.Range(e, e.length-14, e.length);
                var high = math.max(Arrays.MakeListFromObjects(r, "high"));
                var low = math.min(Arrays.MakeListFromObjects(r, "low"));
                kline.kP = ((kline.close-low)/(high-low)) * 100;
                e[e.length-1].kP = kline.kP;
                if(e.length >= 17)
                {
                    var er = Arrays.Range(e, e.length-3, e.length);
                    kline.smoothkP = math.mean(Arrays.MakeListFromObjects(er, "kP"));
                }
                if(e.length >= 28)
                {
                    var dr = Arrays.Range(e, e.length-14, e.length);
                    kline.dP = math.mean(Arrays.MakeListFromObjects(dr, "kP"));
                }
            }
    
            if(e.length == 14)
            {
                var r = Arrays.Range(e, e.length-14, e.length);
                var ups = []; var downs = [];
                for(var i = 0; i< r.length; i++){
                    var item = r[i];
                    var d = item.open-item.close;
                    ups.push(d > 0 ? d : 0);
                    downs.push(d < 0 ? -1 * d : 0);
                }
    
                kline.smmaUp14 = math.mean(ups);
                kline.smmaDown14 = math.mean(downs);
                kline.rs = kline.smmaUp14 / kline.smmaDown14;
                kline.rsi = 100 - (100/(1+kline.rs));
            }
            else if(e.length > 14)
            {
                var d = kline.open-kline.close;
                var up = d > 0 ? d : 0;
                var down = d < 0 ? -1 * d : 0;
                kline.smmaUp14 = (up + (prev.smmaUp14 * 13))/14;
                kline.smmaDown14 = (down + (prev.smmaDown14 * 13))/14;
                kline.rs = kline.smmaUp14 / kline.smmaDown14;
                kline.rsi = 100 - (100/(1+kline.rs));
            }
    
            if(e.length == 15)
            {
                e[e.length-1].dmplus = kline.dmplus;
                e[e.length-1].dmminus = kline.dmminus;
                e[e.length-1].tr = kline.tr;
                var e2 = Arrays.Range(e, 1, 15);
                kline.dmplus14 = math.sum(Arrays.MakeListFromObjects(e2, 'dmplus')); 
                kline.dmminus14 = math.sum(Arrays.MakeListFromObjects(e2, 'dmminus')); 
                kline.tr14 = math.sum(Arrays.MakeListFromObjects(e2, 'tr'));
            }
            else if(e.length > 15){
                kline.tr14 = (prev.tr14 - (prev.tr14/14)) + kline.tr;
                kline.dmplus14 = (prev.dmplus14 - (prev.dmplus14/14)) + kline.dmplus;
                kline.dmminus14 = (prev.dmminus14 - (prev.dmminus14/14)) + kline.dmminus;
    
                kline.diplus = (kline.dmplus14/kline.tr14) * 100;
                kline.diminus = (kline.dmminus14/kline.tr14) * 100;
    
                var abs = math.abs(kline.diplus-kline.diminus);
                var sum = kline.diplus+kline.diminus;
                kline.dx = (abs/sum) * 100;
    
                if(e.length == 28)
                {
                    e[e.length-1].dx = kline.dx;
                    var e3 = Arrays.Range(e, 16, 28);
    
                    kline.adx = math.mean(Arrays.MakeListFromObjects(e3, 'dx'));
                }
                else if(e.length > 28)
                {
                    kline.adx = ((prev.adx * (13)) + kline.dx)/14;
                }
            }

            e[e.length-1] = kline;
        }
        else{
            kline.ma20 = kline.close;
            kline.ma50 = kline.close;
            kline.ma200 = kline.close;
            kline.mplus = 0;
            kline.mminus = 0;
            kline.tr = 0;
            kline.dmplus = 0;
            kline.dmminus = 0;
            kline.tr14 = 0;
            kline.dmplus14 = 0;
            kline.dmminus14 = 0;
            kline.diplus = 0;
            kline.diminus = 0;
            kline.dx = 0;
            kline.adx = 0;
            kline.smmaUp14 = 0;
            kline.smmaDown14 = 0;
            kline.rsi = 0;
            kline.rs = 0;
            kline.kP = 0;
            kline.smoothkP = 0;
            kline.dP = 0;
            e.push(kline);
        }
    }

    var d = [];

    var maxPrice = math.max(Arrays.MakeListFromObjects(e, "high"));
    var smmaMax = math.max(Arrays.MakeListFromObjects(e, "smmaUp14").concat(Arrays.MakeListFromObjects(e, "smmaDown14")));

    e.forEach(x => d.push(klineToArray(x, maxPrice, smmaMax)));

    return { actual: e, normalized: d, primary: primary, secondary: secondary };
}

function klineToArray(kline, maxPrice, smmaMax)
{
    var output = [];
    //output.push(kline.date);
    output.push(kline.open/maxPrice);
    output.push(kline.close/maxPrice);
    output.push(kline.high/maxPrice);
    output.push(kline.low/maxPrice);
    output.push(kline.ma20 ? kline.ma20/maxPrice : 0);
    output.push(kline.ma50 ? kline.ma50/maxPrice : 0);
    output.push(kline.ma200 ? kline.ma200/maxPrice : 0);
    //output.push((kline.mplus + mscaler)/(mscaler*2));
    //output.push((kline.mminus + mscaler)/(mscaler*2));
    //output.push(kline.tr/trMax);
    //output.push((kline.dmplus + mscaler)/(mscaler*2));
    //output.push((kline.dmminus + mscaler)/(mscaler*2));
    //output.push(kline.tr14/maxPrice);
    //output.push(kline.dmplus14/maxPrice);
    //output.push(kline.dmminus14/maxPrice);
    output.push(kline.diplus ? kline.diplus/100 : 0);
    output.push(kline.diminus ? kline.diminus/100 : 0);
    //output.push(kline.dx/100);
    output.push(kline.adx ? kline.adx/100 : 0);
    output.push(kline.smmaUp14 ? kline.smmaUp14/smmaMax : 0);
    output.push(kline.smmaDown14 ? kline.smmaDown14/smmaMax : 0);
    output.push(kline.rsi ? kline.rsi/100 : 0);
    //output.push(kline.rs/10);
    output.push(kline.kP ? kline.kP/100 : 0);
    output.push(kline.smoothkP ? kline.smoothkP/100 : 0);
    output.push(kline.dP ? kline.dP/100 : 0);
    output.push(kline.kst && kline.kSignal ? kline.kst/kline.kSignal : 0);

    return output;
}

async function main(){
    var data = await PrepareDataset();
    //var d = [];

    console.log(`Dataset Prepared`);
    var ga = new NNGAMachineLearning("nnga",true, data, 100, [17, 33, 3]);
    //var ga = new NNGAMachineLearning("nnga", false, data, 10, [26, 51, 3]);

    //var final = ga.Execute(2);
    //var final = ga.Execute(100);
    var final = ga.Execute(10000);
    console.log(final);
}


main();
// X = np.array([[0, 0, 1], [1, 1, 1], [1, 0, 1], [0, 1, 1]])
// y = np.array([[0, 1, 1, 0]]).T
// network = [[3,10,sigmoid],[None,1,sigmoid]]
// ga = genetic_algorithm
// agent = ga.execute(100,5000,0.1,X,y,network)
// weights = agent.neural_network.weights
// agent.fitness
// agent.neural_network.propagate(X)