const auth = require("../auth.json").Binance;
const APIBase = require('./APIBase');
const uid = require('../neuralnetwork/uid');
const querystring = require('querystring');
const r = require('../utilities/RNG');
const RNG = new r();

const fetch = require('node-fetch');
const crypto = require('crypto-js');
const day = 24 * 60 * 60 * 1000;

class BinanceAPITester extends APIBase{
    constructor(exchangeInformation){
        super(exchangeInformation);
        this.endpoint = "https://api.binance.us";
        this.exchangeInformation = exchangeInformation;
        this.balance = {DOGE: 10000, USDT:100000, ETH: 40, BTC: 40};
        this.symbols = {
            "DOGEUSDT" : { combinedSymbol: 'DOGEUSDT', primarySymbol: 'DOGE', secondarySymbol: 'USDT' },
            "ETHUSDT" : { combinedSymbol: 'ETHUSDT', primarySymbol: 'ETH', secondarySymbol: 'USDT' },
            "BTCUSDT" : { combinedSymbol: 'BTCUSDT', primarySymbol: 'BTC', secondarySymbol: 'USDT' },
            "ETHBTC" : { combinedSymbol: 'ETHBTC', primarySymbol: 'ETH', secondarySymbol: 'BTC' },
        };
        this.orderId = RNG.nextInt();
        this.orders = {};
    }

    timer(ms){ return new Promise( res => setTimeout(res, ms)) };

    async GetServerTime()
    {
        var ts = await fetch(`${this.endpoint}/api/v3/time`);
        var timestamp = await ts.json();

        return timestamp.serverTime;
    }


//#region Klines

    async GetKlinesForLastNumberOfDays(symbol, days){
        var timestamp = await this.GetServerTime();

        var last = timestamp - (days * day);

        var q = { symbol:symbol, interval: "1d", startTime: last };
        var queryStr = this.QueryString(q);

        var d = await fetch(`${this.endpoint}/api/v3/klines?${queryStr}`);
        var j = await d.json();

        return j;
    }

//#endregion

//#region Orders 
 getqueryString( url ){
    let qParams = {};
    // create a binding tag to use a property called search
    let anchor = document.createElement('a');
    // assign the href URL of the anchor tag
    anchor.href = url;
    // search property returns URL query string
    let qStrings = anchor.search.substring(1);
    let params = qStrings.split('&');
    for (let i = 0; i < params.length; i++) {
        let pair = params[i].split('=');
        qParams[pair[0]] = decodeURIComponent(pair[1]);
        }
    return qParams;
  }

    async PlaceANewOrder(q){
        //console.log(q, q.quantity);
        var price = q.price ? q.price : await this.GetAveragePrice(q.symbol);

        var quantity = 0;
        var dollars = 0;
        var sym = this.symbols[q.symbol]
        if(q.side == "BUY")
        {
            quantity = Number(q.quantity);
            dollars = quantity * price;
            this.balance[sym.primarySymbol] += quantity;
            this.balance[sym.secondarySymbol] -= dollars;
        }
        else
        {
            quantity = Number(q.quantity);
            dollars = quantity * price;
            this.balance[sym.primarySymbol] -= quantity;
            this.balance[sym.secondarySymbol] += dollars;
        }
        this.orderId = this.orderId + 1;

        var output = {
            "symbol": q.symbol,
            "orderId": this.orderId,
            "orderListId": -1, //Unless OCO, value will be -1
            "clientOrderId": uid(),
            "transactTime": 1507725176595,
            "price": price,
            "origQty": quantity,
            "executedQty": quantity,
            "cummulativeQuoteQty": dollars,
            "status": "FILLED",
            "timeInForce": "GTC",
            "type": "MARKET",
            "side": q.side
          };

        this.orders[this.orderId] = output;

        return output;
    }

    async PlaceMarketOrder(symbol, side, quantity)
    {
        var q = {symbol:symbol, side:side, type:'MARKET', timeInForce: 'GTC', quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime()};

        return await this.PlaceANewOrder(q);
    }

    async PlaceLimitOrder(symbol, side, quantity, price)
    {
        var q = {symbol:symbol, side:side, type:'LIMIT', timeInForce: 'GTC', price:price,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }

    async PlaceStopLossLimitOrder(symbol, side, quantity, price, stopPrice)
    {
        var q = {symbol:symbol, side:side, type:'STOP_LOSS_LIMIT', timeInForce: 'GTC', price:price, stopPrice: stopPrice,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };
            //console.log(JSON.stringify(q));

        return await this.PlaceANewOrder(q);
    }

    async PlaceStopLossOrder(symbol, side, quantity, stopPrice)
    {
        var q = {symbol:symbol, side:side, type:'STOP_LOSS', timeInForce: 'GTC', stopPrice: stopPrice,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }

    async PlaceTakeProfitLimitOrder(symbol, side, quantity, price, stopPrice)
    {
        var q = {symbol:symbol, side:side, type:'TAKE_PROFIT_LIMIT', timeInForce: 'GTC', price:price, stopPrice: stopPrice,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }

    async PlaceTakeProfitOrder(symbol, side, quantity, stopPrice)
    {
        var q = {symbol:symbol, side:side, type:'TAKE_PROFIT', timeInForce: 'GTC', stopPrice: stopPrice,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }

    async PlaceLimitMakerOrder(symbol, side, quantity, price)
    {
        var q = {symbol:symbol, side:side, type:'LIMIT_MAKER', timeInForce: 'GTC', price:price,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }
    
    async CancellingOrder(symbol, id, origId){
        var q = {symbol:symbol, orderId: id, recvWindow: 5000, timestamp: await this.GetServerTime(), origClientOrderId: origId};
        var querystr = this.QueryString(q);
        var api = '/api/v3/order';
        var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        var d = await fetch(`${this.endpoint}${api}?${querystr}&signature=${hmac}`, { 
            method: 'DELETE',
            headers: {
                "X-MBX-APIKEY": auth.apiKey,
                "Content-Type": "application/json",
            }
        });

        return await d.json();
    }
        
    async GetOrder(symbol, orderId, origId){
        return this.orders[orderId];
    }

//#endregion

//#region Pricing and Balances
    async GetBalances(){
        var q = {
                "DOGE": {
                    "coin": "DOGE",
                    "depositAllEnable": true,
                    "free": this.balance.DOGE,
                    "freeze": "0.00000000",
                    "ipoable": "0.00000000",
                    "ipoing": "0.00000000",
                    "isLegalMoney": false,
                    "locked": "0.00000000",
                    "name": "Dogecoin",
                    "networkList": [],
                    "storage": "0.00000000",
                    "trading": true,
                    "withdrawAllEnable": true,
                    "withdrawing": "0.00000000"
                },
                "USDT":{
                    "coin": "USDT",
                    "depositAllEnable": true,
                    "free": this.balance.USDT,
                    "freeze": "0.00000000",
                    "ipoable": "0.00000000",
                    "ipoing": "0.00000000",
                    "isLegalMoney": false,
                    "locked": "0.00000000",
                    "name": "Tether",
                    "networkList": [],
                    "storage": "0.00000000",
                    "trading": true,
                    "withdrawAllEnable": true,
                    "withdrawing": "0.00000000"
                },
                "ETH": {
                    "coin": "ETH",
                    "depositAllEnable": true,
                    "free": this.balance.ETH,
                    "freeze": "0.00000000",
                    "ipoable": "0.00000000",
                    "ipoing": "0.00000000",
                    "isLegalMoney": false,
                    "locked": "0.00000000",
                    "name": "Etherium",
                    "networkList": [],
                    "storage": "0.00000000",
                    "trading": true,
                    "withdrawAllEnable": true,
                    "withdrawing": "0.00000000"
                },
                "BTC":{
                    "coin": "BTC",
                    "depositAllEnable": true,
                    "free": this.balance.BTC,
                    "freeze": "0.00000000",
                    "ipoable": "0.00000000",
                    "ipoing": "0.00000000",
                    "isLegalMoney": false,
                    "locked": "0.00000000",
                    "name": "Bitcoin",
                    "networkList": [],
                    "storage": "0.00000000",
                    "trading": true,
                    "withdrawAllEnable": true,
                    "withdrawing": "0.00000000"
                }
        };

        //console.log(this.balance);
    
        return q;
    }

    async GetLatestPrice(symbol){
        var d = await fetch(`${this.endpoint}/api/v3/ticker/price?symbol=${symbol}`);
        var j = await d.json();
    
        return j.price;
    }

    
    async GetLatestPriceInUSDForSymbol(symbol){
        var d = await fetch(`${this.endpoint}/api/v3/ticker/price?symbol=${symbol}USD`);
        var j = await d.json();
    
        return j.price;
    }
    
    async GetAveragePrice(symbol){
        var d = await fetch(`${this.endpoint}/api/v3/avgPrice?symbol=${symbol}`);
        var j = await d.json();
    
        return j.price;
    
    }
//#endregion

}

module.exports = BinanceAPITester;