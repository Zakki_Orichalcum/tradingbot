const sqlite3 = require('sqlite3').verbose();
const SQLUtility = require("../utilities").SQLUtility;
const SQL = new SQLUtility();
const N = require("../utilities").Numbers;
const fetch = require('node-fetch');
const auth = require('../auth');
const crypto = require('crypto-js');
const math = require('mathjs');
const A = require("../utilities").Arrays;
const Arrays = new A();

class APIBase{
    constructor(){
        this.endpoint = "";
    }

    timer(ms){ return new Promise( res => setTimeout(res, ms)) };

    async GetServerTime(){

        return 0;
    }


//#region Klines

    async GetKlinesForLastNumberOfDays(symbol, days){
        return {};
    }

//#endregion

//#region Orders 

    async PlaceMarketOrder(symbol, side, quantity)
    {
        return {};
    }

    async PlaceLimitOrder(symbol, side, quantity, price)
    {
        return {};
    }

    async PlaceStopLossLimitOrder(symbol, side, quantity, price, stopPrice)
    {
        return {};
    }

    async PlaceStopLossOrder(symbol, side, quantity, stopPrice)
    {
        return {};
    }

    async PlaceTakeProfitLimitOrder(symbol, side, quantity, price, stopPrice)
    {
        return {};
    }

    async PlaceTakeProfitOrder(symbol, side, quantity, stopPrice)
    {
        return {};
    }

    async PlaceLimitMakerOrder(symbol, side, quantity, price, stopPrice)
    {
        return {};
    }
    
    async CancellingOrder(symbol, id, origId){
        return {};
    }
        
    async GetOrder(symbol, orderId, origId){
        return {};
    }

//#endregion

//#region Pricing and Balances
    async GetBalances(){
        return {};
    }
    
    async GetLatestPrice(symbol){
        return 0;
    }
    
    async GetLatestPriceInUSDForSymbol(symbol){
        return 0;
    }
    
    async GetAveragePrice(symbol){
        return 0;
    }
//#endregion

    QueryString(obj){
        return Object.keys(obj).map((key) => {
            return `${key}=${obj[key]}`;
        }).join('&');
    }
}

module.exports = APIBase;