const auth = require("../auth.json").Binance;
const APIBase = require('./APIBase');

const fetch = require('node-fetch');
const crypto = require('crypto-js');
const day = 24 * 60 * 60 * 1000;

class BinanceAPI extends APIBase{
    constructor(exchangeInformation){
        super(exchangeInformation);
        this.endpoint = "https://api.binance.us";
        this.exchangeInformation = exchangeInformation;
    }

    timer(ms){ return new Promise( res => setTimeout(res, ms)) };

    async GetServerTime()
    {
        var ts = await fetch(`${this.endpoint}/api/v3/time`);
        var timestamp = await ts.json();

        return timestamp.serverTime;
    }


//#region Klines

    async GetKlinesForLastNumberOfDays(symbol, days){
        var timestamp = await this.GetServerTime();

        var last = timestamp - (days * day);

        var q = { symbol:symbol, interval: "1d", startTime: last };
        var queryStr = this.QueryString(q);

        var d = await fetch(`${this.endpoint}/api/v3/klines?${queryStr}`);
        var j = await d.json();

        return j;
    }

//#endregion

//#region Orders 

    async PlaceANewOrder(q){
        var querystr = this.QueryString(q);
        //console.log(querystr, q);
        var api = '/api/v3/order';
        var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        var d = await fetch(`${this.endpoint}${api}?${querystr}&signature=${hmac}`, { 
            method: 'POST',
            headers: {
                "X-MBX-APIKEY": auth.apiKey,
                "Content-Type": "application/json",
            }
        });
        return await d.json();
    }

    async PlaceMarketOrder(symbol, side, quantity)
    {
        var q = {symbol:symbol, side:side, type:'MARKET', timeInForce: 'GTC', quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime()};

        return await this.PlaceANewOrder(q);
    }

    async PlaceLimitOrder(symbol, side, quantity, price)
    {
        var q = {symbol:symbol, side:side, type:'LIMIT', timeInForce: 'GTC', price:price,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }

    async PlaceStopLossLimitOrder(symbol, side, quantity, price, stopPrice)
    {
        var q = {symbol:symbol, side:side, type:'STOP_LOSS_LIMIT', timeInForce: 'GTC', price:price, stopPrice: stopPrice,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };
            console.log(JSON.stringify(q));

        return await this.PlaceANewOrder(q);
    }

    async PlaceStopLossOrder(symbol, side, quantity, stopPrice)
    {
        var q = {symbol:symbol, side:side, type:'STOP_LOSS', timeInForce: 'GTC', stopPrice: stopPrice,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }

    async PlaceTakeProfitLimitOrder(symbol, side, quantity, price, stopPrice)
    {
        var q = {symbol:symbol, side:side, type:'TAKE_PROFIT_LIMIT', timeInForce: 'GTC', price:price, stopPrice: stopPrice,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }

    async PlaceTakeProfitOrder(symbol, side, quantity, stopPrice)
    {
        var q = {symbol:symbol, side:side, type:'TAKE_PROFIT', timeInForce: 'GTC', stopPrice: stopPrice,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }

    async PlaceLimitMakerOrder(symbol, side, quantity, price)
    {
        var q = {symbol:symbol, side:side, type:'LIMIT_MAKER', timeInForce: 'GTC', price:price,
            quantity: quantity, recvWindow: 5000, timestamp: await this.GetServerTime() };

        return await this.PlaceANewOrder(q);
    }
    
    async CancellingOrder(symbol, id, origId){
        var q = {symbol:symbol, orderId: id, recvWindow: 5000, timestamp: await this.GetServerTime(), origClientOrderId: origId};
        var querystr = this.QueryString(q);
        var api = '/api/v3/order';
        var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        var d = await fetch(`${this.endpoint}${api}?${querystr}&signature=${hmac}`, { 
            method: 'DELETE',
            headers: {
                "X-MBX-APIKEY": auth.apiKey,
                "Content-Type": "application/json",
            }
        });

        return await d.json();
    }
        
    async GetOrder(symbol, orderId, origId){
        var q = {symbol: symbol, origClientOrderId:origId, orderId:orderId, timestamp: await this.GetServerTime() };
    
        var querystr = this.QueryString(q);
        var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        var d = await fetch(`${this.endpoint}/api/v3/order?${querystr}&signature=${hmac}`, { 
            method: 'GET',
            headers: {
                "X-MBX-APIKEY": auth.apiKey
            }
        });
        return await d.json();
    }

//#endregion

//#region Pricing and Balances
    async GetBalances(){
        var q = { timestamp: await this.GetServerTime() };
    
        var querystr = this.QueryString(q);
        var hmac = crypto.HmacSHA256(querystr, auth.secret).toString();
        var d = await fetch(`${this.endpoint}/sapi/v1/capital/config/getall?${querystr}&signature=${hmac}`, { 
            method: 'GET',
            headers: {
                "Content-Type":"application/json",
                "X-MBX-APIKEY": auth.apiKey
            }
        });
        var s = await d.json();

        var output = {};
        for(var i = 0; i < s.length; i++){
            var coin = s[i];
            output[coin.coin] = coin;
        }

        return output;
    }

    async GetLatestPrice(symbol){
        var d = await fetch(`${this.endpoint}/api/v3/ticker/price?symbol=${symbol}`);
        var j = await d.json();
    
        return j.price;
    }

    
    async GetLatestPriceInUSDForSymbol(symbol){
        var d = await fetch(`${this.endpoint}/api/v3/ticker/price?symbol=${symbol}USD`);
        var j = await d.json();
    
        return j.price;
    }
    
    async GetAveragePrice(symbol){
        var d = await fetch(`${this.endpoint}/api/v3/avgPrice?symbol=${symbol}`);
        var j = await d.json();
    
        return j.price;
    
    }
//#endregion

}

module.exports = BinanceAPI;