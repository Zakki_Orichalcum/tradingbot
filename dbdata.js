//const SQLUtility = require("../utilities").SQLUtility;
//const fs = require('fs');

//const SQL = new SQLUtility();

const DEFAULT_DB = 'data.db';
const sqlite3 = require('sqlite3').verbose();

//const defaultUser = '7536872b-a8ae-41b5-a93b-ac767c496338';
const colors = ["indigo","blue","light-blue","cyan","teal","green","light-green","lime","red","pink","purple","deep-purple","yellow","amber","orange","deep-orange","brown","blue-grey","grey"];

class Database{
    async setup(){
        console.log('Setting up database.');
        //const buildEngine = new dbbuild.SQLBuildEngine(DEFAULT_DB, true, false, false, false, true);
        //buildEngine.Add(new dbbuild.ExecuteOnce('../SQL/*.sql'));
        //await buildEngine.Run();
        console.log('DB complete');
    }    

    GetConnection(){
        this.db = new sqlite3.Database(DEFAULT_DB);
    
        return this.db;
      }
    
      async query (fn, query, values) {
        if (values) {
            fn = fn.bind(this.db, query, values);
        } else {
            fn = fn.bind(this.db, query);
        }
        return new Promise((resolve, reject) => {
            try {
                fn((err, rows) => {
                    if (err) {
                        reject(
                            new Error(
                                `Error running query: ${query} - ${JSON.stringify(values)}: ${err}`,
                            ),
                        );
                    } else {
                        resolve(rows);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
      }

    async AddFeed(url){
        var connection = this.GetConnection();
        var f = await this.GetFeedItem();

        await this.query(connection.run, `INSERT INTO feeds(feedUrl, color) Values ('${url}', '${colors[f.feeds.length % (colors.length-1)]}')`);
    }

    async GetFeed(url)
    {
        var output = [];
        if(url)
        {
            var opts = {
                headers: {
                    'method':'GET',
                }
            };

            await fetch(url, opts)
            .then(res => {
                console.log(res);
                var parser = new DOMParser();
                return parser.parseFromString(res.body, 'text/xml');
            })
            .then(xml =>{
                console.log(xml);
            });
        }
    }

    async GetFeeds(){
        var connection = this.GetConnection();

        var d = await this.query(connection.all, `Select * from feeds`);

        if(d)
            return { feeds:d };
        else
            return { feeds:[] };
    }
}

    
module.exports = Database;