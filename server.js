#!/usr/bin/env node

const MainProgram = require('./main.js');

const main = new MainProgram();

main.main().catch(err => {
    console.log(err);
    process.exit(1);
});
